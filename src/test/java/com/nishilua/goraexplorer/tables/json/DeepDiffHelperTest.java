/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.avro.util.Utf8;
import org.apache.gora.util.GoraException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nishilua.goraexplorer.exceptions.DiffTooLargeArrayIndexException;

import org.junit.Assert;
import persistent.ParseStatus;
import persistent.ProtocolStatus;
import persistent.WebPage;

public class DeepDiffHelperTest {

    public static ObjectMapper mapper = new ObjectMapper() ;
    
    public static ObjectNode addArrayElementDiffEntry = null ;
    public static ObjectNode addArrayElementLargeIndexDiffEntry = null ;
    public static ObjectNode editArrayElementDiffEntry = null ;
    public static ObjectNode deleteArrayElementDiffEntry = null ;
    public static ArrayNode deleteArrayElementsDiffEntry = null ;
    public static ObjectNode editSimpleFieldDiffEntry = null ;
    public static ObjectNode addMapElementDiffEntry = null ;
    public static ObjectNode editMapElementDiffEntry = null ;
    public static ObjectNode deleteMapElementDiffEntry = null ;
    public static ObjectNode removeRecordDiffEntry = null ;
    public static ObjectNode addRecordWithArrayDiffEntry = null ;
    
    WebPage baseEntity ;
    
    @BeforeClass
    public static void initializeTest() {
        // Insert an array element. A - N
        addArrayElementDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "A")
                .put("index", 4) ;
        addArrayElementDiffEntry.putArray("path").add("parseStatus").add("args") ;
        addArrayElementDiffEntry.putObject("item").put("kind", "N").put("rhs","New String value") ;
        
        // Insert an array element. A - N
        addArrayElementLargeIndexDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "A")
                .put("index", 10) ;
        addArrayElementLargeIndexDiffEntry.putArray("path").add("parseStatus").add("args") ;
        addArrayElementLargeIndexDiffEntry.putObject("item").put("kind", "N").put("rhs","String value with large index") ;
        
        // Edit an array element
        editArrayElementDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "E")
                .put("lhs", "(ignore)")
                .put("rhs", "Edited value") ;
        editArrayElementDiffEntry.putArray("path").add("parseStatus").add("args").add(0) ;
        
        // Delete an array element. A - D
        deleteArrayElementDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "A")
                .put("index", 1) ;
        deleteArrayElementDiffEntry.putArray("path").add("parseStatus").add("args") ;
        deleteArrayElementDiffEntry.putObject("item").put("kind", "D").put("lhs","(ignore)") ;
        
        // Delete several array elements. A - D * 3
        deleteArrayElementsDiffEntry = mapper.createArrayNode() ;
        ObjectNode tmpArrayElement = mapper.createObjectNode()
                .put("kind", "A")
                .put("index", 0) ;
        tmpArrayElement.putArray("path").add("parseStatus").add("args") ;
        tmpArrayElement.putObject("item").put("kind", "D").put("lhs","(ignore)") ;
        deleteArrayElementsDiffEntry.add(tmpArrayElement) ;
        tmpArrayElement = mapper.createObjectNode()
                .put("kind", "A")
                .put("index", 1) ;
        tmpArrayElement.putArray("path").add("parseStatus").add("args") ;
        tmpArrayElement.putObject("item").put("kind", "D").put("lhs","(ignore)") ;
        deleteArrayElementsDiffEntry.add(tmpArrayElement) ;
        tmpArrayElement = mapper.createObjectNode()
                .put("kind", "A")
                .put("index", 2) ;
        tmpArrayElement.putArray("path").add("parseStatus").add("args") ;
        tmpArrayElement.putObject("item").put("kind", "D").put("lhs","(ignore)") ;
        deleteArrayElementsDiffEntry.add(tmpArrayElement) ;
        
        // Edit a simple field
        editSimpleFieldDiffEntry  = (ObjectNode) mapper.createObjectNode()
                .put("kind", "E")
                .put("lhs", "(ignore)")
                .put("rhs", "314") ;
        editSimpleFieldDiffEntry.putArray("path").add("fetchTime") ;
        
        // Add a map element
        addMapElementDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "N")
                .put("rhs", "NewValue") ;
        addMapElementDiffEntry.putArray("path").add("headers").add("newKey") ;
        
        // Edit a map element
        editMapElementDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "E")
                .put("lhs", "(ignore)")
                .put("rhs", "New header value") ;
        editMapElementDiffEntry.putArray("path").add("headers").add("Cache-Control") ;
        
        // Delete a map element
        deleteMapElementDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "D")
                .put("lhs", "(ignore)") ;
        deleteMapElementDiffEntry.putArray("path").add("headers").add("Connection") ;

        // Remove a record field
        removeRecordDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "E")
                .put("lhs", "(ignore)")
                .putNull("rhs") ;
        removeRecordDiffEntry.putArray("path").add("protocolStatus") ;
        
        // Add a record (as field) with an array element
        addRecordWithArrayDiffEntry = (ObjectNode) mapper.createObjectNode()
                .put("kind", "E")
                .put("lhs", "(ignore)") ;
        addRecordWithArrayDiffEntry.putArray("path").add("protocolStatus") ;
        addRecordWithArrayDiffEntry.putObject("rhs")
                .put("code", 10)
                .put("lastModified", 20)
                .putArray("args").add("30").add("40") ;
        
        // TODO Add Test for Array - New element but with items being Persistent instances
    }
    
    @Before
    public void BeforeTest() {
        baseEntity = this.buildWebPage() ;
    }
    
    /**
     * Persistent entity to use in the tests
     * @return
     */
    private WebPage buildWebPage() {
        WebPage webpage = WebPage.newBuilder().build() ;
        webpage.setFetchTime(1500000L) ;

        ParseStatus parseStatus = ParseStatus.newBuilder().build() ;
        List<CharSequence> newArgs = new ArrayList<CharSequence>() ;
            newArgs.add("value1") ;
            newArgs.add("value2") ;
            newArgs.add("value3") ;
            newArgs.add("value4") ;
        parseStatus.setArgs(newArgs);
        
        Map<CharSequence,CharSequence> headers = new HashMap<CharSequence,CharSequence>() ;
        headers.put("Cache-Control", "Initial value for map value") ;
        headers.put("Other header", "Other value") ;
        headers.put("Connection", "Entry to delete") ;
        
        webpage.setHeaders(headers) ; 
        webpage.setParseStatus(parseStatus);
        return webpage ;
    }
    
    @Test // E
    public void testEditSimpleField() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage();
        expected.setFetchTime(314L);
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(editSimpleFieldDiffEntry));
        Assert.assertEquals(expected, baseEntity);
    }
    
    @Test // E
    public void testEditArrayElement() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage();
        expected.getParseStatus().getArgs().set(0, "Edited value") ;
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(editArrayElementDiffEntry));
        Assert.assertEquals(expected, baseEntity);        
    }
    
    @Test // E
    public void testEditMapElement() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage();
        expected.getHeaders().put(new Utf8("Cache-Control"), "New header value") ;
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(editMapElementDiffEntry));
        Assert.assertEquals(expected, baseEntity);   
    }
    
    /**
     * Sets null a field (which contains a record data)
     * @throws ClassNotFoundException
     */
    @Test // E
    public void testRemoveRecordField() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage();
        expected.setProtocolStatus(null) ;
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(removeRecordDiffEntry));
        Assert.assertEquals(expected, baseEntity);   
    }
    
    @Test // E
    public void testEditAddRecordField() throws GoraException, ClassNotFoundException {
        baseEntity.setProtocolStatus(null);
        WebPage expected = this.buildWebPage();
        ProtocolStatus expectedPS = ProtocolStatus.newBuilder().build() ;
            List<CharSequence> expectedPSArgs = new ArrayList<CharSequence>() ;
            expectedPSArgs.add("30") ;
            expectedPSArgs.add("40") ;
            expectedPS.setCode(10) ;
            expectedPS.setLastModified(20L) ;
            expectedPS.setArgs(expectedPSArgs) ;
        expected.setProtocolStatus(expectedPS) ;
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(addRecordWithArrayDiffEntry));
        Assert.assertEquals(expected, baseEntity);   
    }
    
    @Test // N
    public void testAddMapElement() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage() ;
        expected.getHeaders().put(new Utf8("newKey"), "NewValue") ;
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(addMapElementDiffEntry));
        Assert.assertEquals(expected, baseEntity);        
    }
    
    @Test // D
    public void testDeleteMapEntry() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage() ;
        expected.getHeaders().remove(new Utf8("Connection")) ;
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(deleteMapElementDiffEntry));
        Assert.assertEquals(expected, baseEntity);  
    }
    
    @Test // A - N
    public void testInsertArrayElement() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage() ;
        expected.getParseStatus().getArgs().add(new Utf8("New String value")) ;
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(addArrayElementDiffEntry));
        Assert.assertEquals(expected, baseEntity);
    }
    
    @Test // A - D
    public void testDeleteArrayElement() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage() ;
        expected.getParseStatus().getArgs().remove(1) ;
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(deleteArrayElementDiffEntry));
        Assert.assertEquals(expected, baseEntity);
    }
    
    /**
     * Deletes several array elements
     * @throws ClassNotFoundException
     */
    @Test // A - D * 3
    public void testDeleteArrayElements() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage() ;
        expected.getParseStatus().getArgs().remove(2) ;
        expected.getParseStatus().getArgs().remove(1) ;
        expected.getParseStatus().getArgs().remove(0) ;
        DeepDiffHelper.merge(baseEntity, deleteArrayElementsDiffEntry);
        Assert.assertEquals(expected, baseEntity);
    }
    
    @Test(expected=DiffTooLargeArrayIndexException.class) // A - N
    public void testAddArrayElementLargeIndexDiffEntry() throws GoraException, ClassNotFoundException {
        DeepDiffHelper.merge(baseEntity, mapper.createArrayNode().add(addArrayElementLargeIndexDiffEntry));
    }
    
    @Test
    public void testMultipleEdits() throws GoraException, ClassNotFoundException {
        WebPage expected = this.buildWebPage() ;
        expected.setFetchTime(314L);
        expected.getParseStatus().getArgs().add("New String value") ;
        expected.getParseStatus().getArgs().set(0, "Edited value") ;
        expected.getHeaders().put(new Utf8("newKey"), "NewValue") ;
        expected.getHeaders().remove(new Utf8("Connection")) ;
        expected.getHeaders().put(new Utf8("Cache-Control"), "New header value") ;
        ProtocolStatus expectedPS = ProtocolStatus.newBuilder().build() ;
        List<CharSequence> expectedPSArgs = new ArrayList<CharSequence>() ;
        expectedPSArgs.add("30") ;
        expectedPSArgs.add("40") ;
        expectedPS.setCode(10) ;
        expectedPS.setLastModified(20L) ;
        expectedPS.setArgs(expectedPSArgs) ;
        expected.setProtocolStatus(expectedPS) ;
    
        DeepDiffHelper.merge(baseEntity,
                mapper.createArrayNode()
                    .add(editSimpleFieldDiffEntry)
                    .add(editArrayElementDiffEntry)
                    .add(addArrayElementDiffEntry)
                    .add(addRecordWithArrayDiffEntry)
                    .add(addMapElementDiffEntry)
                    .add(editMapElementDiffEntry)
                    .add(deleteMapElementDiffEntry));
        Assert.assertEquals(expected, baseEntity);
    }
    
}
