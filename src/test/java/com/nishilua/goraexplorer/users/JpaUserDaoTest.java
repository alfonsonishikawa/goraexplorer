/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.users;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.annotation.Transactional;

import com.nishilua.goraexplorer.users.User;
import com.nishilua.goraexplorer.users.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/goraexplorer-servlet.xml" })
@WebAppConfiguration
public class JpaUserDaoTest {

    @PersistenceContext
    private EntityManager em ;
    
    @Autowired
    private UserService userService ;
    
    private List<User> expectedUsers = new ArrayList<>() ;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        expectedUsers.add(new User("admin", "$2a$10$jsWSIQjF5aW2k3knOhDkBuy24BlxZ8gJUbP5tbJjzXckQJcmOyNxK", "admin@no.com", true)) ;
        expectedUsers.add(new User("alfonso", "$2a$06$1hwx3MXIuqAHjUnB3vtSsOrG3H9nIEI8hRhu3jY4M8fnmqdWHtcb6", "alfonso.nishikawa@gmail.com", true)) ;
        expectedUsers.add(new User("jose1", "$2a$06$227NNFCpyk0QHcxg7oFWXunddSXoiIMNfiYCco8IiZio.4jHpcSV6", "jose@no.com", true)) ;
        expectedUsers.add(new User("pepe1", "$2a$06$8w7Ms8DsZjfEOXJ8wW9BMOqJ7VU5bvexrr7Ap.VGBJEnvMJChLyNi", "pepe@no.com", true)) ;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testFindAll() {
        Collection<User> users = this.userService.findAll() ;
        this.listUsers(users) ;
        assertEquals(this.expectedUsers, users);
    }

    @Test
    public void testFindById() {
        User expected = new User("alfonso", "$2a$06$1hwx3MXIuqAHjUnB3vtSsOrG3H9nIEI8hRhu3jY4M8fnmqdWHtcb6", "alfonso.nishikawa@gmail.com", true) ;
        User read = this.userService.get("alfonso") ;
        assertEquals(expected, read) ;
    }
    
    @Test
    public void testFindByEmail() {
        User expected = new User("pepe1", "$2a$06$8w7Ms8DsZjfEOXJ8wW9BMOqJ7VU5bvexrr7Ap.VGBJEnvMJChLyNi", "pepe@no.com", true) ;
        User read = this.userService.findByEmail("pepe@no.com") ;
        assertEquals(expected, read);
    }
    
    @Test(expected=ConstraintViolationException.class)
    @Transactional
    public void expectedConstraintViolationException() {
        User newUser = new User("n","p","e",true) ;
        this.userService.saveNewUser(newUser);
        em.flush();
    }
    
    @Test(expected=TransactionSystemException.class)
    public void expectedTransactionSystemException() {
        User newUser = new User("n","p","e",true) ;
        this.userService.saveNewUser(newUser);
    }

    private void listUsers(Collection<User> users) {
        System.out.println("Listing users set:");
        for (User user: users) {
            System.out.print("    "); System.out.println(user);
        }
    }

}
