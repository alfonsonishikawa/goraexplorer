/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Helper class for unit tests. Declares methods that returns input data for the tests.
 */
Ext.define('Tests.UnitTestsData', {
    singleton: true,

    uses: [
        'GoraExplorer.model.avro.EntitiesNode',
        'GoraExplorer.model.avro.EntityNode',
        'GoraExplorer.model.avro.StringNode',
        'GoraExplorer.model.avro.IntNode',
        'GoraExplorer.model.avro.LongNode',
        'GoraExplorer.model.avro.RecordNode',
        'GoraExplorer.model.avro.BytesNode',
        'GoraExplorer.model.avro.FloatNode',
        'GoraExplorer.model.avro.MapNode',
        'GoraExplorer.model.avro.ArrayNode'
    ],

    /**
     * Returns a new EntitiesNode tree with a test webpage schema
     * @return {GoraExplorer.model.avro.EntitiesNode|*}
     */
    getWebpageSchemaTreeRootNode: function() {

        var webpageSchema = Ext.create('GoraExplorer.model.avro.EntityNode', {
            name: 'WebPage',
            doc: 'WebPage is the primary data structure in Nutch representing crawl data for a given WebPage at some point in time'
        }) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.StringNode', {
            name: 'baseUrl',
            doc: 'The original associated with this WebPage.',
            nullable: true,
            defaultNullValue: true,
            hbaseFamily: 'f',
            hbaseColumn: 'bas'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.IntNode', {
            name: 'status',
            doc: 'A crawl status associated with the WebPage, can be of value STATUS_UNFETCHED - WebPage was not fetched yet, STATUS_FETCHED - WebPage was successfully fetched, STATUS_GONE - WebPage no longer exists, STATUS_REDIR_TEMP - WebPage temporarily redirects to other page, STATUS_REDIR_PERM - WebPage permanently redirects to other page, STATUS_RETRY - Fetching unsuccessful, needs to be retried e.g. transient errors and STATUS_NOTMODIFIED - fetching successful - page is not modified',
            nullable: false,
            defaultIntValue: 0,
            hbaseFamily: 'f',
            hbaseColumn: 'st'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.LongNode', {
            name: 'fetchTime',
            doc: 'The system time in milliseconds for when the page was fetched.',
            nullable: false,
            defaultLongValue: 0,
            hbaseFamily:'f',
            hbaseColumn: 'ts'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.LongNode', {
            name: 'prevFetchTime',
            doc: 'The system time in milliseconds for when the page was last fetched if it was previously fetched which can be used to calculate time delta within a fetching schedule implementation',
            nullable: false,
            defaultLongValue: 0,
            hbaseFamily: 'f',
            hbaseColumn: 'pts'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.RecordNode', {
            name: 'protocolStatus',
            recordFieldTypeName: 'ProtocolStatus',
            nullable: true,
            defaultNullValue: true,
            hbaseFamily: 'f',
            hbaseColumn: 'prot'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.BytesNode', {
            name: 'content',
            doc: 'The entire raw document content e.g. raw XHTML',
            nullable: true,
            defaultNullValue: true,
            hbaseFamily: 'f',
            hbaseColumn: 'cnt'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.RecordNode', {
            name: 'parseStatus',
            recordFieldTypeName: 'ParseStatus',
            nullable: true,
            defaultNullValue: true,
            hbaseFamily: 'p',
            hbaseColumn: 'st'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.FloatNode', {
            name: 'score',
            doc: 'A score used to determine a WebPage\'s relevance within the web graph it is part of. This score may change over time based on graph characteristics.',
            nullable: false,
            defaultLongValue: 0,
            hbaseFamily: 's',
            hbaseColumn: 's'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.MapNode', {
            name: 'headers',
            doc: 'Header information returned from the web server used to server the content which is subsequently fetched from. This includes keys such as TRANSFER_ENCODING, CONTENT_ENCODING, CONTENT_LANGUAGE, CONTENT_LENGTH, CONTENT_LOCATION, CONTENT_DISPOSITION, CONTENT_MD5, CONTENT_TYPE, LAST_MODIFIED and LOCATION.',
            nullable: false,
            valuesType: 'String',
            valuesNullable: true,
            hbaseFamily: 'h'
        })) ;

        var protocolStatus = Ext.create('GoraExplorer.model.avro.EntityNode', {
            name: 'ProtocolStatus',
            doc: 'A nested container representing data captured from web server responses.'
        }) ;

        protocolStatus.appendChild(Ext.create('GoraExplorer.model.avro.IntNode', {
            name: 'code',
            doc: 'A protocol response code which can be one of SUCCESS - content was retrieved without errors, FAILED - Content was not retrieved. Any further errors may be indicated in args, PROTO_NOT_FOUND - This protocol was not found. Application may attempt to retry later, GONE - Resource is gone, MOVED - Resource has moved permanently. New url should be found in args, TEMP_MOVED - Resource has moved temporarily. New url should be found in args., NOTFOUND - Resource was not found, RETRY - Temporary failure. Application may retry immediately., EXCEPTION - Unspecified exception occured. Further information may be provided in args., ACCESS_DENIED - Access denied - authorization required, but missing/incorrect., ROBOTS_DENIED - Access denied by robots.txt rules., REDIR_EXCEEDED - Too many redirects., NOTFETCHING - Not fetching., NOTMODIFIED - Unchanged since the last fetch., WOULDBLOCK - Request was refused by protocol plugins, because it would block. The expected number of milliseconds to wait before retry may be provided in args., BLOCKED - Thread was blocked http.max.delays times during fetching.',
            nullable: false,
            defaultIntValue: 0
        })) ;

        protocolStatus.appendChild(Ext.create('GoraExplorer.model.avro.ArrayNode', {
            name: 'args',
            doc: 'Optional arguments supplied to compliment and/or justify the response code.',
            nullable: false,
            itemsType: 'String'
        })) ;

        var parseStatus = Ext.create('GoraExplorer.model.avro.EntityNode', {
            name: 'ParseStatus',
            doc: 'A nested container representing parse status data captured from invocation of parsers on fetch of a WebPage'
        }) ;

        parseStatus.appendChild(Ext.create('GoraExplorer.model.avro.IntNode', {
            name: 'majorCode',
            doc: 'Major parsing status\' including NOTPARSED (Parsing was not performed), SUCCESS (Parsing succeeded), FAILED (General failure. There may be a more specific error message in arguments.)',
            nullable: false,
            defaultIntValue: 0
        })) ;

        parseStatus.appendChild(Ext.create('GoraExplorer.model.avro.ArrayNode', {
            name: 'args',
            doc: 'Optional arguments supplied to compliment and/or justify the parse status code.',
            nullable: false,
            itemsType: 'String'
        })) ;

        var testTreeRootNode = Ext.create('GoraExplorer.model.avro.EntitiesNode') ;
        testTreeRootNode.appendChild(webpageSchema) ;
        testTreeRootNode.appendChild(protocolStatus) ;
        testTreeRootNode.appendChild(parseStatus) ;

        return testTreeRootNode ;
    },

    /**
     * Returns a new the example data Object of a webpage
     * @return {Object}
     */
    getWebpageTestData: function() {

        return {
            "baseUrl": "http://feedly.com/i/subscription/feed/http:/feeds.weblogssl.com/genbeta",
            "status": 2,
            "fetchTime": 1478548565861,
            "prevFetchTime": 1475956528685,
            "fetchInterval": 2592000,
            "retriesSinceFetch": 0,
            "modifiedTime": 0,
            "prevModifiedTime": 0,
            "protocolStatus": {
                "code": 1,
                "args": [
                    'ArrayValue1',
                    'ArrayValue2',
                    'ArrayValue3'
                ],
                "lastModified": 0
            },
            "content": "PCFkb2N0eXBlIGh0bWw+DQo8aHRtbCBpdGVtc2NvcGUgaXRlbXR5cGU9Imh0dHA6Ly9zY2hlbWEub3JnL1dlYlBhZ2UiPg0KICAgIDxoZWFkPg0KCQkNCgkJPG1ldGEgY2hhcnNldD11dGYtOD4NCiAgICAgICAgDQogICAgICAgIDwhLS0gUGxhY2UgdGhpcyBkYXRhIGJldHdlZW4gdGhlIDxoZWFkPiB0YWdzIG9mIHlvdXIgd2Vic2l0ZSAtLT4NCgkJPHRpdGxlPmZlZWRseS4gUmVhZCBtb3JlLCBrbm93IG1vcmUuPC90aXRsZT4NCgkJPG1ldGEgbmFtZT0iZGVzY3JpcHRpb24iIGNvbnRlbnQ9ImZlZWRseTogb3JnYW5pemUsIHJlYWQgYW5kIHNoYXJlIHdoYXQgbWF0dGVycyB0byB5b3UuIiAvPg0KCQk8bGluayByZWw9Im1hc2staWNvbiIgaHJlZj0iLy9zMy5mZWVkbHkuY29tL2ltZy9mZWVkbHktaWNvbi5zdmciIGNvbG9yPSIjMmJiMjRjIj4NCgkJPGxpbmsgcmVsPSJhbHRlcm5hdGUiIHR5cGU9ImFwcGxpY2F0aW9uL3Jzcyt4bWwiIHRpdGxlPSJmZWVkbHkgYmxvZyIgaHJlZj0iaHR0cDovL2Jsb2cuZmVlZGx5LmNvbS9mZWVkLyIgLz4NCgkJDQoJCTwhLS0gR29vZ2xlIEF1dGhvcnNoaXAgYW5kIFB1Ymxpc2hlciBNYXJrdXAgLS0+DQoJCTxsaW5rIHJlbD0iYXV0aG9yIiBocmVmPSJodHRwczovL3BsdXMuZ29vZ2xlLmNvbS8rZmVlZGx5L3Bvc3RzIi8+DQoJCTxsaW5rIHJlbD0icHVibGlzaGVyIiBocmVmPSJodHRwczovL3BsdXMuZ29vZ2xlLmNvbS8rZmVlZGx5Ij4NCgkJDQoJCTwhLS0gU2NoZW1hLm9yZyBtYXJrdXAgZm9yIEdvb2dsZSsgLS0+DQoJCTxtZXRhIG5hbWU9Imdvb2dsZS1zaXRlLXZlcmlmaWNhdGlvbiIgY29udGVudD0ic2VDQzVTbF91bko1c09WZmZtRXI1bHFFMG1JZWpXajBVN3g0ekMxQ2MxUSIgLz4NCgkJPG1ldGEgbmFtZT0iZ29vZ2xlLXNpdGUtdmVyaWZpY2F0aW9uIiBjb250ZW50PSJ6UzgtNHVaMjVWc1NWZGxLTzA5MzJJamVUQW9DLXNfcVdnNlZQZlNpVE5jIiAvPg0KCQk8bWV0YSBpdGVtcHJvcD0ibmFtZSIgY29udGVudD0iZmVlZGx5OiBvcmdhbml6ZSwgcmVhZCBhbmQgc2hhcmUgd2hhdCBtYXR0ZXJzIHRvIHlvdS4iPg0KCQk8bWV0YSBpdGVtcHJvcD0iZGVzY3JpcHRpb24iIGNvbnRlbnQ9IkZlZWRseSBjb25uZWN0cyB5b3UgdG8gdGhlIGluZm9ybWF0aW9uIGFuZCBrbm93bGVkZ2UgeW91IGNhcmUgYWJvdXQuIFdlIGhlbHAgeW91IGdldCBtb3JlIG91dCBvZiB5b3Ugd29yaywgZWR1Y2F0aW9uLCBob2JiaWVzIGFuZCBpbnRlcmVzdHMuIFRoZSBmZWVkbHkgcGxhdGZvcm0gbGV0cyB5b3UgZGlzY292ZXIgc291cmNlcyBvZiBxdWFsaXR5IGNvbnRlbnQsIGZvbGxvdyBhbmQgcmVhZCBldmVyeXRoaW5nIHRob3NlIHNvdXJjZXMgcHVibGlzaCB3aXRoIGVhc2UgYW5kIG9yZ2FuaXplIGV2ZXJ5dGhpbmcgaW4gb25lIHBsYWNlLiI+DQoJCTxtZXRhIGl0ZW1wcm9wPSJpbWFnZSIgY29udGVudD0iaHR0cDovL3MzLmZlZWRseS5jb20vaW1nL2ZlZWRseS01MTIucG5nIj4NCgkJDQoJCTwhLS0gVHdpdHRlciBDYXJkIGRhdGEgLS0+DQoJCTxtZXRhIG5hbWU9InR3aXR0ZXI6Y2FyZCIgY29udGVudD0ic3VtbWFyeV9sYXJnZV9pbWFnZSI+DQoJCTxtZXRhIG5hbWU9InR3aXR0ZXI6c2l0ZSIgY29udGVudD0iQGZlZWRseSI+DQoJCTxtZXRhIG5hbWU9InR3aXR0ZXI6dGl0bGUiIGNvbnRlbnQ9ImZlZWRseTogb3JnYW5pemUsIHJlYWQgYW5kIHNoYXJlIHdoYXQgbWF0dGVycyB0byB5b3UuIj4NCgkJPG1ldGEgbmFtZT0idHdpdHRlcjpkZXNjcmlwdGlvbiIgY29udGVudD0iRmVlZGx5IGNvbm5lY3RzIHlvdSB0byB0aGUgaW5mb3JtYXRpb24gYW5kIGtub3dsZWRnZSB5b3UgY2FyZSBhYm91dC4gV2UgaGVscCB5b3UgZ2V0IG1vcmUgb3V0IG9mIHlvdSB3b3JrLCBlZHVjYXRpb24sIGhvYmJpZXMgYW5kIGludGVyZXN0cy4gVGhlIGZlZWRseSBwbGF0Zm9ybSBsZXRzIHlvdSBkaXNjb3ZlciBzb3VyY2VzIG9mIHF1YWxpdHkgY29udGVudCwgZm9sbG93IGFuZCByZWFkIGV2ZXJ5dGhpbmcgdGhvc2Ugc291cmNlcyBwdWJsaXNoIHdpdGggZWFzZSBhbmQgb3JnYW5pemUgZXZlcnl0aGluZyBpbiBvbmUgcGxhY2UuIj4NCgkJPG1ldGEgbmFtZT0idHdpdHRlcjpjcmVhdG9yIiBjb250ZW50PSJAZmVlZGx5Ij4NCgkJPCEtLSBUd2l0dGVyIHN1bW1hcnkgY2FyZCB3aXRoIGxhcmdlIGltYWdlIG11c3QgYmUgYXQgbGVhc3QgMjgweDE1MHB4IC0tPg0KCQk8bWV0YSBuYW1lPSJ0d2l0dGVyOmltYWdlOnNyYyIgY29udGVudD0iaHR0cDovL2ZlZWRseS5jb20vaW5kZXguaHRtbCI+DQoJCQ0KCQk8IS0tIE9wZW4gR3JhcGggZGF0YSAtLT4gICAgICAgIA0KICAgICAgICANCiAgICAgICAgPG1ldGEgcHJvcGVydHk9Im9nOnVybCIgY29udGVudD0iaHR0cDovL2ZlZWRseS5jb20vaW5kZXguaHRtbCI+DQogICAgICAgIDxtZXRhIHByb3BlcnR5PSJvZzpzaXRlX25hbWUiIGNvbnRlbnQ9ImZlZWRseSI+DQogICAgICAgIDxtZXRhIHByb3BlcnR5PSJvZzppbWFnZSIgY29udGVudD0iaHR0cDovL3MzLmZlZWRseS5jb20vaW1nL2ZlZWRseS01MTIucG5nIj4NCiAgICAgICAgPG1ldGEgcHJvcGVydHk9Im9nOnRpdGxlIiBjb250ZW50PSJmZWVkbHk6IG9yZ2FuaXplLCByZWFkIGFuZCBzaGFyZSB3aGF0IG1hdHRlcnMgdG8geW91LiI+DQogICAgICAgIDxtZXRhIHByb3BlcnR5PSJvZzpkZXNjcmlwdGlvbiIgY29udGVudD0iRmVlZGx5IGNvbm5lY3RzIHlvdSB0byB0aGUgaW5mb3JtYXRpb24gYW5kIGtub3dsZWRnZSB5b3UgY2FyZSBhYm91dC4gV2UgaGVscCB5b3UgZ2V0IG1vcmUgb3V0IG9mIHlvdSB3b3JrLCBlZHVjYXRpb24sIGhvYmJpZXMgYW5kIGludGVyZXN0cy4gVGhlIGZlZWRseSBwbGF0Zm9ybSBsZXRzIHlvdSBkaXNjb3ZlciBzb3VyY2VzIG9mIHF1YWxpdHkgY29udGVudCwgZm9sbG93IGFuZCByZWFkIGV2ZXJ5dGhpbmcgdGhvc2Ugc291cmNlcyBwdWJsaXNoIHdpdGggZWFzZSBhbmQgb3JnYW5pemUgZXZlcnl0aGluZyBpbiBvbmUgcGxhY2UuIj4NCg0KCQk8bWV0YSBpZD0idnAiIG5hbWU9InZpZXdwb3J0IiAJIGNvbnRlbnQ9IndpZHRoPWRldmljZS13aWR0aCwgaW5pdGlhbC1zY2FsZT0xLjAsIG1heGltdW0tc2NhbGU9MS4wLCB1c2VyLXNjYWxhYmxlPW5vIiAvPg0KDQogICAgICAgIDxsaW5rIGhyZWY9Imh0dHBzOi8vcGx1cy5nb29nbGUuY29tLzExNDI3OTU4MDQ4MDExNjExNjk1Ny8iIHJlbD0icHVibGlzaGVyIj4NCiAgICAgICAgPGxpbmsgcmVsPSJjaHJvbWUtd2Vic3RvcmUtaXRlbSIgaHJlZj0iaHR0cHM6Ly9jaHJvbWUuZ29vZ2xlLmNvbS93ZWJzdG9yZS9kZXRhaWwvaGlwYmZpamlucGNnZm9nYW9wbWdlaGllZ2FjYmhtb2IiPg0KCQk8bGluayByZWw9ImNocm9tZS13ZWJzdG9yZS1pdGVtIiBocmVmPSJodHRwczovL2Nocm9tZS5nb29nbGUuY29tL3dlYnN0b3JlL2RldGFpbC9oZGhibHBoY2RqY2ljZWZuZWFwa2htbGVhcGZhb2NpaCI+IA0KCQk8bGluayByZWw9ImNocm9tZS13ZWJzdG9yZS1pdGVtIiBocmVmPSJodHRwczovL2Nocm9tZS5nb29nbGUuY29tL3dlYnN0b3JlL2RldGFpbC9jY21rYmdrbmFwb2tjamFibWlhaW1pcGllcGdwZ2JjbyI+IA0KCQk8bWV0YSBuYW1lPSJnb29nbGUtc2l0ZS12ZXJpZmljYXRpb24iIGNvbnRlbnQ9InpTOC00dVoyNVZzU1ZkbEtPMDkzMklqZVRBb0Mtc19xV2c2VlBmU2lUTmMiIC8+DQoNCiAgICAgICAgPGJhc2UgaHJlZj0nLy9zMy5mZWVkbHkuY29tL3Byb2R1Y3Rpb24vaGVhZC8nIC8+DQoNCiAgICAgICAgPGxpbmsgcmVsPSJzdHlsZXNoZWV0IiB0eXBlPSJ0ZXh0L2NzcyIgaHJlZj0iLy9zMy5mZWVkbHkuY29tL3dlYi8zMC4wLjEyNDYvZngvZmVlZGx5LWZ4LmNzcyI+DQoJDQoJCTxzdHlsZT4NCgkJCUBmb250LWZhY2UNCgkJCXsNCgkJCQlmb250LWZhbWlseQk6ICdTbGFiIFNlcmlmJzsNCgkJCQlzcmMJCQk6IHVybCggJy8vczMuZmVlZGx5LmNvbS9mb250cy10dGYvR3VhcmRpYW5UZXh0RWd5cC1SZWd1bGFyLVdlYi50dGYnICkgZm9ybWF0KCAndHJ1ZXR5cGUnICksDQoJCQkJCQkJICB1cmwoICcvL3MzLmZlZWRseS5jb20vZm9udHMtd29mZi9HdWFyZGlhblRleHRFZ3lwLVJlZ3VsYXItV2ViLndvZmYnICkgZm9ybWF0KCAnd29mZicgKTsNCgkJCQlmb250LXdlaWdodCA6IG5vcm1hbDsNCgkJCQlmb250LXN0eWxlIAk6IG5vcm1hbDsNCgkJCX0NCgkJDQoJCQlAZm9udC1mYWNlDQoJCQl7DQoJCQkJZm9udC1mYW1pbHkJOiAnU2xhYiBTZXJpZic7DQoJCQkJc3JjCQkJOiB1cmwoICcvL3MzLmZlZWRseS5jb20vZm9udHMtdHRmL0d1YXJkaWFuVGV4dEVneXAtUmVndWxhckl0LVdlYi50dGYnICkgZm9ybWF0KCAndHJ1ZXR5cGUnICksDQoJCQkJCQkJICB1cmwoICcvL3MzLmZlZWRseS5jb20vZm9udHMtd29mZi9HdWFyZGlhblRleHRFZ3lwLVJlZ3VsYXJJdC1XZWIud29mZicgKSBmb3JtYXQoICd3b2ZmJyApOw0KCQkJCWZvbnQtd2VpZ2h0IDogbm9ybWFsOw0KCQkJCWZvbnQtc3R5bGUgCTogaXRhbGljOw0KCQkJfQ0KCQkNCgkJCUBmb250LWZhY2UNCgkJCXsNCgkJCQlmb250LWZhbWlseQk6ICdTbGFiIFNlcmlmJzsNCgkJCQlzcmMJCQk6IHVybCggJy8vczMuZmVlZGx5LmNvbS9mb250cy10dGYvR3VhcmRpYW5UZXh0RWd5cC1Cb2xkLVdlYi50dGYnICkgZm9ybWF0KCAndHJ1ZXR5cGUnICksDQoJCQkJCQkJICB1cmwoICcvL3MzLmZlZWRseS5jb20vZm9udHMtd29mZi9HdWFyZGlhblRleHRFZ3lwLUJvbGQtV2ViLndvZmYnICkgZm9ybWF0KCAnd29mZicgKTsNCgkJCQlmb250LXdlaWdodCA6IGJvbGQ7DQoJCQkJZm9udC1zdHlsZSAJOiBub3JtYWw7DQoJCQl9DQoJCQ0KCQkJQGZvbnQtZmFjZQ0KCQkJew0KCQkJCWZvbnQtZmFtaWx5CTogJ1NsYWIgU2VyaWYnOw0KCQkJCXNyYwkJCTogdXJsKCAnLy9zMy5mZWVkbHkuY29tL2ZvbnRzLXR0Zi9HdWFyZGlhblRleHRFZ3lwLUJvbGRJdC1XZWIudHRmJyApIGZvcm1hdCggJ3RydWV0eXBlJyApLA0KCQkJCQkJCSAgdXJsKCAnLy9zMy5mZWVkbHkuY29tL2ZvbnRzLXdvZmYvR3VhcmRpYW5UZXh0RWd5cC1Cb2xkSXQtV2ViLndvZmYnICkgZm9ybWF0KCAnd29mZicgKTsNCgkJCQlmb250LXdlaWdodCA6IGJvbGQ7DQoJCQkJZm9udC1zdHlsZSAJOiBpdGFsaWM7DQoJCQl9DQoNCgkJCSNmZWVkbHlCbG9ja2VkDQoJCQl7DQoJCQkJdHJhbnNpdGlvbjogb3BhY2l0eSAxcyBsaW5lYXI7DQoJCQl9DQoJCQkNCgkJCS5jZW50ZXINCgkJICAgIHsNCgkJICAgICAgICBtYXJnaW46IDAgYXV0bzsNCgkJICAgIH0NCgkJICAgIA0KCQkJI2ZlZWRseVNwbGFzaHNjcmVlbg0KCQkJew0KICAJCSAJCXBvc2l0aW9uCTogYWJzb2x1dGU7DQogICAgCQkJdG9wCQkJOiA1MCU7DQogICAgCQkJbGVmdAkJOiAwOw0KICAgIAkJCXRyYW5zZm9ybQk6IHRyYW5zbGF0ZVgoMCkgdHJhbnNsYXRlWSgtNTAlKTsNCiAgICAJCQlmb250LXNpemUJOiA0NHB4Ow0KICAgIAkJCWNvbG9yCQk6ICNENkQ2RDY7DQogICAgCQkJZm9udC13ZWlnaHQJOiBib2xkOw0KICAgIAkJCWZvbnQtZmFtaWx5IDogaGVsdmV0aWNhLCBzYW5zLXNlcmlmOw0KICAgIAkJCWxldHRlci1zcGFjaW5nOiAtMC4wNGVtOw0KICAgIAkJCXRleHQtYWxpZ24JOiBjZW50ZXI7DQogICAgCQkJd2lkdGgJCTogMTAwJTsNCgkJCX0NCgkJCQ0KCQkJI2ZlZWRseVN0YXRpY1Byb2dyZXNzQmFyDQoJCQl7DQogIAkJIAkJcG9zaXRpb24JOiBhYnNvbHV0ZTsNCiAgICAJCQl0b3AJCQk6IDA7DQogICAgCQkJbGVmdAkJOiAwOw0KICAgIAkJCXdpZHRoCQk6IDFweDsNCiAgICAJCQloZWlnaHQJCTogM3B4Ow0KICAgIAkJCWJhY2tncm91bmQtY29sb3I6ICNDQ0M7DQogICAgCQkJdGV4dC1hbGlnbgk6IGNlbnRlcjsNCgkJCX0NCgkJCQ0KCQkJI2ZlZWRseVN0YXRpY1Byb2dyZXNzTWVzc2FnZQ0KCQkJew0KCQkJCWZvbnQtc2l6ZQk6IDEzcHg7DQoJCQkJY29sb3IJCTogI0NGQ0ZDRjsNCiAgICAJCQlmb250LWZhbWlseSA6IGhlbHZldGljYSwgc2Fucy1zZXJpZjsNCiAgICAJCQlsaW5lLWhlaWdodAk6IDRlbTsNCiAgICAJCQlsZXR0ZXItc3BhY2luZzogMGVtOw0KICAgIAkJCWZvbnQtd2VpZ2h0IDogbm9ybWFsOw0KICAgIAkJCWRpc3BsYXkJCTogbm9uZTsNCgkJCX0NCgkJPC9zdHlsZT4NCg0KICAgICAgICA8c2NyaXB0Pg0KCSAgICAJdmFyIGZlZWRseVQwID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7DQoJICAgIAkNCgkgICAgCWZ1bmN0aW9uIGZyb21GZWVkbHlUMCgpew0KCSAgICAJCXJldHVybiBuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIGZlZWRseVQwOw0KCSAgICAJfQ0KCSAgICAJDQoJICAgIAlmdW5jdGlvbiBnZXRGZWVkbHlDb29raWUoKQ0KCSAgICAJew0KCSAgICAJCXRyeQ0KCSAgICAJCXsNCgkJICAgCQkJdmFyIG5hbWVFUSA9ICJmZWVkbHkuc2Vzc2lvbj0iOw0KCQkgICAJCQl2YXIgY2EgPSBkb2N1bWVudC5jb29raWUuc3BsaXQoJzsnKTsNCgkJICAgCQkJZm9yKHZhciBpPTA7aSA8IGNhLmxlbmd0aDtpKyspIHsNCgkJICAgCQkJCXZhciBjID0gY2FbaV07DQoJCSAgIAkJCQl3aGlsZSAoYy5jaGFyQXQoMCk9PScgJykgYyA9IGMuc3Vic3RyaW5nKDEsYy5sZW5ndGgpOw0KCQkgICAJCQkJaWYgKGMuaW5kZXhPZihuYW1lRVEpID09IDApIHJldHVybiBjLnN1YnN0cmluZyhuYW1lRVEubGVuZ3RoLGMubGVuZ3RoKTsNCgkJICAgCQkJfQ0KCQkgICAJCQlyZXR1cm4gbnVsbDsNCgkgICAgCQl9DQoJICAgIAkJY2F0Y2goIGUgKQ0KCSAgICAJCXsNCgkgICAgCQkJcmV0dXJuIG51bGw7DQoJICAgIAkJfQ0KCSAgICAJfQ0KDQogICAgCQl2YXIgZXJyb3JUaW1lciA9IHdpbmRvdy5zZXRUaW1lb3V0KCBmdW5jdGlvbigpew0KICAgICAgICAgICAgICAgCS8vIFRoZSBwYWdlIGhhcyBzdWNjZXNzZnVsbHkgbG9hZGVkLg0KICAgICAgICAgICAgCXZhciBibG9ja2VkRWxlbWVuZCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCAiZmVlZGx5QmxvY2tlZCIgKTsNCiAgICAgICAgICAgICAgICBpZiggYmxvY2tlZEVsZW1lbmQgPT0gbnVsbCApDQogICAgICAgICAgICAgICAgCXJldHVybjsNCiAgICAgICAgICAgIAkNCiAgICAgICAgICAgIAloYW5kbGVFcnJvcigpDQogICAgICAgICAgICB9LA0KICAgICAgICAgICAgMTUwMDANCiAgICAgICAgICAgICk7DQogICAgICAgICAgICANCiAgICAgICAgICAgIGZ1bmN0aW9uIGhhbmRsZUVycm9yKCkNCiAgICAgICAgICAgIHsNCiAgICAgICAgICAgIAlpZiggZXJyb3JUaW1lciAhPSBudWxsICkNCiAgICAgICAgICAgIAl7DQogICAgICAgICAgICAJCXdpbmRvdy5jbGVhclRpbWVvdXQoIGVycm9yVGltZXIgKTsNCiAgICAgICAgICAgIAkJZXJyb3JUaW1lciA9IG51bGw7DQogICAgICAgICAgICAJfQ0KDQogICAgICAgICAgICAJLy8gaWYgbG9hZGluZyBmcm9tIGh0dHAsIHRoZW4gcmVkaXJlY3QgdG8gaHR0cHMgdG8gYXZvaWQgQ0RODQogICAgICAgICAgICAJaWYoIGRvY3VtZW50LmxvY2F0aW9uLnByb3RvY29sID09ICJodHRwIiApDQogICAgICAgICAgICAJew0KICAgICAgICAgICAgCQlkb2N1bWVudC5sb2NhdGlvbiA9ICJodHRwczovL2ZlZWRseS5jb20vIjsNCiAgICAgICAgICAgIAkJcmV0dXJuOw0KICAgICAgICAgICAgCX0NCiAgICAgICAgICAgIAllbHNlDQogICAgICAgICAgICAJew0KICAgICAgICAgICAgICAgICAgIAkvLyBUaGUgcGFnZSBoYXMgc3VjY2Vzc2Z1bGx5IGxvYWRlZC4NCiAgICAgICAgICAgICAgICAJdmFyIGJsb2NrZWRFbGVtZW5kID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoICJmZWVkbHlCbG9ja2VkIiApOw0KICAgICAgICAgICAgICAgICAgICBpZiggYmxvY2tlZEVsZW1lbmQgIT0gbnVsbCApDQoJICAgICAgICAgICAgCQlibG9ja2VkRWxlbWVuZC5zdHlsZS5vcGFjaXR5ID0gMTsNCiAgICAgICAgICAgICAgICAgICAgDQogICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCAiZmVlZGx5U3RhdGljUHJvZ3Jlc3NCYXIiICkuc3R5bGUuZGlzcGxheSA9ICJub25lIjsNCiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoICJmZWVkbHlTcGxhc2hzY3JlZW4iICkuc3R5bGUuZGlzcGxheSA9ICJub25lIjsNCiAgICAgICAgICAgIAl9DQogICAgICAgICAgICB9DQogICAgICAgICAgICAgICAgICAgICAgICANCgkJCXRyeQ0KICAgICAgICAJew0KICAgICAgICAJCS8vIE1BUFBJTkcgVkVSU0lPTiA0DQoJCSAgICAJLy8NCgkJICAgIAkvLyBpZiB0aGlzIGlzIGFuIG9sZCBVUkwsIHdlIHNob3VsZCByZWRpcmVjdCBpdCB0byB0aGUgbmV3IFVSTCBmb3JtYXQNCgkJICAgIAkvLyBPTEQgaHR0cDovL2ZlZWRseS5jb20vYmV0YSN3ZWxjb21lDQoJCSAgICAJLy8gTkVXIGh0dHA6Ly9mZWVkbHkuY29tL2Ivd2VsY29tZQ0KCQkgICAgCQ0KCQkgICAgCXZhciBoID0gd2luZG93LmxvY2F0aW9uLmhhc2g7ICAgICAgICAgICAgIC8vIGV4YW1wbGUgI3dlbGNvbWUNCgkJICAgIAlpZiggaC5pbmRleE9mKCAiIyIgKSA9PSAwICkNCgkJICAgIAkJaCA9IGguc2xpY2UoIDEgKTsNCgkJICAgIAkNCgkJICAgIAl2YXIgcCA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZTsgICAgICAgICAvLyBleGFtcGxlIC8gL2JldGEgL2Rldi5odG1sIC9pbmRleC5odG1sDQoJCSAgICAJDQoJCSAgICAJdmFyIGhvc3QgPSB3aW5kb3cubG9jYXRpb24uaG9zdDsgICAgICAgICAgLy8gZXhhbXBsZSBmZWVkbHkuY29tIG9yIGNsb3VkLmZlZWRseS5jb20NCgkJICAgIAkNCgkJICAgIAl2YXIgcHJvdG9jb2wgPSB3aW5kb3cubG9jYXRpb24ucHJvdG9jb2w7ICAvLyBleGFtcGxlIGh0dHA6IG9yIGh0dHBzOg0KCQkgICAgCSAgICAgICAgICAgIAkNCgkJICAgIAkvLyBDQVNFIDAgLSBPbGQgaG9zdHMgKGV4YW1wbGU6IGNsb3VkLmZlZWRseS5jb20gb3Igd3d3LmZlZWRseS5jb20pDQoJCSAgICAJaWYoIHdpbmRvdy5sb2NhdGlvbi5ob3N0ICE9ICJmZWVkbHkuY29tIiAmJiB3aW5kb3cubG9jYXRpb24uaG9zdCAhPSAic2FuZGJveC5mZWVkbHkuY29tIiApDQoJCSAgICAJew0KCQkJCQl3aW5kb3cubG9jYXRpb24gPSB3aW5kb3cubG9jYXRpb24ucHJvdG9jb2wgKyAiLy9mZWVkbHkuY29tIiArIHAgKyB3aW5kb3cubG9jYXRpb24uaGFzaDsNCgkJICAgIAl9DQoJCSAgICAJZWxzZSBpZiggcCA9PSAiL2hvbWUiICkNCgkJICAgIAl7DQoJCQkJCXdpbmRvdy5sb2NhdGlvbiA9IHdpbmRvdy5sb2NhdGlvbi5wcm90b2NvbCArICIvL2ZlZWRseS5jb20vaS8iICsgaDsNCgkJICAgIAl9DQoJCSAgICAJLy8gQ0FTRSAxIC0gV2UgaGF2ZSBhbiBvbGQgaGFzaCBVUkwNCgkJICAgIAllbHNlIGlmKCBoLmxlbmd0aCA+IDAgKQ0KCQkgICAgCXsNCgkJICAgIAkJY29uc29sZS5sb2coICJbZmVlZGx5XSBuZWVkIHRvIG1hcCBoYXNoOiIgKyBoICsgIiBwYXRobmFtZToiICsgcCApOw0KCQkgICAgCQlzd2l0Y2goIHAgKQ0KCQkgICAgCQl7DQoJCSAgICAJCQljYXNlICIvYmV0YSI6DQoJCSAgICAJCQkJd2luZG93LmxvY2F0aW9uLmhyZWYgPSB3aW5kb3cubG9jYXRpb24ucHJvdG9jb2wgKyAiLy9mZWVkbHkuY29tL2IvIiArIGg7DQoJCSAgICAJCQkJYnJlYWs7DQoJCSAgICAJCQkJDQoJCSAgICAJCQljYXNlICIvYWxwaGEuaHRtbCI6DQoJCSAgICAJCQkJd2luZG93LmxvY2F0aW9uLmhyZWYgPSB3aW5kb3cubG9jYXRpb24ucHJvdG9jb2wgKyAiLy9mZWVkbHkuY29tL2EvIiArIGg7DQoJCSAgICAgICAgCQkJYnJlYWs7DQoJCSAgICAJCQkJDQoJCSAgICAJCQljYXNlICIvZGV2Lmh0bWwiOg0KCQkgICAgCQkJCXdpbmRvdy5sb2NhdGlvbi5ocmVmID0gd2luZG93LmxvY2F0aW9uLnByb3RvY29sICsgIi8vZmVlZGx5LmNvbS9kLyIgKyBoOw0KCQkgICAgICAgIAkJCWJyZWFrOw0KCQkgICAgCQkJCQ0KCQkgICAgCQkJY2FzZSAiL2hvbWUiOg0KCQkgICAgCQkJY2FzZSAiL2luZGV4Lmh0bWwiOg0KCQkgICAgCQkJY2FzZSAiLyI6DQoJCSAgICAJCQkJd2luZG93LmxvY2F0aW9uLmhyZWYgPSB3aW5kb3cubG9jYXRpb24ucHJvdG9jb2wgKyAiLy9mZWVkbHkuY29tL2kvIiArIGg7DQoJCSAgICAgICAgCQkJYnJlYWs7DQoJCQ0KCQkgICAgCQkJZGVmYXVsdDoNCgkJICAgIAkJCQkvLyBXZSBtdXN0IGJlIGluIGEgY2FzZSB3aGVyZSANCgkJICAgIAkJCQkvLyBodHRwOi8vZmVlZGx5LmNvbS9pL3N1YnNjcmlwdGlvbi9mZWVkLy4uLi4NCgkJICAgIAkJCQkvLyB3aGVyZSB0aGUgZmVlZCBpZCBoYXMgYSBoYXNoLg0KCQkgICAgCQl9DQoJCSAgICAJfQ0KCQkgICAgCWVsc2UgaWYoIHAuaW5kZXhPZiggIi5odG1sIiApID4gMCApDQoJCQkJew0KCQkJICAgIAkvLyBDQVNFIDIgLSBPbGQgcGF0aG5hbWVzDQoJCQkgICAJCXN3aXRjaCggcCApDQoJCQkgICAJCXsNCgkJCSAgIAkJCWNhc2UgIi9hbHBoYS5odG1sIjoNCgkJCSAgIAkJCQl3aW5kb3cubG9jYXRpb24uaHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5wcm90b2NvbCArICIvL2ZlZWRseS5jb20vYSI7DQoJCQkgICAgICAgCQkJYnJlYWs7DQoJCQkgICAJCQkJDQoJCQkgICAJCQljYXNlICIvZGV2Lmh0bWwiOg0KCQkJICAgCQkJCXdpbmRvdy5sb2NhdGlvbi5ocmVmID0gd2luZG93LmxvY2F0aW9uLnByb3RvY29sICsgIi8vZmVlZGx5LmNvbS9kIjsNCgkJCSAgICAgICAJCQlicmVhazsNCgkJCSAgIAkJCQkNCgkJCSAgIAkJCWNhc2UgIi9pbmRleC5odG1sIjoNCgkJCSAgIAkJCQl3aW5kb3cubG9jYXRpb24uaHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5wcm90b2NvbCArICIvL2ZlZWRseS5jb20vIjsNCgkJCSAgICAgICAJCQlicmVhazsNCgkJCQ0KCQkJICAgCQkJZGVmYXVsdDoNCgkJCSAgIAkJfQ0KCQkJCX0NCgkJICAgIAkNCgkJICAgIH0NCgkJICAgIGNhdGNoKCBlICkNCgkJICAgIHsNCgkJICAgICAgICAvLyBpZ25vcmUNCgkJICAgICAgICBjb25zb2xlLmxvZyggIltmZWVkbHldIHVybCBtYXBwaW5nIGZhaWxlZDoiICsgZS5uYW1lICsgIiAtLSAiICsgZS5tZXNzYWdlICkNCgkJICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9ICJodHRwczovL2ZlZWRseS5jb20vIjsNCgkJICAgIH0NCg0KCQkgICAgdmFyIGZlZWRseVByb2dyZXNzQ291bnRlciAgPSAwOw0KCQkgICAgdmFyIGZlZWRseVByb2dyZXNzRHVyYXRpb24gPSAxMDAwOw0KCQkgICAgdmFyIGZlZWRseVByb2dyZXNzU3RlcHMJICAgPSA1MDsNCiAgICAgICAgICAgIHZhciBmZWVkbHlQcm9ncmVzc0ludGVydmFsID0gd2luZG93LnNldEludGVydmFsKCBmdW5jdGlvbigpew0KICAgICAgICAgICAgCQ0KICAgICAgICAgICAgCWlmKCB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUuaW5kZXhPZiggIi9wcm8iICkgPT0gMiApDQogICAgICAgICAgICAJew0KICAgICAgICAgICAgCQl3aW5kb3cuY2xlYXJJbnRlcnZhbCggZmVlZGx5UHJvZ3Jlc3NJbnRlcnZhbCApOw0KICAgICAgICAgICAgCQlyZXR1cm47DQogICAgICAgICAgICAJfQ0KDQogICAgICAgICAgICAJZmVlZGx5UHJvZ3Jlc3NDb3VudGVyKys7DQogICAgICAgICAgICAJDQogICAgICAgICAgICAJaWYoIGZlZWRseVByb2dyZXNzQ291bnRlciA9PSAxICkNCiAgICAgICAgICAgIAl7DQogICAgICAgICAgICAJCXZhciBwYkVsZW0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggImZlZWRseVN0YXRpY1Byb2dyZXNzQmFyIiApOw0KICAgICAgICAgICAgCQlpZiggcGJFbGVtICE9IG51bGwgKQ0KICAgICAgICAgICAgCQkJcGJFbGVtLnN0eWxlLmRpc3BsYXkgPSAiYmxvY2siOw0KICAgICAgICAgICAgCQkNCiAgICAgICAgICAgIAkJdmFyIHBzRWxlbSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCAiZmVlZGx5U3BsYXNoc2NyZWVuIiApOw0KICAgICAgICAgICAgCQlpZiggcHNFbGVtICE9IG51bGwgKQ0KICAgICAgICAgICAgCQkJcHNFbGVtLnN0eWxlLmRpc3BsYXkgPSAiYmxvY2siOw0KDQogICAgICAgICAgICAJCXZhciB3bUVsZW0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggImZlZWRseVN0YXRpY1Byb2dyZXNzTWVzc2FnZSIgKTsNCiAgICAgICAgICAgIAkJaWYoIHdtRWxlbSAhPSBudWxsICkNCiAgICAgICAgICAgIAkJew0KICAgICAgICAgICAgICAgIAkJdmFyIGZjID0gZ2V0RmVlZGx5Q29va2llKCk7DQogICAgICAgICAgICAgICAgCQlpZiggZmMgIT0gbnVsbCApDQogICAgICAgICAgICAgICAgCQl7DQogICAgICAgICAgICAgICAgCQkJd21FbGVtLmlubmVySFRNTCA9ICJXZWxjb21lIGJhY2shIjsNCiAgICAgICAgICAgICAgICAJCX0NCiAgICAgICAgICAgICAgICAJCWVsc2UNCiAgICAgICAgICAgICAgICAJCXsNCiAgICAgICAgICAgICAgICAJCQl3bUVsZW0uaW5uZXJIVE1MID0gIldlbGNvbWUhIjsNCiAgICAgICAgICAgICAgICAJCX0NCiAgICAgICAgICAgIAkJfQ0KICAgICAgICAgICAgCX0NCiAgICAgICAgICAgIAkNCiAgICAgICAgICAgIAlpZiggZmVlZGx5UHJvZ3Jlc3NDb3VudGVyID4gZmVlZGx5UHJvZ3Jlc3NTdGVwcyApDQogICAgICAgICAgICAJew0KICAgICAgICAgICAgCQl3aW5kb3cuY2xlYXJJbnRlcnZhbCggZmVlZGx5UHJvZ3Jlc3NJbnRlcnZhbCApOw0KICAgICAgICAgICAgCQlyZXR1cm47DQogICAgICAgICAgICAJfQ0KICAgICAgICAgICAgCQ0KICAgICAgICAgICAgCXZhciBwYkVsZW0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggImZlZWRseVN0YXRpY1Byb2dyZXNzQmFyIiApOw0KICAgICAgICAgICAgCWlmKCBwYkVsZW0gPT0gbnVsbCApDQogICAgICAgICAgICAJCXdpbmRvdy5jbGVhckludGVydmFsKCBmZWVkbHlQcm9ncmVzc0ludGVydmFsICk7DQogICAgICAgICAgICAJZWxzZSANCiAgICAgICAgICAgIAkJcGJFbGVtLnN0eWxlLndpZHRoID0gcGFyc2VJbnQgKCBmZWVkbHlQcm9ncmVzc0NvdW50ZXIgLyBmZWVkbHlQcm9ncmVzc1N0ZXBzICogd2luZG93LmlubmVyV2lkdGggKSArICJweCI7DQogICAgICAgICAgICB9LA0KICAgICAgICAgICAgcGFyc2VJbnQoIGZlZWRseVByb2dyZXNzRHVyYXRpb24gLyBmZWVkbHlQcm9ncmVzc1N0ZXBzICkNCiAgICAgICAgICAgICk7DQoJCTwvc2NyaXB0Pg0KICAgIDwvaGVhZD4NCiAgICA8Ym9keSBpZD0iYm94IiBjbGFzcz0iaG9tZSI+DQogICAgCTxkaXYgaWQ9ImZlZWRseVN0YXRpY1Byb2dyZXNzQmFyIiBzdHlsZT0iZGlzcGxheTpub25lIj48L2Rpdj4NCiAgICAJPGRpdiBpZD0iZmVlZGx5U3BsYXNoc2NyZWVuIiBzdHlsZT0iZGlzcGxheTpub25lIj4NCgkgICAgCWZlZWRseQ0KCSAgICAJPGRpdiBpZD0iZmVlZGx5U3RhdGljUHJvZ3Jlc3NNZXNzYWdlIj4mbmJzcDs8L2Rpdj4NCiAgICAJPC9kaXY+ICAgCQ0KICAgIAk8ZGl2IGlkPSJmZWVkbHlCbG9ja2VkIiBzdHlsZT0ib3BhY2l0eTowOyBmb250LWZhbWlseTogc2Fucy1zZXJpZjsgdGV4dC1hbGlnbjpsZWZ0OyBjb2xvcjojNjY2OyBtYXgtd2lkdGg6NjAwcHg7IHBhZGRpbmctdG9wOiA2MHB4OyBwYWRkaW5nLWxlZnQ6IDQwcHg7IGxpbmUtaGVpZ2h0OiAxLjQ1ZW07IGZvbnQtc2l6ZTogMThweDsgcGFkZGluZy1yaWdodDo0MHB4OyBtYXJnaW4tbGVmdDphdXRvOyBtYXJnaW4tcmlnaHQ6YXV0byI+DQogICAgCQk8aW1nIGNsYXNzPSJjZW50ZXIiIGFsdD0iIiBzcmM9Ii8vczMuZmVlZGx5LmNvbS9pbWcvb29wcy5wbmciLz48YnI+PGJyPg0KICAgIAkJPHN0cm9uZyBzdHlsZT0iY29sb3I6IzMzMyI+SXMgZmVlZGx5IGJsb2NrZWQ/PC9zdHJvbmc+PGJyPg0KICAgIAkJRmVlZGx5IGlzIG5vdCBhYmxlIHRvIGxvYWQuIEl0IGlzIHByb2JhYmx5IGJlY2F1c2Ugb25lIG9mIHlvdXIgZXh0ZW5zaW9ucyBpcyBibG9ja2luZyBpdC4gSWYgeW91IHJ1biBBZGJsb2NrLCANCiAgICAJCUhUVFBTRXZlcnl3aGVyZSwgQXdlc29tZSBzY3JlZW5zaG90IGV0Yy4uIHBsZWFzZSBtYWtlIHN1cmUgdGhhdCBmZWVkbHkuY29tIGlzIHdoaXRlIGxpc3RlZC48YnI+DQogICAgCQk8YSBocmVmPSJodHRwczovL3BsdXMuZ29vZ2xlLmNvbS9jb21tdW5pdGllcy8xMTM2NDg1ODI3MzE4MzgxNzU2NDMvc3RyZWFtLzNlZDZkZTJkLTgzNTgtNDM5MC04NmIxLWE1OTJjYTZlOGNmNCI+QXNrIGEgcXVlc3Rpb248L2E+DQogICAgCTwvZGl2Pg0KICAgIAkNCgkJPCEtLSAgRkVFRExZIFVJIFNDUklQVFMgIC0tPg0KICAgICAgICA8c2NyaXB0IHNyYz0iLy9zMy5mZWVkbHkuY29tL3dlYi8zMC4wLjEyNDYvanMvd2ViLW1haW4tYnVuZGxlLmpzIiB0eXBlPSJ0ZXh0L2phdmFzY3JpcHQiIG9uZXJyb3I9ImhhbmRsZUVycm9yKCkiPjwvc2NyaXB0Pg0KICAgICAgICA8c2NyaXB0IHNyYz0iLy9zMy5mZWVkbHkuY29tL3dlYi8zMC4wLjEyNDYvanMvd2ViLXRlbXBsYXRlcy1idW5kbGUuanMiIHR5cGU9InRleHQvamF2YXNjcmlwdCIgb25lcnJvcj0iaGFuZGxlRXJyb3IoKSI+PC9zY3JpcHQ+DQoJCTwhLS0gIEZFRURMWSBVSSBTQ1JJUFRTICAtLT4NCiAgICA8L2JvZHk+DQo8L2h0bWw+DQo=",
            "contentType": "text/html",
            "prevSignature": null,
            "signature": null,
            "title": null,
            "text": null,
            "parseStatus": {
                "majorCode": 2,
                "minorCode": 200,
                "args": [
                    "org.apache.nutch.parse.ParseException: Unable to successfully parse content"
                ]
            },
            "score": 0,
            "reprUrl": null,
            "headers": {
                "CF-RAY": "2eec2776b7013c5f-CDG",
                "Server": "cloudflare-nginx",
                "Connection": "close",
                "Last-Modified": "Sat Oct 08 12:56:05 PDT 2016",
                "Pragma": "no-cache",
                "Date": "Sat, 08 Oct 2016 19:56:05 GMT",
                "Cache-Control": "no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0",
                "Content-Encoding": "gzip",
                "Set-Cookie": "__cfduid=d2e716391f9c8716829d539788b9fa3931475956565; expires=Sun, 08-Oct-17 19:56:05 GMT; path=/; domain=.feedly.com; HttpOnly",
                "Vary": "Accept-Encoding",
                "X-Feedly-Server": "ap3-sv2",
                "X-Feedly-Processing-Time": "0",
                "Content-Type": "text/html;charset=UTF-8"
            },
            "outlinks": {},
            "inlinks": {},
            "markers": {
                "dist": "1"
            },
            "metadata": {
                "CharEncodingForConversion": "dXRmLTg=",
                "OriginalCharEncoding": "dXRmLTg=",
                "_rs_": "AAABig==",
                "_csh_": "AAAAAA=="
            },
            "batchId": "1475956544-867262504",
            "sitemaps": {},
            "stmPriority": 0,
            "__id__": "com.feedly:http/i/subscription/feed/http:/feeds.weblogssl.com/genbeta"
        } ;

    }

}) ;