/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('Values card generator from Schema Nodes', function (t) {
    t.diag('Testing the generation of values card with ValuesCardGeneratorVisitor') ;

    var testTreeRootNode = null ;
    var testData = null ;
    var expectedWebPageCardDefinition = null ;
    var expectedMapTestOutput = null ;
    var expectedArrayTestOutput = null ;

    /**
     * The visitor to test
     * @type {GoraExplorer.view.tables.builders.avro.visitor.ValuesCardGeneratorVisitor}
     */
    var valuesCardGeneratorVisitor = null ;

    t.beforeEach(function(){
        testTreeRootNode = Tests.UnitTestsData.getWebpageSchemaTreeRootNode() ;
        valuesCardGeneratorVisitor = GoraExplorer.view.tables.builders.avro.visitor.ValuesCardGeneratorFactory.getGeneratorVisitor(testTreeRootNode) ;
        testData = Tests.UnitTestsData.getWebpageTestData() ;
        expectedWebPageCardDefinition = Tests.expect.UnitTests080.getExpectedWebpageCardDefinition() ;
        expectedMapTestOutput = Tests.expect.UnitTests080.getExpectedMapCardDefinition() ;
        expectedArrayTestOutput = Tests.expect.UnitTests080.getExpectedArrayCardDefinition() ;
    });

    t.it('Should generate a record card', function(t) {
        var webPageNode = testTreeRootNode.findChild('name', 'WebPage'),
            entityCardDefinition = valuesCardGeneratorVisitor.generateCard(webPageNode, 'recordData', testData) ;

        // Can't check all :\
        // ExtJS TreeNode recursivity must be avoided
        // Function definitons can't be compared neither
        Tests.expect.TestsHelper.clearExpectedDefinition(entityCardDefinition) ;

        t.expect(entityCardDefinition).toEqual(expectedWebPageCardDefinition) ;
        t.done();   // Optional, marks the correct exit point from the test

    }) ;

    t.it('Should generate a map card', function(t) {
        var webPageNode = testTreeRootNode.findChild('name', 'WebPage'),
            headersNode = webPageNode.findChild('name', 'headers'),
            headersCardDefinition = valuesCardGeneratorVisitor.generateCard(headersNode, 'recordData.headers', testData['headers']) ;

        Tests.expect.TestsHelper.clearExpectedDefinition(headersCardDefinition) ;
        t.expect(headersCardDefinition).toEqual(expectedMapTestOutput) ;
        t.done() ;
    }) ;

    t.it('Should generate an array card', function(t) {
        var protocolStatusNode = testTreeRootNode.findChild('name', 'ProtocolStatus'),
            argsNode = protocolStatusNode.findChild('name', 'args'),
            argsArrayDefinition = valuesCardGeneratorVisitor.generateCard(argsNode, 'recordData.protocolStatus.args', testData['protocolStatus']['args']) ;

        Tests.expect.TestsHelper.clearExpectedDefinition(argsArrayDefinition) ;
        t.expect(argsArrayDefinition).toEqual(expectedArrayTestOutput) ;
        t.done() ;
    }) ;

}) ;