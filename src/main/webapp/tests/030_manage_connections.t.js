/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('The application', function(t) {

    t.getExt().query = t.getExt().ComponentQuery.query ;

    t.it('Must log in as admin', function (t) {
        t.chain(
            {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
            {type: 'admin', target: '>> textfield[name=username]', clearExisting: true, desc: 'Typed username'},
            {type: 'admin', target: '>> textfield[name=password]', clearExisting: true, desc: 'Typed password'},
            {click: '>> button[text=' + _('Next') + ']', desc: 'Clicked Next'},
            {waitFor: 'CQNotVisible', args:'login', desc: 'The Login window hidden'}
        ) ;
    });

    var connectionsMenuOptionComponent = t.getExt().query('treelistitem[text="' + _('Connections') + '"]') ;

    t.it('Must create a new connection', function(t) {
        t.chain(
            function(next) {
                // The navigation menu bar at left must be expanded in order for the tests to work :\
                t.getExt().query('app-main')[0].getController().expandNavigationSize() ;
                next() ;
            },
            {click: connectionsMenuOptionComponent[0].getEl(), desc: 'Clicked on Connection main menu option'},
            function(next) {
                // Delete old connection if exists
                t.waitForComponentVisible('connectionsgrid', function() {
                    var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                        connectionsStore = connectionsGrid.getStore(),
                        connectionModel = connectionsStore.getData().find('name', 'NEW CONNECTION') ;

                    if (connectionModel !== null) {
                        connectionsStore.remove(connectionModel) ;
                        connectionsStore.sync(
                            GoraExplorer.applicationHelper.handleStoreSync({
                                successMessage: _('Connection saved successfully'),
                                failureMessage: _('Error saving the connection'),
                                success: function (batch, options) {
                                    next() ;
                                },
                                failure: function (batch, options) {
                                    t.fail('Could not delete an existing test connection')
                                }
                            }));
                    } else {
                        // Nothing to delete
                        next();
                    }
                })
            },
            {click: '>> button[text=' + _('Connection') + ']', desc: 'Clicked on menu Connection'},
            {click: '>> menuitem[text=' + _x('Connection','New') + ']', desc: 'Clicked on new connection option'},
            {type: 'NEW CONNECTION', target: '>> textfield[name=name]', clearExisting: true, desc: 'Typed new connection name'},
            {type: 'NEW DESCRIPTION', target: '>> textfield[name=description]', clearExisting: true, desc: 'Typed new connection description'},
            {click: '>> combobox[name=type]', desc: 'Clicked on connection type combobox'},
            function (next) {
                // Select HBASE connection type
                var typeCombo = t.getExt().query('combobox[name=type]')[0] ;
                t.click(typeCombo.getPicker().getNode(0), function() {
                    t.pass('Selected HBASE connection type') ;
                    next() ;
                }) ;
            },
            {
                type: 'hbase.client.autoflush.default=false\n' +
                'gora.datastore.scanner.caching=10\n' +
                'gora.datastore.default=org.apache.gora.hbase.store.HBaseStore',
                target: '>> textarea[name=goraProperties]',
                desc: 'Typed Gora Properties content'
            },
            {
                type: '<?xml version="1.0"?>\n'+
                '<configuration>\n'+
                '</configuration>',
                target: '>> textarea[name=hadoopConfiguration]',
                desc: 'Typed Hadoop Configuration content'
            },
            {click: '>> button[text=' + _('Create') + ']', desc: 'Clicked Create button'},
            function(next) {
                // Detect the new created connection when there are no phantom models
                var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                    connectionsStore = connectionsGrid.getStore() ;

                t.waitForFn( function() {
                    var connectionModel = connectionsStore.getData().find('name', 'NEW CONNECTION') ;
                    return (connectionModel !== null) && !connectionModel.phantom ;
                }, function () {
                    t.pass('New connection detected') ;
                })
            }
        ) ;
    }) ;

    t.it('Must update a connection', function(t) {
        t.chain(
            function(next) {
                // The navigation menu bar at left must be expanded in order for the tests to work :\
                t.getExt().query('app-main')[0].getController().expandNavigationSize() ;
                next() ;
            },
            {click: connectionsMenuOptionComponent[0].getEl(), desc: 'Clicked on Connection main menu option'},
            {waitForComponentVisible: 'connectionsgrid', desc: 'The connections grid is visible'},
            function(next) {
                // Find the test connection row
                var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                    connectionsStore = connectionsGrid.getStore(),
                    connectionModel = connectionsStore.getData().find('name', 'NEW CONNECTION') ;

                if (connectionModel === null) {
                    t.fail('Expected to find a test connection. Execute the suit from the beginning') ;
                } else {
                    var itemIndex = connectionsStore.indexOf(connectionModel) ;
                    var gridRow = t.getRow(connectionsGrid, itemIndex) ;
                    // Click the connection row
                    if (connectionsGrid.getSelectionModel().isSelected(connectionModel)) {
                        next() ;
                    } else {
                        // Row not selected
                        t.click(gridRow, next);
                    }
                }
            },
            {click: '>> button[text=' + _('Connection') + ']', desc: 'Clicked on menu Connection'},
            {click: '>> menuitem[text=' + _x('Connection','Edit') + ']', desc: 'Clicked on edit connection option'},
            {type: 'EDITED DESCRIPTION', target: '>> textfield[name=description]', clearExisting: true, desc: 'Typed a new description'},
            {click: '>> combobox[name=type]', desc: 'Clicked on connection type combobox'},
            function (next) {
                // Select KUDU connection type
                var typeCombo = t.getExt().query('combobox[name=type]')[0] ;
                t.click(typeCombo.getPicker().getNode(1), function() {
                    t.pass('Edited connection type') ;
                    next() ;
                }) ;
            },
            {
                type: 'Test text 1',
                target: '>> textarea[name=goraProperties]',
                desc: 'Typed Gora Properties content',
                clearExisting: true
            },
            {
                type: 'Test text 2',
                target: '>> textarea[name=hadoopConfiguration]',
                desc: 'Typed Hadoop Configuration content',
                clearExisting: true
            },
            {click: '>> button[text=' + _('Update') + ']', desc: 'Clicked Update button'},
            function(next) {
                // Wait until there are no records to update
                t.waitForFn(function(){
                    var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                        connectionsStore = connectionsGrid.getStore() ;

                    // True if there are no more records to update
                    return connectionsStore.getUpdatedRecords().length === 0 ;
                }, function() {
                    t.pass('Connection updated') ;
                    next();
                }) ;
            },
            function(next) {
                // Check the expected data
                var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                    connectionsStore = connectionsGrid.getStore(),
                    connectionModel = connectionsStore.getData().find('name', 'NEW CONNECTION') ;

                t.expect(connectionModel.get('description')).toBe('EDITED DESCRIPTION') ;
                t.expect(connectionModel.get('type')).toBe('KUDU') ;
                t.expect(connectionModel.get('goraProperties')).toBe('Test text 1') ;
                t.expect(connectionModel.get('hadoopConfiguration')).toBe('Test text 2') ;
                t.expect(connectionModel.dirty).toBeFalsy() ;
                t.expect(connectionModel.phantom).toBeFalsy() ;

            }
        ) ;
    }) ;

    t.it('Must delete an existing connection', function(t) {
        t.chain(
            function(next) {
                // The navigation menu bar at left must be expanded in order for the tests to work :\
                t.getExt().query('app-main')[0].getController().expandNavigationSize() ;
                next() ;
            },
            {click: connectionsMenuOptionComponent[0].getEl(), desc: 'Clicked on Connection main menu option'},
            {waitForComponentVisible: 'connectionsgrid', desc: 'The connections grid is visible'},
            function(next) {
                // Find the test connection row
                var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                    connectionsStore = connectionsGrid.getStore(),
                    connectionModel = connectionsStore.getData().find('name', 'NEW CONNECTION') ;

                if (connectionModel === null) {
                    t.fail('Expected to find a test connection. Execute the suit from the beginning') ;
                } else {
                    // Click the connection row if it is not already selected
                    var itemIndex = connectionsStore.indexOf(connectionModel) ;
                    var gridRow = t.getRow(connectionsGrid, itemIndex) ;
                    if (connectionsGrid.getSelectionModel().isSelected(connectionModel)) {
                        next() ;
                    } else {
                        // Row not selected
                        t.click(gridRow, next);
                    }
                }
            },
            {click: '>> button[text=' + _('Connection') + ']', desc: 'Clicked on menu Connection'},
            {click: '>> menuitem[text=' + _x('Connection','Delete') + ']', desc: 'Clicked on delete connection'},
            {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK'},
            function(next) {
                // Wait until the connection has been deleted in the server
                t.waitForFn(function(){
                    var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                        connectionsStore = connectionsGrid.getStore() ;

                    // True if removed records has been successfuly removed
                    return connectionsStore.getRemovedRecords().length === 0 ;
                }, function() {
                    t.pass('Test connection deleted') ;
                }) ;
            }
        ) ;
    }) ;

});