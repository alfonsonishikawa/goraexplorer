/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var harness = new Siesta.Harness.Browser.ExtJS;

harness.configure({
    title: 'Conjunto de tests de GoraExplorer',
    preload: [
        '../resources/js/deep-diff-0.3.8.min.js'
    ]
});

harness.start(
    {
        group  : 'Unit Tests',
        pageUrl: '../index.html?unittest',
        items  : [
            {
                url: '010_sanity.t.js',
                title: 'Basic sanity test',
                desc: 'Basic sanity tests taken from Siesta tutorial'
            },
            {
                url: '060_avro_schema_generator.t.js',
                title: 'Avro Schema Trees',
                desc: 'Generators and visitors for schema trees'
            },
            {
                url: '080_valuescards_generator.t.js',
                title: 'Values Cards generation',
                desc: 'Generators of values cards'
            },
            {
                url: '090_grid_columns_generator.t.js',
                title: 'Grid Columns generation',
                desc: 'Visitor generator of the grid columns definition for a table'
            }
        ],
        requires: [
            'GoraExplorer.model.avro.visitor.JsonSchemaGeneratorVisitor',
            'GoraExplorer.model.avro.builder.SchemaTreeBuilder',
            'GoraExplorer.model.avro.builder.HBaseSchemaTreeBuilder',
            'GoraExplorer.view.tables.builders.avro.visitor.ValuesCardGeneratorFactory',
            'GoraExplorer.model.avro.SchemaNode',
            'Tests.UnitTestsData',
            'Tests.expect.TestsHelper',
            'Tests.expect.UnitTests080',
            'Tests.expect.UnitTests090'
        ],
        preload: 'inherit'
    },
    {
        group : 'Application Tests',
        runCore : 'sequential',
        pageUrl: '../index.html#login',
        items : [
            {
                url: '020_login.t.js',
                title: 'Simple Login',
                pageUrl: '../index.html#login'
            },
            {
                url: '030_manage_connections.t.js',
                title: 'Manage connections',
                pageUrl: '../index.html#login'
            },
            {
                url: '050_manage_tables_configuration.t.js',
                title: 'Manage tables configuration',
                pageUrl: '../index.html#login'
            },
            {
                url: '070_manage_tables_wizard.t.js',
                title: 'Wizard to manage tables configuration',
                pageUrl: '../index.html#login',
                requires: ['Tests.expect.Tests070']
            },
            {
                url: '100_edit_table_row.t.js',
                title: 'Edit table rows',
                pageUrl: '../index.html#login'
            },
            {
                url: 'monkey_test.t.js',
                title: 'Monkey test',
                pageUrl: '../index.html#login'
            }
        ]
    }
);