/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

StartTest(function (t) {
    t.getExt().query = t.getExt().ComponentQuery.query ;
    t.describe('The application', function (t) {

        t.it('Must show the login window', function (t) {
            t.waitForCQVisible('login', function() { t.pass('The Login window is visible'); } ) ;
        });

        t.it('Must forbid an erroneous login attempt', function (t) {
            t.chain(
                {type: 'Eclipse', target: '>> textfield[name=username]', clearExisting: true},
                {type: 'wrongPass', target: '>> textfield[name=password]', clearExisting: true},
                {click: '>> button[text=' + _('Next') + ']'},
                {waitFor: 'componentQueryVisible', args: '>> label[text=' + _('Bad username/password') + ']'},
                {waitFor: 'CQVisible', args:'login'}
            );
        });

        t.it('Must log in as admin', function (t) {
            t.chain(
                {type: 'admin', target: '>> textfield[name=username]', clearExisting: true},
                {type: 'admin', target: '>> textfield[name=password]', clearExisting: true},
                {click: '>> button[text=' + _('Next') + ']'},
                {waitFor: 'CQNotVisible', args:'login'}
            )
        });
    })
});