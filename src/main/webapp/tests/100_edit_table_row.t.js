/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('The application', function(t) {

    t.getExt().query = t.getExt().ComponentQuery.query ;

    t.it('Must log in as admin', function (t) {
        t.chain(
            {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
            {type: 'admin', target: '>> textfield[name=username]', clearExisting: true, desc: 'Typed username'},
            {type: 'admin', target: '>> textfield[name=password]', clearExisting: true, desc: 'Typed password'},
            {click: '>> button[text=' + _('Next') + ']', desc: 'Clicked Next'},
            {waitFor: 'CQNotVisible', args:'login', desc: 'The Login window hidden'}
        ) ;
    });

    var connectionsMenuOptionComponent = t.getExt().query('treelistitem[text="' + _('Connections') + '"]') ;

    t.it('Must create a new row', function(t) {

        t.chain(
            function(next) {
                // The navigation menu bar at left must be expanded in order for the tests to work :\
                t.getExt().query('app-main')[0].getController().expandNavigationSize() ;
                next() ;
            },
            {click: connectionsMenuOptionComponent[0].getEl(), desc: 'Clicked on Connection main menu option'},
            {waitFor: 'rowsVisible', args: 'connectionsgrid' },
            {click: 'grid => .x-grid-cell:contains(HBase @ localhost)', desc: 'Selected the webpage connection'},
            {click: 'grid => .x-grid-cell:contains(webpage)', desc: 'Selected the webpage table'},
            {click: '>> button[text=' + _('Open table') + ']', desc: 'Clicked on open table button'},
            {waitFor: 'rowsVisible', args: 'tablegrid' },
            function(next) {
                // CHECK IF 'aaaa' ALREADY EXISTS
                var testRowQueryArray = t.compositeQuery('grid => .x-grid-cell:contains(aaaa)') ;
                if (t.getExt().isEmpty(testRowQueryArray)) {
                    // The test row does not exists, skip
                    next() ;
                } else {
                    // The test row exists => delete it
                    t.chain(
                        {click: 'grid => .x-grid-cell:contains(aaaa)', desc: 'Checking if the test row already exists'},
                        {click: '>> button[text=' + _('Delete row') + ']', desc: 'Clicked the button to delete the row'},
                        {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.yes + ']', desc: 'Clicked on OK to delete the test row'},
                        function(nextChecking) {
                            // Try to delete the row before continuing
                            t.waitForFn(function() {
                                var success = false ;
                                Ext.Array.each(t.getExt().query('toast'), function(toast) {
                                    if (toast.initialConfig.html === 'Row deleted successfully') {
                                        t.pass('Test row deleted') ;
                                        success = true ;
                                    }
                                }) ;
                                return success ;
                            }, next) ;
                        }
                    );
                }
            },
            {click: '>> button[text=' + _('Add new row') + ']', desc: 'Clicked Add new row'},
            {type: 'aaaa', target: 'messagebox => input', desc: 'Typed the new row key'},
            {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK to add the test row'},
            function(next) {
                t.waitForFn(function() {
                    var found = false ;
                    Ext.Array.each(t.getExt().query('toast'), function(toast) {
                        if (toast.initialConfig.html === 'Row added successfully') {
                            found = true ;
                        }
                    }) ;
                    return found ;
                }, next) ;
            }
        ) ;
    }) ;

    t.it('Must edit a row', function(t) {
        var componentRef = null ; // Generic variable to hold components to click
        t.chain(
            {click: 'grid => .x-grid-cell:contains(aaaa)', desc: 'Test row selected'},
            {click: 'valuesregion => .x-tool-pencil', desc: 'Entered row edit mode'},
            // Edit number field
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=status:]')[0].up().down('numberfield') ;
                t.type(componentRef, 5, next, undefined, undefined, true) ;
            },
            // Add record
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=protocolStatus (record):]')[0].up().down('additemvaluebutton') ;
                t.click(componentRef, next) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=protocolStatus (record):]')[0].up().down('show-childcard-button') ;
                t.click(componentRef, next) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=code:]')[0].up().down('numberfield') ;
                t.type(componentRef, 15, next, undefined, undefined, true) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=args (array:string):]')[0].up().down('show-childcard-button') ;
                t.click(componentRef, next) ;
            },
            {click: '>> button[text=' + _('Add item') + ']', desc: 'Clicked on add array item'},
            {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK to add an array element'},
            {click: '>> button[text=' + _('Add item') + ']', desc: 'Clicked on add array item'},
            {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK to add an array element'},
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=0:]')[0].up().down('textfield') ;
                t.type(componentRef, 'Value 0', next, undefined, undefined, true) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=1:]')[0].up().down('textfield') ;
                t.type(componentRef, 'Value 1', next, undefined, undefined, true) ;
            },
            {click: '>> button[text=' + _('recordData') + ']', desc: 'Clicked on the root node'},
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=headers (map:string):]')[0].up().down('show-childcard-button') ;
                t.click(componentRef, next) ;
            },
            {click: '>> button[text=' + _('Add item') + ']', desc: 'Clicked on add array item'},
            {type: 'key1', target: '>> messagebox textfield', clearExisting: true, desc: 'Typed map key'},
            {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK to add a map element'},
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=key1:]')[0].up().down('textfield') ;
                t.type(componentRef, 'Map value 1', next, undefined, undefined, true) ;
            },
            {click: '>> button[text=' + _('Add item') + ']', desc: 'Clicked on add array item'},
            {type: 'key2', target: '>> messagebox textfield', clearExisting: true, desc: 'Typed map key'},
            {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK to add a map element'},
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=key2:]')[0].up().down('textfield') ;
                t.type(componentRef, 'Map value 2', next, undefined, undefined, true) ;
            },
            {click: '>> tool[tooltip=' + _('Save changes') + ']', desc: 'Clicked save row'},
            function(next) {
                // Wait for save successful
                t.waitForFn(function() {
                    var found = false ;
                    Ext.Array.each(t.getExt().query('toast'), function(toast) {
                        if (toast.initialConfig.html === 'Data saved') {
                            t.pass('Saved edits') ;
                            found = true ;
                        }
                    }) ;
                    return found ;
                }, next) ;
            },
            // TODO More edits
            {click: 'valuesregion => .x-tool-pencil', desc: 'Entered row edit mode the second time'},
            {click: '>> button[text=' + _('recordData') + ']', desc: 'Clicked on the root node'},
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=protocolStatus (record):]')[0].up().down('show-childcard-button') ;
                t.click(componentRef, next) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=args (array:string):]')[0].up().down('show-childcard-button') ;
                t.click(componentRef, next) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=1:]')[0].up().down('textfield') ;
                t.type(componentRef, 'Updated value 1', next, undefined, undefined, true) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=0:]')[0].up().down('deletearrayitembutton');
                t.click(componentRef, next) ;
            },
            {click: '>> button[text=' + _('recordData') + ']', desc: 'Clicked on the root node'},
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=headers (map:string):]')[0].up().down('show-childcard-button') ;
                t.click(componentRef, next) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=key2:]')[0].up().down('textfield') ;
                t.type(componentRef, 'Updated map value 2', next, undefined, undefined, true) ;
            },
            function(next) {
                componentRef = t.getExt().query('valuescarditemlabel[text=key1:]')[0].up().down('deletemapitembutton');
                t.click(componentRef, next) ;
            },
            {click: '>> tool[tooltip=' + _('Save changes') + ']', desc: 'Clicked save row'},
            function(next) {
                // Wait for save successful
                t.waitForFn(function() {
                    var found = false ;
                    Ext.Array.each(t.getExt().query('toast'), function(toast) {
                        if (toast.initialConfig.html === 'Data saved') {
                            t.pass('Saved edits') ;
                            found = true ;
                        }
                    }) ;
                    return found ;
                }, next) ;
            },
        ) ;
    }) ;

    t.it('Must delete a row', function(t) {
        t.chain(
            {click: 'grid => .x-grid-cell:contains(aaaa)', desc: 'Selecting the test row'},
            {click: '>> button[text=' + _('Delete row') + ']', desc: 'Clicked the button to delete the row'},
            {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.yes + ']', desc: 'Clicked on OK to delete the test row'},
            function(next) {
                t.waitForFn(function() {
                    var found = false ;
                    Ext.Array.each(t.getExt().query('toast'), function(toast) {
                        if (toast.initialConfig.html === 'Row deleted successfully') {
                            t.pass('Test row deleted') ;
                            found = true ;
                        }
                    }) ;
                    return found ;
                }, next) ;
            }
        ) ;
    }) ;

});