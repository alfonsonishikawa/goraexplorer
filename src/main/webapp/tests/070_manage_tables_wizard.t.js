/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('The application', function(t) {

    var expectedCreatedSchema = Tests.expect.Tests070.getExpectedCreatedSchema() ;
    var expectedCreatedMapping = Tests.expect.Tests070.getExpectedCreatedMapping() ;

    t.getExt().query = t.getExt().ComponentQuery.query ;

    t.it('Must log in as admin', function (t) {
        t.chain(
            {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
            {type: 'admin', target: '>> textfield[name=username]', clearExisting: true, desc: 'Typed username'},
            {type: 'admin', target: '>> textfield[name=password]', clearExisting: true, desc: 'Typed password'},
            {click: '>> button[text=' + _('Next') + ']', desc: 'Clicked Next'},
            {waitFor: 'CQNotVisible', args:'login', desc: 'The Login window hidden'}
        ) ;
    });

    var connectionsMenuOptionComponent = t.getExt().query('treelistitem[text="' + _('Connections') + '"]') ;

    t.it('Must create a new table with the wizard', function(t) {

        var connectionsGrid = null,
            tablesGrid = null ;

        t.chain(
            function(next) {
                // The navigation menu bar at left must be expanded in order for the tests to work :\
                t.getExt().query('app-main')[0].getController().expandNavigationSize() ;
                next() ;
            },
            {click: connectionsMenuOptionComponent[0].getEl(), desc: 'Clicked on Connection main menu option'},
            function(next) {
                // select the test connection row
                connectionsGrid = t.getExt().query('connectionsgrid')[0] ;
                next() ;
            },
            { waitFor: 'rowsVisible', args: function() { return connectionsGrid; } },
            { click: function() { return t.getFirstRow(connectionsGrid) ; }, desc: 'Selected the first connection' },

            // Delete old table if exists from previous tests execution
            function(next) {
                tablesGrid = t.getExt().query('tablesgrid')[0] ;
                var tablesStore = tablesGrid.getStore(),
                    tableModel = tablesStore.getData().find('name', 'New Table') ;
                if (tableModel) {
                    var itemIndex = tablesStore.indexOf(tableModel) ;
                    t.chain(
                        {click: function() { return t.getRow(tablesGrid, itemIndex) ; }, desc: 'Selected the test table to delete'},
                        {click: '>> button[text=' + _('Table') + ']', desc: 'Clicked on menu Table'},
                        {click: '>> menuitem[text=' + _x('Table','Delete') + ']', desc: 'Clicked on delete table option'},
                        {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK to delete the test table'},
                        function(nextDelete) {
                            next() ;
                        }
                    )
                } else {
                    // Nothing to delete
                    next() ;
                }
            },

            {click: '>> button[text=' + _('Table') + ']', desc: 'Clicked on menu Table'},
            {click: '>> menuitem[text=' + _x('Table','New wizard') + ']', desc: 'Clicked on new table wizard option'},

            // Wizard: 1st step

            {type: 'New Table', target: '>> textfield[name=name]', clearExisting: true, desc: 'Typed new table name'},
            {type: 'NEW DESCRIPTION', target: '>> textfield[name=description]', clearExisting: true, desc: 'Typed new table description'},
            function(next) {
                // Wait until backendmetadata is loaded
                var wizardNewTable = t.getExt().query('wizard-newtable')[0],
                    backendMetadata = wizardNewTable.getViewModel().data.backendMetadata ;

                t.waitForFn( function() {
                    return !backendMetadata.isLoading() ;
                }, next) ;
            },
            {click: '>> combobox[name=nativeTable] => .x-form-trigger'},
            {click: 'li:contains(twitterraw)', desc: 'Selected the native table twitterraw'},
            {click: '>> wizard-newtable button[text=' + _('Next') + ']', desc: 'Clicked Next button to step 2'},

            // Wizard: 2nd step

            {click: 'treepanel => .x-grid-cell:contains(NewTable)', desc: 'Selected the Main Entity'},
            {type: 'Tweets', target: '>> wizard-newtable-schema textfield[name=name]', clearExisting: true, desc: 'Typed the Main Entity name'},
            {type: 'Tweets from Twitter', target: '>> textarea[name=documentation]', clearExisting: true, desc: 'Typed the Main Entity documentation'},
            {click: '>> button[text=' + _('Add Field') + ']'},
            {click: '.x-menu-item:contains(String)', desc: 'Added a new String field'},
            {click: 'treepanel => .x-grid-cell:contains(newStringField)', desc: 'Selected the status field'},
            {type: 'status', target: '>> wizard-newtable-schema textfield[name=name]', clearExisting: true},
            {type: 'Text', target: '>> wizard-newtable-schema textarea[name=documentation]', clearExisting: true},

            {click: '>> button[text=' + _('Add Field') + ']'},
            {click: '.x-menu-item:contains(Double)', desc: 'Added a new Double field'},
            {click: 'treepanel => .x-grid-cell:contains(newDoubleField)', desc: 'Selected the latitud field'},
            {type: 'latitud', target: '>> wizard-newtable-schema textfield[name=name]', clearExisting: true},
            {type: 'Latitude', target: '>> wizard-newtable-schema textarea[name=documentation]', clearExisting: true},
            {click: '>> checkbox[fieldLabel=' + _('Allow Null field')+']', desc: 'Clicked to allow null values in latitud'},
            {click: '>> checkbox[fieldLabel=' + _('Set Null as default field value')+']', desc: 'Clicked to set null as default value in latitud'},

            {click: '>> wizard-newtable button[text=' + _('Next') + ']', desc: 'Clicked Next button to step 3'},

            // Wizard: 3rd step

            {click: 'wizard-newtable-hbase-mapping => .x-grid-cell:contains(status)', desc: 'Selected the status field'},
            {click: '>> combobox[name=family] => .x-form-trigger'},
            {click: 'li:contains(default)', desc: 'Selected the "default" column family'},
            {type: 'status', target: '>> textfield[name=column]', clearExisting: true},

            {click: 'wizard-newtable-hbase-mapping => .x-grid-cell:contains(latitud)', desc: 'Selected the latitud field'},
            {click: '>> combobox[name=family] => .x-form-trigger'},
            {click: 'li:contains(default)', desc: 'Selected the "default" column family'},
            {type: 'latitud', target: '>> textfield[name=column]', clearExisting: true},

            {click: '>> wizard-newtable button[text=' + _('Next') + ']', desc: 'Clicked Next button to step 4'},

            // Wizard: 4th step

            function(next) {
                var schema = t.getExt().query('wizard-newtable-review textarea[name=schemaTextArea]')[0],
                    mapping = t.getExt().query('wizard-newtable-review textarea[name=mappingTextArea]')[0] ;

                t.expect(schema.getValue()).toEqual(expectedCreatedSchema) ;
                t.expect(mapping.getValue()).toEqual(expectedCreatedMapping) ;
                next() ;
            },
            {click: '>> wizard-newtable button[text=' + _('Finish') + ']', desc: 'Clicked Finish button'},
            function(next) {
                // Detect the new created table when there are no phantom models
                var tablesStore = tablesGrid.getStore() ;

                t.waitForFn( function() {
                    var tableModel = tablesStore.getData().find('name', 'New Table') ;
                    return (tableModel !== null) && !tableModel.phantom ;
                }, function () {
                    t.pass('New table detected') ;
                })
            }
        ) ;
    }) ;

    // EDIT a table with the wizard

    t.it('Must edit a table with the wizard', function(t) {

        var connectionsGrid = null,
            tablesGrid = null,
            tableIndex = null,
            tablesStore = null,
            tableModel = null ;

        var expectedUpdatedSchema = Tests.expect.Tests070.getExpectedUpdatedSchema() ;
        var expectedUpdatedMapping = Tests.expect.Tests070.getExpectedUpdatedMapping() ;

        t.chain(
            function(next) {
                // select the test connection row
                connectionsGrid = t.getExt().query('connectionsgrid')[0] ;
                next() ;
            },
            { waitFor: 'rowsVisible', args: function() { return connectionsGrid; } },
            function(next) {
                tablesGrid = t.getExt().query('tablesgrid')[0] ;
                tablesStore = tablesGrid.getStore() ;
                tableModel = tablesStore.getData().find('name', 'New Table') ;

                t.expect(tableModel).not.toBeNull() ;

                tableIndex = tablesStore.indexOf(tableModel) ;
                next() ;
            },
            {click: function() { return t.getRow(tablesGrid, tableIndex) ; }, desc: 'Selected the test table to delete'},
            {click: '>> button[text=' + _('Table') + ']', desc: 'Clicked on menu Table'},
            {click: '>> menuitem[text=' + _x('Table','Edit wizard') + ']', desc: 'Clicked on edit table with wizard option'},

            // 1st step

            {type: 'NEW DESCRIPTION - edited', target: '>> textfield[name=description]', clearExisting: true, desc: 'Typed new table description'},
            {click: '>> wizard-newtable button[text=' + _('Next') + ']', desc: 'Clicked Next button to step 2'},

            // 2nd step

            {click: 'wizard-newtable-schema => .x-tool-plus', desc: 'Added a new Entity' },
            {click: 'treepanel => .x-grid-cell:contains(NewEntity)', desc: 'Selected the new entity'},
            {type: 'Hashtag', target: '>> wizard-newtable-schema textfield[name=name]', clearExisting: true},
            {type: 'Hashtags', target: '>> wizard-newtable-schema textarea[name=documentation]', clearExisting: true},
            {click: '>> button[text=' + _('Add Field') + ']'},
            {click: '.x-menu-item:contains(String)', desc: 'Added a new String field'},
            {click: 'treepanel => .x-grid-cell:contains(newStringField)', desc: 'Selected the texto field'},
            {type: 'texto', target: '>> wizard-newtable-schema textfield[name=name]', clearExisting: true},

            {click: '>> button[text=' + _('Add Field') + ']'},
            {click: '.x-menu-item:contains(Int)', desc: 'Added a new Int field'},
            {click: 'treepanel => .x-grid-cell:contains(newIntField)', desc: 'Selected the inicio field'},
            {type: 'inicio', target: '>> wizard-newtable-schema textfield[name=name]', clearExisting: true},
            {click: '>> checkbox[fieldLabel=' + _('Allow Null field')+']', desc: 'Clicked to allow null values in inicio'},
            {click: '>> checkbox[fieldLabel=' + _('Set Null as default field value')+']', desc: 'Clicked to set null as default value in inicio'},

            {click: '>> button[text=' + _('Add Field') + ']'},
            {click: '.x-menu-item:contains(Int)', desc: 'Added a new Int field'},
            {click: 'treepanel => .x-grid-cell:contains(newIntField)', desc: 'Selected the fin field'},
            {type: 'fin', target: '>> wizard-newtable-schema textfield[name=name]', clearExisting: true},
            {click: '>> checkbox[fieldLabel=' + _('Allow Null field')+']', desc: 'Clicked to allow null values in inicio'},
            {click: '>> checkbox[fieldLabel=' + _('Set Null as default field value')+']', desc: 'Clicked to set null as default value in inicio'},

            {click: 'treepanel => .x-grid-cell:contains(Tweets)', desc: 'Selected Tweets entity'},
            {click: '>> button[text=' + _('Add Field') + ']'},
            {click: '.x-menu-item:contains(Map)', desc: 'Added a new hashtags map field'},
            {click: 'treepanel => .x-grid-cell:contains(newMapField)', desc: 'Selected the hashtag field'},
            {type: 'hashtags', target: '>> wizard-newtable-schema textfield[name=name]', clearExisting: true},
            {click: '>> combobox[name=valuesType] => .x-form-trigger'},
            {click: 'li:contains(record)', desc: 'Selected the map type: record'},
            {click: '>> combobox[name=valuesRecordName] => .x-form-trigger'},
            {click: 'li:contains(Hashtag)', desc: 'Selected the record type Hashtag'},

            {click: '>> wizard-newtable button[text=' + _('Next') + ']', desc: 'Clicked Next button to step 3'},

            // Wizard: 3rd step

            {click: 'wizard-newtable-hbase-mapping => .x-grid-cell:contains(hashtags)', desc: 'Selected the hashtags field'},
            {click: '>> combobox[name=family] => .x-form-trigger'},
            {click: 'li:contains(hashtags)', desc: 'Selected the "hashtags" column family'},

            {click: '>> wizard-newtable button[text=' + _('Next') + ']', desc: 'Clicked Next button to step 4'},

            // Wizard: 4th step

            function(next) {
                var schema = t.getExt().query('wizard-newtable-review textarea[name=schemaTextArea]')[0],
                    mapping = t.getExt().query('wizard-newtable-review textarea[name=mappingTextArea]')[0] ;

                t.expect(schema.getValue()).toEqual(expectedUpdatedSchema) ;
                t.expect(mapping.getValue()).toEqual(expectedUpdatedMapping) ;
                next() ;
            },
            {click: '>> wizard-newtable button[text=' + _('Finish') + ']', desc: 'Clicked Finish button'},
            function(next) {
                t.waitForFn( function() {
                    return !tableModel.dirty ;
                }, next) ;
            },
            function(next) {
                var schema = tableModel.get('avroSchema'),
                    mapping = tableModel.get('mapping') ;

                t.expect(schema).toEqual(expectedUpdatedSchema) ;
                t.expect(mapping).toEqual(expectedUpdatedMapping) ;
                next() ;
            },
            function(next) {
                // Cleanup: Delete the test table
                t.chain(
                    {click: '>> button[text=' + _('Table') + ']', desc: 'Clicked on menu Table'},
                    {click: '>> menuitem[text=' + _x('Table','Delete') + ']', desc: 'Clicked on delete table option'},
                    {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK to delete the test table'},
                    function(nextDelete) {
                        t.done() ;
                        next() ;
                    }
                )
            }

        ) ;

    }) ;

});