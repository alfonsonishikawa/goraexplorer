/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('Avro Schema Generator', function (t) {
    t.diag('Testing the Avro JSON schema generator visitor JsonSchemaGeneratorVisitor') ;

    var testTreeRootNode = null ;
    var expectedJsonSchema = {} ;

    t.beforeEach(function(){

        var webpageSchema = Ext.create('GoraExplorer.model.avro.EntityNode', {
            name: 'WebPage',
            doc: 'WebPage is the primary data structure in Nutch representing crawl data for a given WebPage at some point in time'
        }) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.StringNode', {
            name: 'baseUrl',
            doc: 'The original associated with this WebPage.',
            nullable: true,
            defaultNullValue: true,
            hbaseFamily: 'f',
            hbaseColumn: 'bas'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.IntNode', {
            name: 'status',
            doc: 'A crawl status associated with the WebPage, can be of value STATUS_UNFETCHED - WebPage was not fetched yet, STATUS_FETCHED - WebPage was successfully fetched, STATUS_GONE - WebPage no longer exists, STATUS_REDIR_TEMP - WebPage temporarily redirects to other page, STATUS_REDIR_PERM - WebPage permanently redirects to other page, STATUS_RETRY - Fetching unsuccessful, needs to be retried e.g. transient errors and STATUS_NOTMODIFIED - fetching successful - page is not modified',
            nullable: false,
            defaultIntValue: 0,
            hbaseFamily: 'f',
            hbaseColumn: 'st'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.LongNode', {
            name: 'fetchTime',
            doc: 'The system time in milliseconds for when the page was fetched.',
            nullable: false,
            defaultLongValue: 0,
            hbaseFamily:'f',
            hbaseColumn: 'ts'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.LongNode', {
            name: 'prevFetchTime',
            doc: 'The system time in milliseconds for when the page was last fetched if it was previously fetched which can be used to calculate time delta within a fetching schedule implementation',
            nullable: false,
            defaultLongValue: 0,
            hbaseFamily: 'f',
            hbaseColumn: 'pts'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.RecordNode', {
            name: 'protocolStatus',
            recordFieldTypeName: 'ProtocolStatus',
            nullable: true,
            defaultNullValue: true,
            hbaseFamily: 'f',
            hbaseColumn: 'prot'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.BytesNode', {
            name: 'content',
            doc: 'The entire raw document content e.g. raw XHTML',
            nullable: true,
            defaultNullValue: true,
            hbaseFamily: 'f',
            hbaseColumn: 'cnt'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.RecordNode', {
            name: 'parseStatus',
            recordFieldTypeName: 'ParseStatus',
            nullable: true,
            defaultNullValue: true,
            hbaseFamily: 'p',
            hbaseColumn: 'st'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.FloatNode', {
            name: 'score',
            doc: 'A score used to determine a WebPage\'s relevance within the web graph it is part of. This score may change over time based on graph characteristics.',
            nullable: false,
            defaultLongValue: 0,
            hbaseFamily: 's',
            hbaseColumn: 's'
        })) ;

        webpageSchema.appendChild(Ext.create('GoraExplorer.model.avro.MapNode', {
            name: 'headers',
            doc: 'Header information returned from the web server used to server the content which is subsequently fetched from. This includes keys such as TRANSFER_ENCODING, CONTENT_ENCODING, CONTENT_LANGUAGE, CONTENT_LENGTH, CONTENT_LOCATION, CONTENT_DISPOSITION, CONTENT_MD5, CONTENT_TYPE, LAST_MODIFIED and LOCATION.',
            nullable: false,
            valuesType: 'String',
            valuesNullable: true,
            hbaseFamily: 'h'
        })) ;

        var protocolStatus = Ext.create('GoraExplorer.model.avro.EntityNode', {
            name: 'ProtocolStatus',
            doc: 'A nested container representing data captured from web server responses.'
        }) ;

        protocolStatus.appendChild(Ext.create('GoraExplorer.model.avro.IntNode', {
            name: 'code',
            doc: 'A protocol response code which can be one of SUCCESS - content was retrieved without errors, FAILED - Content was not retrieved. Any further errors may be indicated in args, PROTO_NOT_FOUND - This protocol was not found. Application may attempt to retry later, GONE - Resource is gone, MOVED - Resource has moved permanently. New url should be found in args, TEMP_MOVED - Resource has moved temporarily. New url should be found in args., NOTFOUND - Resource was not found, RETRY - Temporary failure. Application may retry immediately., EXCEPTION - Unspecified exception occured. Further information may be provided in args., ACCESS_DENIED - Access denied - authorization required, but missing/incorrect., ROBOTS_DENIED - Access denied by robots.txt rules., REDIR_EXCEEDED - Too many redirects., NOTFETCHING - Not fetching., NOTMODIFIED - Unchanged since the last fetch., WOULDBLOCK - Request was refused by protocol plugins, because it would block. The expected number of milliseconds to wait before retry may be provided in args., BLOCKED - Thread was blocked http.max.delays times during fetching.',
            nullable: false,
            defaultIntValue: 0
        })) ;

        protocolStatus.appendChild(Ext.create('GoraExplorer.model.avro.ArrayNode', {
            name: 'args',
            doc: 'Optional arguments supplied to compliment and/or justify the response code.',
            nullable: false,
            itemsType: 'String'
        })) ;

        var parseStatus = Ext.create('GoraExplorer.model.avro.EntityNode', {
            name: 'ParseStatus',
            doc: 'A nested container representing parse status data captured from invocation of parsers on fetch of a WebPage'
        }) ;

        parseStatus.appendChild(Ext.create('GoraExplorer.model.avro.IntNode', {
            name: 'majorCode',
            doc: 'Major parsing status\' including NOTPARSED (Parsing was not performed), SUCCESS (Parsing succeeded), FAILED (General failure. There may be a more specific error message in arguments.)',
            nullable: false,
            defaultIntValue: 0
        })) ;

        parseStatus.appendChild(Ext.create('GoraExplorer.model.avro.ArrayNode', {
            name: 'args',
            doc: 'Optional arguments supplied to compliment and/or justify the parse status code.',
            nullable: false,
            itemsType: 'String'
        })) ;

        testTreeRootNode = Ext.create('GoraExplorer.model.avro.EntitiesNode') ;
        testTreeRootNode.appendChild(webpageSchema) ;
        testTreeRootNode.appendChild(protocolStatus) ;
        testTreeRootNode.appendChild(parseStatus) ;

        expectedJsonSchema = {
            "name": "WebPage",
            "type": "record",
            "namespace": "testnamespace",
            "doc": "WebPage is the primary data structure in Nutch representing crawl data for a given WebPage at some point in time",
            "fields": [
                {
                    "name": "baseUrl",
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "The original associated with this WebPage.",
                    "default": null
                },
                {
                    "name": "status",
                    "type": "int",
                    "doc": "A crawl status associated with the WebPage, can be of value STATUS_UNFETCHED - WebPage was not fetched yet, STATUS_FETCHED - WebPage was successfully fetched, STATUS_GONE - WebPage no longer exists, STATUS_REDIR_TEMP - WebPage temporarily redirects to other page, STATUS_REDIR_PERM - WebPage permanently redirects to other page, STATUS_RETRY - Fetching unsuccessful, needs to be retried e.g. transient errors and STATUS_NOTMODIFIED - fetching successful - page is not modified",
                    "default": 0
                },
                {
                    "name": "fetchTime",
                    "type": "long",
                    "doc": "The system time in milliseconds for when the page was fetched.",
                    "default": 0
                },
                {
                    "name": "prevFetchTime",
                    "type": "long",
                    "doc": "The system time in milliseconds for when the page was last fetched if it was previously fetched which can be used to calculate time delta within a fetching schedule implementation",
                    "default": 0
                },
                {
                    "name": "protocolStatus",
                    "type": [
                        "null",
                        {
                            "name": "ProtocolStatus",
                            "type": "record",
                            "namespace": "testnamespace",
                            "doc": "A nested container representing data captured from web server responses.",
                            "fields": [
                                {
                                    "name": "code",
                                    "type": "int",
                                    "doc": "A protocol response code which can be one of SUCCESS - content was retrieved without errors, FAILED - Content was not retrieved. Any further errors may be indicated in args, PROTO_NOT_FOUND - This protocol was not found. Application may attempt to retry later, GONE - Resource is gone, MOVED - Resource has moved permanently. New url should be found in args, TEMP_MOVED - Resource has moved temporarily. New url should be found in args., NOTFOUND - Resource was not found, RETRY - Temporary failure. Application may retry immediately., EXCEPTION - Unspecified exception occured. Further information may be provided in args., ACCESS_DENIED - Access denied - authorization required, but missing/incorrect., ROBOTS_DENIED - Access denied by robots.txt rules., REDIR_EXCEEDED - Too many redirects., NOTFETCHING - Not fetching., NOTMODIFIED - Unchanged since the last fetch., WOULDBLOCK - Request was refused by protocol plugins, because it would block. The expected number of milliseconds to wait before retry may be provided in args., BLOCKED - Thread was blocked http.max.delays times during fetching.",
                                    "default": 0
                                },
                                {
                                    "name": "args",
                                    "type": {
                                        "type": "array",
                                        "items": "string"
                                    },
                                    "doc": "Optional arguments supplied to compliment and/or justify the response code.",
                                    "default": [

                                    ]
                                }
                            ]
                        }
                    ],
                    "default": null
                },
                {
                    "name": "content",
                    "type": [
                        "null",
                        "bytes"
                    ],
                    "doc": "The entire raw document content e.g. raw XHTML",
                    "default": null
                },
                {
                    "name": "parseStatus",
                    "type": [
                        "null",
                        {
                            "name": "ParseStatus",
                            "type": "record",
                            "namespace": "testnamespace",
                            "doc": "A nested container representing parse status data captured from invocation of parsers on fetch of a WebPage",
                            "fields": [
                                {
                                    "name": "majorCode",
                                    "type": "int",
                                    "doc": "Major parsing status' including NOTPARSED (Parsing was not performed), SUCCESS (Parsing succeeded), FAILED (General failure. There may be a more specific error message in arguments.)",
                                    "default": 0
                                },
                                {
                                    "name": "args",
                                    "type": {
                                        "type": "array",
                                        "items": "string"
                                    },
                                    "doc": "Optional arguments supplied to compliment and/or justify the parse status code.",
                                    "default": [

                                    ]
                                }
                            ]
                        }
                    ],
                    "default": null
                },
                {
                    "name": "score",
                    "type": "float",
                    "doc": "A score used to determine a WebPage's relevance within the web graph it is part of. This score may change over time based on graph characteristics.",
                    "default": 0
                },
                {
                    "name": "headers",
                    "type": {
                        "type": "map",
                        "values": [
                            "null",
                            "string"
                        ]
                    },
                    "doc": "Header information returned from the web server used to server the content which is subsequently fetched from. This includes keys such as TRANSFER_ENCODING, CONTENT_ENCODING, CONTENT_LANGUAGE, CONTENT_LENGTH, CONTENT_LOCATION, CONTENT_DISPOSITION, CONTENT_MD5, CONTENT_TYPE, LAST_MODIFIED and LOCATION.",
                    "default": {

                    }
                }
            ]
        } ;
    });

    t.it('Should generated JSON from Avro Schema Tree', function(t) {
        var jsonSchemaGeneratorVisitor = Ext.create('GoraExplorer.model.avro.visitor.JsonSchemaGeneratorVisitor', 'testnamespace', testTreeRootNode),
            mainEntityNode = testTreeRootNode.findChild('name', 'WebPage'),
            generatedJson = jsonSchemaGeneratorVisitor.generateJsonSchema(mainEntityNode) ;

        Ext.log({level: 'info', dump: DeepDiff(expectedJsonSchema, generatedJson)}, 'Expected JSON vs Generated JSON') ;

        t.expect(generatedJson).toEqual(expectedJsonSchema) ;

        t.it('Should generate an Avro Schema Tree from a JSON', function(t) {
            var generatedSchemaTree = GoraExplorer.schemaTreeBuilder.buildFromJSON(expectedJsonSchema),
                jsonSchemaGeneratorVisitor = Ext.create('GoraExplorer.model.avro.visitor.JsonSchemaGeneratorVisitor', 'testnamespace', generatedSchemaTree),
                mainEntityNode = testTreeRootNode.findChild('name', 'WebPage'),
                generatedJson = jsonSchemaGeneratorVisitor.generateJsonSchema(mainEntityNode) ;

            // Comparing the two hierarchies (generatedSchemaTree <-> testTreeRootNode) is not a good idea since
            // both have model instances that will have differente values
            t.expect(generatedJson).toEqual(expectedJsonSchema) ;

            t.done();   // Optional, marks the correct exit point from the test
        }) ;

        t.done();   // Optional, marks the correct exit point from the test

    }) ;

    // =================================================================================================
    // Testing HBase Schema generators

    t.it('Should handle HBase mapping', function(t) {

        // Convert String to XmlDoc as expected value
        var expectedMappingString = '<?xml version="1.0" encoding="UTF-8"?>\n' +
            '<gora-odm>\n' +
            '  <table name="webpage">\n' +
            '    <family name="f" maxVersions="1"/>\n' +
            '    <family name="p" maxVersions="1"/>\n' +
            '    <family name="s" maxVersions="1"/>\n' +
            '    <family name="h" maxVersions="1"/>\n' +
            '  </table>\n' +
            '  <class table="webpage" keyClass="java.lang.String" name="testnamespace.WebPage">\n' +
            '    <field name="baseUrl" family="f" qualifier="bas"/>\n' +
            '    <field name="status" family="f" qualifier="st"/>\n' +
            '    <field name="fetchTime" family="f" qualifier="ts"/>\n' +
            '    <field name="prevFetchTime" family="f" qualifier="pts"/>\n' +
            '    <field name="protocolStatus" family="f" qualifier="prot"/>\n' +
            '    <field name="content" family="f" qualifier="cnt"/>\n' +
            '    <field name="parseStatus" family="p" qualifier="st"/>\n' +
            '    <field name="score" family="s" qualifier="s"/>\n' +
            '    <field name="headers" family="h"/>\n' +
            '  </class>\n' +
            '</gora-odm>' ;

        t.diag('Testing HBase Avro Schema generators') ;

        t.it('Should validate the HBase Mapping', function(t) {


            t.it('Should pass correct validation', function(t) {
                var hbaseMappingValidatorVisitor = Ext.create('GoraExplorer.model.avro.visitor.HBaseMappingValidatorVisitor', testTreeRootNode),
                    mainEntityNode = testTreeRootNode.findChild('name', 'WebPage'),
                    isValid = hbaseMappingValidatorVisitor.isValid(mainEntityNode) ;

                t.expect(isValid).toEqual(true) ;
                t.done() ;
            }) ;

            t.it('Should fail an incorrect validation', function(t) {
                var hbaseMappingValidatorVisitor = Ext.create('GoraExplorer.model.avro.visitor.HBaseMappingValidatorVisitor', testTreeRootNode),
                    mainEntityNode = testTreeRootNode.findChild('name', 'WebPage') ;

                // Collision at column:family -> collision of 'status' with 'baseUrl'
                mainEntityNode.findChild('name', 'status').set('hbaseColumn', 'bas') ;

                // Collision at column: -> collision of 'headers' with 'score'
                mainEntityNode.findChild('name', 'headers').set('hbaseFamily', 's') ;

                // Column defined at a Array/Map
                mainEntityNode.findChild('name', 'headers').set('hbaseColumn', 'shouldBeEmpty') ;

                mainEntityNode.findChild('name', 'content').set('hbaseFamily', '') ;
                mainEntityNode.findChild('name', 'content').set('hbaseColumn', '') ;

                var isValid = hbaseMappingValidatorVisitor.isValid(mainEntityNode),
                    errors = hbaseMappingValidatorVisitor.getErrors() ;

                t.expect(isValid).toEqual(false) ;
                t.expect(errors).toEqual(
                    {
                        columnDefinedErrorFields: ['headers'],
                        missingMappings: ['content'],
                        familyAndColumnsRegistry: {
                            'f:bas': ['baseUrl', 'status'],
                            's:s': ['score', 'headers']
                        }
                    }
                ) ;
                t.done() ;
            }) ;
            t.done() ;
        }) ;

        t.it('Should generate the HBase Mapping', function(t) {
            var jsonHBaseMappingGeneratorVisitor =
                    Ext.create('GoraExplorer.model.avro.visitor.HBaseMappingGeneratorVisitor',
                        'testnamespace',
                        'webpage',
                        'java.lang.String',
                        testTreeRootNode,
                        true /* prettyPrint */),
                mainEntityNode = testTreeRootNode.findChild('name', 'WebPage'),
                generatedMappingString = jsonHBaseMappingGeneratorVisitor.generateHBaseMapping(mainEntityNode) ;

            t.expect(generatedMappingString).toEqual(expectedMappingString) ;

            t.it('Should fill an Avro Schema Tree from the HBase Mapping XML', function(t) {
                var generatedSchemaTree = GoraExplorer.schemaTreeBuilder.buildFromJSON(expectedJsonSchema) ;

                GoraExplorer.hbaseSchemaTreeBuilder.addMappingInfo(generatedSchemaTree, expectedMappingString) ;

                var jsonHBaseMappingGeneratorVisitor =
                        Ext.create('GoraExplorer.model.avro.visitor.HBaseMappingGeneratorVisitor',
                            'testnamespace',
                            'webpage',
                            'java.lang.String',
                            generatedSchemaTree,
                            true /* prettyPrint */),
                    mainEntityNode = generatedSchemaTree.findChild('name', 'WebPage'),
                    generatedMappingString = jsonHBaseMappingGeneratorVisitor.generateHBaseMapping(mainEntityNode) ;

                // Comparing the two hierarchies (generatedSchemaTree <-> testTreeRootNode) is not a good idea since
                // both have model instances that will have differente values
                t.expect(generatedMappingString).toEqual(expectedMappingString) ;

                t.done();   // Optional, marks the correct exit point from the test
            }) ;

            t.done();   // Optional, marks the correct exit point from the test
        }) ;

        t.it('Should get the native table name from the mapping', function(t) {
            var nativeTableName = GoraExplorer.hbaseSchemaTreeBuilder.getNativeTableName(testTreeRootNode, expectedMappingString) ;

            t.expect(nativeTableName).toEqual('webpage') ;
            t.done() ;

        }) ;
    }) ;

}) ;