/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('Values card generator from Schema Nodes', function (t) {
    t.diag('Testing the generation of values card with ValuesCardGeneratorVisitor') ;

    var testTreeRootNode = null ;
    var testData = null ;
    var expectedColumnsDefinition = null ;

    /**
     * The visitor to test
     * @type {GoraExplorer.view.tables.builders.avro.visitor.GridColumnsGeneratorVisitor}
     */
    var gridColumnsGeneratorVisitor = Ext.create('GoraExplorer.view.tables.builders.avro.visitor.GridColumnsGeneratorVisitor') ;

    t.beforeEach(function(){
        testTreeRootNode = Tests.UnitTestsData.getWebpageSchemaTreeRootNode() ;
    });

    t.it('Should create a Grid\'s columns definition from a EntityNode', function(t) {
        var webPageNode = testTreeRootNode.findChild('name', 'WebPage'),
            columnsDefinition = gridColumnsGeneratorVisitor.generateColumns(webPageNode),
            expectedColumnsDefinition = Tests.expect.UnitTests090.getExpectedColumnsDefinition() ;

        // Can't check all :\
        // ExtJS TreeNode recursivity must be avoided
        // Function definitions can't be compared neither
        Tests.expect.TestsHelper.clearExpectedDefinition(columnsDefinition) ;

        t.expect(columnsDefinition).toEqual(expectedColumnsDefinition) ;

        t.done();   // Optional, marks the correct exit point from the test
    }) ;

}) ;