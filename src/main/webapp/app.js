/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */

// Just to hide some warnings we don't want by now
Ext.ariaWarn = Ext.emptyFn;

Ext.application({
    name: 'GoraExplorer',

    extend: 'GoraExplorer.Application',

    requires: [
        'GoraExplorer.view.main.Main'
    ],

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //
    mainView: location.search.match('unittest') ? null : 'GoraExplorer.view.main.Main'
	
    //-------------------------------------------------------------------------
    // Most customizations should be made to GoraExplorer.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});
