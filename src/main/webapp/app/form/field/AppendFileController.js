/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.form.field.AppendFileController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.appendfilefield',

    uses: [
        'GoraExplorer.helpers.toast.ToastHelper'
    ],

    /**
     * Given a the selected fileField file, appends the content of the selected field to the textarea configured
     * in the "appendTo" attribute configured in the fileField
     *
     * @param {Ext.form.field.File} fileField
     * @param {String} path
     * @param {Object} eOpts
     */
    onInputFileChange: function (fileField, path, eOpts) {
        var me = this,
            targetReferenceName = fileField.appendTo,
            // BUG at ExtJS: Can't use lookupReference because the button/filefiled don't have the method.
            // Workaround: Go through the upper viewController
            targetTextArea = fileField.lookupController(true).lookupReference(targetReferenceName) ;

        var reader = new FileReader();
        reader.onload = function (event) {
            targetTextArea.setValue(targetTextArea.getValue() + event.target.result);
            GoraExplorer.toastHelper.showInfoToast(_('File loaded')) ;
        };
        reader.onerror = function () {
            GoraExplorer.toastHelper.showErrorToast(_('Error loading file')) ;
        };
        reader.readAsText(fileField.fileInputEl.dom.files[0]);
    }

}) ;
