/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This model is specific for HBase backends. It holds the information about a specific native table at the backend.
 *
 * For HBase the information is:
 *
 * - List of column families
 *
 * To load the model there are 2 needed parameters:
 *
 * * connectionId
 * * tableName: from the list retrieved with {@link GoraExplorer.model.nativemetadata.BackendMetadata}
 *
 * ```
 * hbaseTableMetadata.load({
 *     params: {
 *         connectionId: me.lookupReference('connectionsGrid').selection.get('id'),
 *         tableName: 'twitterraw'
 *     },
 *     ...
 * ```
 *
 */
Ext.define('GoraExplorer.model.nativemetadata.HBaseTableMetadata', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'columnFamilies', type: 'auto' }
    ],

    proxy: {
        type: 'rest',
        url: GoraExplorer.applicationHelper.getRestUrl('connections/${connectionId}/metadata/table/${tableName}'),
        appendId: false
    }

});