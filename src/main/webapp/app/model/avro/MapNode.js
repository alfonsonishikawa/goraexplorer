/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.model.avro.MapNode', {
    extend: 'GoraExplorer.model.avro.FieldNode',

    requires: [
        'GoraExplorer.model.avro.*'
    ],

    fields: [

        /**
         * @override
         */
        { name: 'type', type: 'string', defaultValue: 'map' },

        /**
         * Values type the map hold (String -> X): 'boolean', 'int', 'long', 'float', 'double', 'string', 'nytes' or 'record'
         */
        { name: 'valuesType', type: 'string' },

        /**
         * If the values can be null
         */
        { name: 'valuesNullable', type: 'boolean' },

        /**
         * When the values are records, the name of the record that exists in Entities Tree Panel
         */
        { name: 'valuesRecordName', type: 'string'},

        /**
         * Flag that marks if the default value of the field is null
         */
        { name: 'defaultNullValue', type: 'boolean', defaultValue: false }

    ],

    /**
     *
     * @property {GoraExplorer.model.avro.SchemaNode} _valueTypeNode - Cache of the reusable node with the actual type.
     */
    _valueTypeNode: null,

    /**
     * @override
     */
    accept: function(visitor) {
        return visitor.visitMapNode(this) ;
    },

    /**
     * Returns a <i>reused</i> value type node of this map. Take care of not updating it (or only irrelevant properties).
     * @return {GoraExplorer.model.avro.SchemaNode}
     */
    getValueTypeNode: function() {
        var me = this ;

        if (me._valueTypeNode === null) {
            me._valueTypeNode = me.createValueTypeNode() ;
        }
        return me._valueTypeNode ;
    },

    /**
     * Given this MapNode, returns a new node instance of the item type
     * @return {GoraExplorer.model.avro.SchemaNode}
     */
    createValueTypeNode: function() {
        var me = this,
            nodeValuesType = me.get('valuesType'),
            nodeValuesNullable = me.get('valuesNullable'),
            nodeValuesRecordName = me.get('valuesRecordName'),
            tmpValueNode = Ext.create('GoraExplorer.model.avro.' + Ext.String.capitalize(nodeValuesType) + 'Node') ;

        // Sadly a little hack :( maybe schema tree should allow children nodes at array and maps, but that would
        // add other complexities (like a map in a map in a map and alike, now not supported)
        tmpValueNode.set('nullable', nodeValuesNullable) ;
        if (nodeValuesType === 'record') {
            tmpValueNode.set('recordFieldTypeName', nodeValuesRecordName) ;
        }

        return tmpValueNode ;
    }

}) ;