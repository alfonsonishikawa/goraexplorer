/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.model.avro.builder.HBaseSchemaTreeBuilder', {
    singleton: true,

    uses: [
        'GoraExplorer.helpers.toast.ToastHelper',
        'GoraExplorer.model.avro.ArrayNode',
        'GoraExplorer.model.avro.MapNode'
    ],

    /**
     * Given a XML mapping string, adds the mapping information to the Schema Tree at 'entitiesNode' parameter
     * @param {GoraExplorer.model.avro.EntitiesNode} entitiesNode
     * @param {String} mappingXmlString - The mapping XML in String format
     */
    addMappingInfo: function(entitiesNode, mappingXmlString) {
        var me = this,
            mainEntity = entitiesNode.firstChild,
            parser = new DOMParser(),
            mappingXmlDocument = parser.parseFromString(mappingXmlString, "text/xml") ;

        mainEntity.eachChild(function(fieldNode) {
            me._addFieldInfo(fieldNode, mappingXmlDocument) ;
        }) ;
    },

    /**
     * Returns the native table name for the main Entity defined in the entitiesNode
     * @param {GoraExplorer.model.avro.EntitiesNode} entitiesNode
     * @param {String} mappingXmlString - The mapping XML in String format
     */
    getNativeTableName: function(entitiesNode, mappingXmlString) {
        var me = this,
            mainEntity = entitiesNode.firstChild,
            entityName = mainEntity.get('name'),
            parser = new DOMParser(),
            mappingXmlDocument = parser.parseFromString(mappingXmlString, "text/xml"),
            nativeTableName = null ;

        mappingXmlDocument.querySelectorAll('class').forEach(
            function(classTagItem) {
                var nameAttr = classTagItem.getAttribute('name'),
                    tableAttr = classTagItem.getAttribute('table') ;
                if (nameAttr === entityName || Ext.String.endsWith(nameAttr, '.' + entityName)) {
                    nativeTableName = tableAttr ;
                }
            }
        ) ;

        return nativeTableName ;
    },

    /**
     *
     * @param fieldNode
     * @param {XmlDOM Document} mappingXmlDocument - The mapping XML document
     * @private
     */
    _addFieldInfo: function(fieldNode, mappingXmlDocument) {
        var me = this,
            fieldName = fieldNode.get('name'),
            fieldType = fieldNode.get('type'),
            xmlFieldElement = mappingXmlDocument.querySelector(Ext.String.format('field[name=\'{0}\']',fieldName)) ;

        if (xmlFieldElement === null) {
            Ext.raise(Ext.String.format(_('Expected to find mapping &lt;field&gt; tag for the field "{0}"'), fieldName)) ;
        }

        if ( ! xmlFieldElement.hasAttribute('family') ) {
            Ext.raise(Ext.String.format(_('The mapping for the field "{0}" has not been found in the mapping XML'), fieldName)) ;
        }
        fieldNode.set('hbaseFamily', xmlFieldElement.getAttribute('family')) ;

        if (xmlFieldElement.hasAttribute('qualifier')) {
            if( fieldNode instanceof GoraExplorer.model.avro.ArrayNode
                || fieldNode instanceof GoraExplorer.model.avro.MapNode
            ) {
                GoraExplorer.toastHelper.showWarnToast(Ext.String.format(_('The field "{0}" has a column/qualifier mapping, but it is of type "{1}". Ignoring'), fieldName, fieldType)) ;
            } else {
                fieldNode.set('hbaseColumn', xmlFieldElement.getAttribute('qualifier')) ;
            }
        }
    }

},
function (HBaseSchemaTreeBuilder){
    GoraExplorer.hbaseSchemaTreeBuilder = HBaseSchemaTreeBuilder ;
}) ;