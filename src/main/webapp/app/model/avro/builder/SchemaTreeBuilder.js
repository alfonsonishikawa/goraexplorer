/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Builder of Avro Schemas Entities Trees form JSON.
 */
Ext.define('GoraExplorer.model.avro.builder.SchemaTreeBuilder', {
    singleton: true,

    uses: [
        'GoraExplorer.helpers.toast.ToastHelper',
        'GoraExplorer.model.avro.BytesNode',
        'GoraExplorer.model.avro.EntitiesNode',
        'GoraExplorer.model.avro.EntityNode',
        'GoraExplorer.model.avro.FloatNode',
        'GoraExplorer.model.avro.IntNode',
        'GoraExplorer.model.avro.LongNode',
        'GoraExplorer.model.avro.RecordNode',
        'GoraExplorer.model.avro.StringNode'
    ],

    _allowedMapTypes: [ 'boolean', 'int', 'long', 'float', 'double', 'string', 'bytes', 'record' ],
    _forbiddenMapTypes: [ 'array', 'map', 'fixed' ],

    _allowedArrayTypes: [ 'boolean', 'int', 'long', 'float', 'double', 'string', 'bytes', 'record' ],
    _forbiddenArrayTypes: [ 'array', 'map', 'fixed' ],


    /**
     * {GoraExplorer.model.avro.EntitiesNode} _rootEntitiesNode -  The root of the Entities Tree
     */
    _rootEntitiesNode: null,

    /**
     *
     * @param {String/Object} schema - Given a schema in JSON String format, or Avro Object, generates the Schema Entities Tree.
     * @returns {GoraExplorer.model.avro.EntitiesNode} The Schema Entities Tree
     */
    buildFromJSON: function(schema) {
        var me = this ;

        if (Ext.isString(schema)) {
            schema = Ext.JSON.decode(schema) ;
        }

        me._rootEntitiesNode = Ext.create('GoraExplorer.model.avro.EntitiesNode') ;

        me._buildEntity(schema) ;
        return me._rootEntitiesNode ;
    },

    /**
     * Builds an EntityNode from an object schema
     * @param {Object | String} EntitySchema
     * @return {GoraExplorer.model.avro.EntityNode}
     * @private
     */
    _buildEntity: function(entitySchema) {
        var me = this ;

        // If we get a record name, we do nothing
        if (Ext.isString(entitySchema)) {
            return ;
        }

        if (entitySchema.type !== 'record') {
            Ext.raise(_('Error: expected a record schema, but found type "' + entitySchema.type + '"')) ;
        }

        // Check if it has been already generated
        var entityName = entitySchema.name ;
        if (me._rootEntitiesNode.findChild('name', entityName) !== null) {
            // Already generated: skip
            return ;
        }

        var entityNode = Ext.create('GoraExplorer.model.avro.EntityNode') ;
        entityNode.set('name', entitySchema.name) ;
        entityNode.set('namespace', entitySchema.namespace) ;
        entityNode.set('doc', entitySchema.doc) ;

        // Add to generated Set to avoid double generating
        me._rootEntitiesNode.appendChild(entityNode) ;

        Ext.Array.forEach(entitySchema.fields, function(fieldSchema) {
            var fieldNode = me._buildField(fieldSchema) ;
            entityNode.appendChild(fieldNode) ;
        }) ;

        return entityNode ;
    },

    _buildField: function(fieldSchema) {
        var me = this,
            fieldType = me._getFieldNonNullTypeName(fieldSchema) ;

        switch (fieldType) {
            case 'boolean': return me._buildBooleanField(fieldSchema) ;
            case 'int'    : return me._buildIntField(fieldSchema) ;
            case 'long'   : return me._buildLongField(fieldSchema) ;
            case 'float'  : return me._buildFloatField(fieldSchema) ;
            case 'double' : return me._buildDoubleField(fieldSchema) ;
            case 'string' : return me._buildStringField(fieldSchema) ;
            case 'bytes'  : return me._buildBytesField(fieldSchema) ;
            case 'record' : return me._buildRecordField(fieldSchema) ;
            case 'map'    : return me._buildMapField(fieldSchema) ;
            case 'array'  : return me._buildArrayField(fieldSchema) ;
            case 'fixed'  : Ext.raise(_('Unsupported field type "fixed"')) ;
            case 'null'   : Ext.raise(_('Unsupported field type "null" out of an union')) ;
            // Unrecognized value, maybe it is a reference to a record type
            default:
                Ext.raise('Unrecognized avro schema type: ' + fieldType.toLowerCase()) ;
        }
    },

    _buildBooleanField: function(fieldSchema) {
        var me = this,
            booleanNode = Ext.create('GoraExplorer.model.avro.BooleanNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        booleanNode.set('name', fieldSchema.name) ;
        booleanNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            booleanNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                booleanNode.set('defaultNullValue', true) ;
            } else {
                booleanNode.set('defaultBooleanValue', fieldSchema.default) ;
            }
        }
        return booleanNode ;
    },

    _buildIntField: function(fieldSchema) {
        var me = this,
            intNode = Ext.create('GoraExplorer.model.avro.IntNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        intNode.set('name', fieldSchema.name) ;
        intNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            intNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                intNode.set('defaultNullValue', true) ;
            } else {
                intNode.set('defaultIntValue', fieldSchema.default) ;
            }
        }
        return intNode ;
    },

    _buildLongField: function(fieldSchema) {
        var me = this,
            longNode = Ext.create('GoraExplorer.model.avro.LongNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        longNode.set('name', fieldSchema.name) ;
        longNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            longNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                longNode.set('defaultNullValue', true) ;
            } else {
                longNode.set('defaultLongValue', fieldSchema.default) ;
            }
        }
        return longNode ;
    },

    _buildFloatField: function(fieldSchema) {
        var me = this,
            floatNode = Ext.create('GoraExplorer.model.avro.FloatNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        floatNode.set('name', fieldSchema.name) ;
        floatNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            floatNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                floatNode.set('defaultNullValue', true) ;
            } else {
                floatNode.set('defaultFloatValue', fieldSchema.default) ;
            }
        }
        return floatNode ;
    },

    _buildDoubleField: function(fieldSchema) {
        var me = this,
            doubleNode = Ext.create('GoraExplorer.model.avro.DoubleNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        doubleNode.set('name', fieldSchema.name) ;
        doubleNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            doubleNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                doubleNode.set('defaultNullValue', true) ;
            } else {
                doubleNode.set('defaultDoubleValue', fieldSchema.default) ;
            }
        }
        return doubleNode ;
    },

    _buildStringField: function(fieldSchema) {
        var me = this,
            stringNode = Ext.create('GoraExplorer.model.avro.StringNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        stringNode.set('name', fieldSchema.name) ;
        stringNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            stringNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                stringNode.set('defaultNullValue', true) ;
            } else {
                stringNode.set('defaultStringValue', fieldSchema.default) ;
            }
        }
        return stringNode ;
    },

    _buildBytesField: function(fieldSchema) {
        var me = this,
            bytesNode = Ext.create('GoraExplorer.model.avro.BytesNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        bytesNode.set('name', fieldSchema.name) ;
        bytesNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            bytesNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                bytesNode.set('defaultNullValue', true) ;
            } else {
                Ext.raise(_('Unexpected Bytes field default value (other than null): ' + fieldSchema.default)) ;
            }
        }
        return bytesNode ;
    },

    _buildRecordField: function(fieldSchema) {
        var me = this,
            recordNode = Ext.create('GoraExplorer.model.avro.RecordNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        recordNode.set('name', fieldSchema.name) ;
        recordNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            recordNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                GoraExplorer.toastHelper.showWarnToast(Ext.String.format(_('Field "{0}" is not nullable, but has a default null value!'), fieldSchema.name)) ;
            }
            if (fieldSchema.default === null) {
                recordNode.set('defaultNullValue', true) ;

            } else if ( ! Ext.Object.equals(fieldSchema.default, {})) {
                GoraExplorer.toastHelper.showWarnToast(Ext.String.format(_('Field {0} has an unexpected default value (other than null or empty object)'), fieldSchema.name)) ;
                Ext.log({level: 'warn', dump: fieldSchema.default, stack: true}, 'Unexpected record field default value') ;
            }
        }

        recordNode.set('recordFieldTypeName', me._getRecordFieldRecordName(fieldSchema)) ;

        // Try to generate the Entity from the record
        var recordDefinition = me._getFieldNonNullTypeObject(fieldSchema) ;
        me._buildEntity(recordDefinition) ;

        return recordNode ;
    },

    _buildMapField: function(fieldSchema) {
        var me = this,
            mapNode = Ext.create('GoraExplorer.model.avro.MapNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        mapNode.set('name', fieldSchema.name) ;
        mapNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            mapNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                mapNode.set('defaultNullValue', true) ;
            } else if ( ! Ext.Object.equals(fieldSchema.default, {})) {
                GoraExplorer.toastHelper.showWarnToast(Ext.String.format(_('Field {0} has an unexpected default value (other than null or empty object)'), fieldSchema.name)) ;
                Ext.log({level: 'warn', dump: fieldSchema.default, stack: true}, 'Unexpected map field default value') ;
            }
        }

        var valuesType = me._getMapValuesType(fieldSchema) ;
        mapNode.set('valuesType', valuesType) ;
        mapNode.set('valuesNullable', me._getMapValuesNullable(fieldSchema)) ;

        if (valuesType === 'record') {
            mapNode.set('valuesRecordName', me._getMapValuesRecordName(fieldSchema)) ;

            // Try to generate the Entity from the record
            var mapValues = me._getFieldNonNullTypeObject(fieldSchema).values,
                valuesRecordDef = me._getNonNullItemsOrValuesOrType(mapValues) ; // Can be a String or an Object
            me._buildEntity(valuesRecordDef) ;
        }

        return mapNode ;
    },

    _buildArrayField: function(fieldSchema) {
        var me = this,
            arrayNode = Ext.create('GoraExplorer.model.avro.ArrayNode'),
            fieldIsNullable = me._fieldIsNullable(fieldSchema) ;

        arrayNode.set('name', fieldSchema.name) ;
        arrayNode.set('nullable', fieldIsNullable) ;
        if (Ext.isDefined(fieldSchema.doc)) {
            arrayNode.set('doc', fieldSchema.doc);
        }
        if (Ext.isDefined(fieldSchema.default)) {
            if (fieldSchema.default === null && !fieldIsNullable) {
                Ext.log({level: 'warn', stack: true}, 'Field ' + fieldSchema.name + ' is not nullable, but has a default null value!') ;
            }
            if (fieldSchema.default === null) {
                arrayNode.set('defaultNullValue', true) ;
            } else if (!Ext.isArray(fieldSchema.default) || fieldSchema.default.length > 0) {
                GoraExplorer.toastHelper.showWarnToast(Ext.String.format(_('Field {0} has an unexpected default value (other than null or empty array)'), fieldSchema.name)) ;
                Ext.log({level: 'warn', dump: fieldSchema.default, stack: true}, 'Unexpected array field default value') ;
            }
        }

        var itemsType = me._getArrayItemsType(fieldSchema) ;
        arrayNode.set('itemsType', itemsType) ;
        arrayNode.set('itemsNullable', me._getArrayItemsNullable(fieldSchema)) ;

        if (itemsType === 'record') {
            arrayNode.set('itemsRecordName', me._getArrayItemsRecordName(fieldSchema)) ;

            // Try to generate the Entity from the record
            var mapValues = me._getFieldNonNullTypeObject(fieldSchema).items,
                valuesRecordDef = me._getNonNullItemsOrValuesOrType(mapValues) ; // Can be a String or an Object
            me._buildEntity(valuesRecordDef) ;
        }

        return arrayNode ;
    },

    // ============================================================================================================
    // ============================================================================================================
    //
    // Helper methods
    //
    // ============================================================================================================
    // ============================================================================================================


    /**
     * Given a field, gets the non-null type as an object. It doesn't matter if it is a string, object, or anything.
     * Just, if it is an union with a null, gets rid of the null.
     * @param {Object} fieldSchema
     * @private
     */
    _getFieldNonNullTypeObject: function(fieldSchema) {
        var me = this,
            type = fieldSchema.type ;

        return me._getNonNullItemsOrValuesOrType(type) ;
    },

    /**
     * Given a field object schema, returns its type name, getting rid of the null type.
     * @param fieldSchema
     * @private
     */
    _getFieldNonNullTypeName: function(fieldSchema) {
        var me = this,
            fieldTypeObject = me._getFieldNonNullTypeObject(fieldSchema) ;

        if (Ext.isObject(fieldTypeObject)) {
            // Record DEFINITION
            return fieldTypeObject.type ;
        } else {
            // Assume is string => type == name
            return fieldTypeObject ;
        }
    },

    /**
     * Returns true if in a field type is an array with 2 elements (supposed 1 is null)
     * @param {Object} fieldSchema
     * @return {boolean}
     * @private
     */
    _fieldIsNullable: function(fieldSchema) {
        var me = this,
            type = fieldSchema.type ;
        if (Ext.isArray(type) && type.length > 2) {
            Ext.raise(_('Unsupported 3 types unions. Field name: ' + fieldSchema.name)) ;
        }
        if (Ext.isArray(type) && type.length === 2) {
            if (type[0] !== 'null' && type[1] !== 'null') {
                Ext.raise(Ext.String.format(_('Unsupported feature: field {0} has 2 non-null types'), fieldSchema.name)) ;
            }
            return true ;
        } else {
            if (type[0] === 'null') {
                Ext.raise(Ext.String.format(_('Unsupported feature: field {0} has "null" type'), fieldSchema.name)) ;
            }
            return false ;
        }
    },

    /**
     * Given a record field schema, returns the record name used as type
     * @param {Object}fieldSchema
     * @returns {String} The name of the record used
     * @private
     */
    _getRecordFieldRecordName: function(fieldSchema) {
        var me = this,
            fieldTypeObject = me._getFieldNonNullTypeObject(fieldSchema) ;

        if (Ext.isObject(fieldTypeObject)) {
            // Record DEFINITION
            return fieldTypeObject.name ;
        } else {
            // Assume is string => type == name
            return fieldTypeObject ; // Actually it is the record name
        }
    },

    _getMapValuesType: function(fieldSchema) {
        var me = this,
            fieldTypeObject = me._getFieldNonNullTypeObject(fieldSchema),
            mapValue = fieldTypeObject.values ;

        mapValue = me._getNonNullItemsOrValuesOrType(mapValue) ;
        // Now we have a non-null value at mapValue

        if (Ext.isString(mapValue)) {
            mapValue = mapValue.toLowerCase() ;
            if (Ext.Array.contains(me._forbiddenMapTypes, mapValue)) {
                Ext.raise(_('Unimplemented feature. Maps can\'t hold arrays, maps, or fixed. At field ' + fieldSchema.name)) ;
            }
            // Simple type or record  name
            if (Ext.Array.contains(me._allowedMapTypes, mapValue)) {
                return mapValue ;
            } else {
                // It is a record name
                return 'record' ;
            }
        }

        if (Ext.isObject(mapValue)) {
            // Record definition
            return 'record' ;
        }

        Ext.raise(_('Couldn\'t get the map values type for the field ' + fieldSchema.name)) ;
    },

    _getMapValuesNullable: function(fieldSchema) {
        var me = this,
            fieldTypeObject = me._getFieldNonNullTypeObject(fieldSchema),
            mapValue = fieldTypeObject.values ;

        return me._isItemsOrValuesNullable(mapValue) ;
    },

    _getMapValuesRecordName: function(fieldSchema) {
        var me = this,
            fieldTypeObject = me._getFieldNonNullTypeObject(fieldSchema),
            mapValue = fieldTypeObject.values ;

        mapValue = me._getNonNullItemsOrValuesOrType(mapValue) ;
        // Now we have a non-null value at mapValue

        if (Ext.isString(mapValue)) {
            if (Ext.Array.contains(me._forbiddenMapTypes, mapValue.toLowerCase())) {
                Ext.raise(_('Unimplemented feature. Maps can\'t hold arrays, maps, or fixed. At field ' + fieldSchema.name)) ;
            }
            // Simple type or record  name
            if (Ext.Array.contains(me._allowedMapTypes, mapValue.toLowerCase())) {
                return mapValue ;
            } else {
                // It is a record name
                return mapValue ;
            }
        }

        if (Ext.isObject(mapValue)) {
            // Record definition
            return mapValue.name;
        }
    },

    _getArrayItemsType: function(fieldSchema) {
        var me = this,
            fieldTypeObject = me._getFieldNonNullTypeObject(fieldSchema),
            arrayItem = fieldTypeObject.items ;

        arrayItem = me._getNonNullItemsOrValuesOrType(arrayItem) ;
        // Now we have a non-null item type at arrayItem

        if (Ext.isString(arrayItem)) {
            arrayItem = arrayItem.toLowerCase() ;
            if (Ext.Array.contains(me._forbiddenArrayTypes, arrayItem)) {
                Ext.raise(_('Unimplemented feature. Arrays can\'t hold arrays, maps, or fixed. At field ' + fieldSchema.name)) ;
            }
            // Simple type or record  name
            if (Ext.Array.contains(me._allowedArrayTypes, arrayItem)) {
                return arrayItem ;
            } else {
                // It is a record name
                return 'record' ;
            }
        }

        if (Ext.isObject(arrayItem)) {
            // Record definition
            return 'record' ;
        }

        Ext.raise(_('Couldn\'t get the array values type for the field ' + fieldSchema.name)) ;
    },

    _getArrayItemsNullable: function(fieldSchema) {
        var me = this,
            fieldTypeObject = me._getFieldNonNullTypeObject(fieldSchema),
            arrayItem = fieldTypeObject.items ;

        return me._isItemsOrValuesNullable(arrayItem) ;
    },

    _getArrayItemsRecordName: function(fieldSchema) {
        var me = this,
            fieldTypeObject = me._getFieldNonNullTypeObject(fieldSchema),
            arrayItem = fieldTypeObject.items ;

        arrayItem = me._getNonNullItemsOrValuesOrType(arrayItem) ;
        // Now we have a non-null value at arrayItem

        if (Ext.isString(arrayItem)) {
            if (Ext.Array.contains(me._forbiddenMapTypes, arrayItem.toLowerCase())) {
                Ext.raise(_('Unimplemented feature. Maps can\'t hold arrays, maps, or fixed. At field ' + fieldSchema.name)) ;
            }
            // Simple type or record  name
            if (Ext.Array.contains(me._allowedMapTypes, arrayItem.toLowerCase())) {
                return arrayItem ;
            } else {
                // It is a record name
                return arrayItem ;
            }
        }

        if (Ext.isObject(arrayItem)) {
            // Record definition
            return arrayItem.name;
        }
    },

    /**
     * Given a values or items or type definition gets rid of the null type if it is an UNION NULL
     * @param {String / Array} valuesOrItemsDef - The 'values' or 'items' field of an Array or Map, or type defintion
     * @return {String / Object} Returns the type withouht the null component, if there is any. A String when
     *   it is a basic type, or a Record Name An Object when it is a record definition.
     * @private
     */
    _getNonNullItemsOrValuesOrType: function(valuesOrItemsDef) {
        var me = this,
            itemOrValue ;

        // If UNION NULL, get rid of the null
        if (Ext.isArray(valuesOrItemsDef)) {
            if (valuesOrItemsDef.length > 2) {
                Ext.raise(_('Unsupported 3 types unions.')) ;
            }
            if (valuesOrItemsDef.length === 2) {
                if (valuesOrItemsDef[0] === 'null') {
                    itemOrValue = valuesOrItemsDef[1] ;
                } else {
                    itemOrValue = valuesOrItemsDef[0] ;
                }
            } else {
                // 1 type union - should not be union
                itemOrValue = valuesOrItemsDef[0]
            }
        } else {
            // It is a String or a object (record definition)
            itemOrValue = valuesOrItemsDef ;
        }
        return itemOrValue ;
    },

    /**
     * Given a values or items definition of a Map or Array returns true when the items/values are nullable
     * @param {String / Array} valuesOrItemsDef - The 'values' or 'items' field of an Array or Map
     * @return {Boolean} Returns true when the items/values are nullable, false otherwise
     * @private
     */
    _isItemsOrValuesNullable: function(valuesOrItemsDef) {
        var me = this ;

        if (Ext.isArray(valuesOrItemsDef)) {
            if (valuesOrItemsDef.length > 2) {
                Ext.raise(_('Unsupported 3 types unions. Field name: ' + fieldSchema.name)) ;
            }
            if (valuesOrItemsDef.length === 2) {
                return (valuesOrItemsDef[0] === 'null' || valuesOrItemsDef[1] === 'null' ) ;
            } else {
                if (valuesOrItemsDef[0] === 'null') {
                    Ext.raise(_('Unsupported only null type at field: ' + fieldSchema.name)) ;
                }
                return false ; // In this case, it is not nullable
            }
        } else {
            // Assume it is String or object(record definition)
            return false ;
        }
    }

},
function (SchemaTreeBuilder){
    GoraExplorer.schemaTreeBuilder = SchemaTreeBuilder ;
}) ;