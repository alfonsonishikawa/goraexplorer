/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Abstract class for all Field Nodes.
 * Defines some common values.
 *
 * @abstract
 */
Ext.define('GoraExplorer.model.avro.FieldNode', {
    extend: 'GoraExplorer.model.avro.SchemaNode',

    requires: [
        'Ext.data.validator.Format',
        'Ext.data.validator.Presence'
    ],

    fields: [

        /**
         * Fields can be nullable
         */
        { name: 'nullable', type: 'boolean' },

        /**
         * Flag that marks if the default value of the field is null
         */
        { name: 'defaultNullValue', type: 'boolean' },


        // NodeInterface related values -----------------------------------------

        /**
         * @override
         * @readonly
         */
        { name: 'leaf', type: 'boolean', defaultValue: true},

        /**
         * @override
         * @readonly
         */
        { name: 'iconCls', type: 'string', defaultValue: 'x-fa fa-file-text-o' }

    ],

    validators: {
        name: [
            { type: 'presence' },
            { type: 'format', matcher: /^[a-z_][a-zA-Z_]*$/, message: _('The Entity name must begin with a lower letter and contain only letters, numbers and underscores') }
        ]
    },

    /**
     * @override
     */
    accept: function(visitor) {
        return visitor.visitFieldNode(this) ;
    }

}) ;