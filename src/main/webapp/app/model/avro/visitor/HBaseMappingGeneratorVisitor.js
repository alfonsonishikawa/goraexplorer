/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Given an EntityNode, generates the HBase Mapping XML. If the instance is configured with "prettyPrint == true"
 * the generated XML and String will hold the necessary spaces.
 */
Ext.define('GoraExplorer.model.avro.visitor.HBaseMappingGeneratorVisitor', {
    extend: 'GoraExplorer.model.avro.visitor.AbstractSchemaVisitor',

    /**
     * @cfg {String} namespace - The namespace to use as configuration value. Recommended the username
     */
    namespace: null,

    /**
     * {GoraExplorer.model.avro.EntitiesNode} - (constructor) The tree root holding all possible entities, one to be generated
     */
    rootEntitiesNode: null,

    /**
     * @cfg {String} nativeTableName - (constructor) Native table name to generate it's mapping
     */
    nativeTableName: null,

    /**
     * @cfg {String} keyClass - (constructor) Full java key class (with namespace) of the native table hbase key
     */
    keyClass: null,

    /**
     * Will hold the generated XML document
     * @private
     */
    xmlDoc: null,

    /**
     * Controls if the result (XML and String) must be pretty printed (adds indentation)
     * @private
     */
    prettyPrint: false,

    /**
     * @param {String} namespace - The namespace to use as configuration value. Recommended the username :)
     * @param {String} nativeTableName
     * @param {String} keyClass
     * @param {GoraExplorer.model.avro.EntitiesNode rootEntitiesNode - The root "EntitiesNode" of the schema tree, one
     *          of the EntityNodes can be generated (visited).
     * @param {Boolean} [prettyPrint=false]
     */
    constructor: function(namespace, nativeTableName, keyClass, rootEntitiesNode, prettyPrint) {
        var me = this ;

        if (!Ext.isDefined(namespace)) {
            Ext.raise('Missing "namespace" parameter at JsonSchemaGeneratorVisitor constructor') ;
        }

        if (!Ext.isDefined(rootEntitiesNode)) {
            Ext.raise('Missing "rootEntitiesNodes" parameter at JsonSchemaGeneratorVisitor constructor') ;
        }

        if (Ext.isEmpty(nativeTableName)) {
            Ext.raise('Missing "nativeTableName" parameter at JsonSchemaGeneratorVisitor constructor') ;
        }

        if (Ext.isEmpty(keyClass)) {
            Ext.raise('Missing "keyClass" parameter at JsonSchemaGeneratorVisitor constructor') ;
        }

        me.namespace = namespace ;
        me.rootEntitiesNode = rootEntitiesNode ;
        me.nativeTableName = nativeTableName ;
        me.keyClass = keyClass ;
        if (prettyPrint) {
            me.prettyPrint = prettyPrint ;
        }
    },

    /**
     * This method is the main entrance
     * @param {GoraExplorer.model.avro.EntityNode} node - Entity to generate its hbase mapping
     * @returns {String} HBase mapping for the entity
     * @public
     */
    generateHBaseMapping: function(node) {
        var me = this ;

        // Convert XML to String
        var serializer = new XMLSerializer(),
            xmlString = serializer.serializeToString(me.generateHBaseMappingAsXmlDoc(node)) ;
        return '<?xml version="1.0" encoding="UTF-8"?>\n' + xmlString ;
    },

    /**
     * This method is the main entrance
     * @param {GoraExplorer.model.avro.EntityNode} node - Entity to generate its hbse mapping
     * @returns {xmlDoc} XmlDoc with the HBase Mapping
     * @public
     */
    generateHBaseMappingAsXmlDoc: function(node) {
        var me = this ;

        me.xmlDoc = document.implementation.createDocument(null, 'gora-odm') ; // I like "gora - Object-Datastore-Mapper"

        var tableElement = me.xmlDoc.createElement('table') ;
        tableElement.setAttribute('name', me.nativeTableName) ;

        var classElement = me.xmlDoc.createElement('class') ;
        classElement.setAttribute('table', me.nativeTableName) ;
        classElement.setAttribute('keyClass', me.keyClass) ;
        classElement.setAttribute('name', me.namespace + '.' + node.get('name')) ;

        if (me.prettyPrint) me.xmlDoc.documentElement.append(me.xmlDoc.createTextNode('\n  ')) ;
        me.xmlDoc.documentElement.append(tableElement) ;
        if (me.prettyPrint) me.xmlDoc.documentElement.append(me.xmlDoc.createTextNode('\n  ')) ;
        me.xmlDoc.documentElement.append(classElement) ;
        if (me.prettyPrint) me.xmlDoc.documentElement.append(me.xmlDoc.createTextNode('\n')) ;

        node.accept(me) ;

        if (me.prettyPrint) tableElement.append(me.xmlDoc.createTextNode('\n  ')) ;
        if (me.prettyPrint) classElement.append(me.xmlDoc.createTextNode('\n  ')) ;

        return me.xmlDoc ;
    },

    addFamilyColumnField: function(node) {
        var me = this,
            tableElement = me.xmlDoc.getElementsByTagName('table')[0],
            classElement = me.xmlDoc.getElementsByTagName('class')[0] ;


        if (tableElement.querySelector('family[name='+node.get('hbaseFamily')+']') === null) {
            var newTableFamily = me.xmlDoc.createElement('family') ;

            newTableFamily.setAttribute('name', node.get('hbaseFamily')) ;
            newTableFamily.setAttribute('maxVersions', '1') ;
            if (me.prettyPrint) tableElement.append(me.xmlDoc.createTextNode('\n    ')) ;
            tableElement.append(newTableFamily) ;
        }

        var classField = me.xmlDoc.createElement('field') ;
        classField.setAttribute('name', node.get('name')) ;
        classField.setAttribute('family', node.get('hbaseFamily')) ;
        classField.setAttribute('qualifier', node.get('hbaseColumn')) ;
        if (me.prettyPrint) classElement.append(me.xmlDoc.createTextNode('\n    ')) ;
        classElement.append(classField) ;
    },

    addFamilyField: function(node) {
        var me = this,
            tableElement = me.xmlDoc.getElementsByTagName('table')[0],
            classElement = me.xmlDoc.getElementsByTagName('class')[0] ;

        if (tableElement.querySelector('family[name='+node.get('hbaseFamily')+']') === null) {
            var newTableFamily = me.xmlDoc.createElement('family') ;
            newTableFamily.setAttribute('name', node.get('hbaseFamily')) ;
            newTableFamily.setAttribute('maxVersions', '1') ;
            if (me.prettyPrint) tableElement.append(me.xmlDoc.createTextNode('\n    ')) ;
            tableElement.append(newTableFamily) ;
        }

        var classField = me.xmlDoc.createElement('field') ;
        classField.setAttribute('name', node.get('name')) ;
        classField.setAttribute('family', node.get('hbaseFamily')) ;
        if (me.prettyPrint) classElement.append(me.xmlDoc.createTextNode('\n    ')) ;
        classElement.append(classField) ;
    },

    /**
     * @param {GoraExplorer.model.avro.ArrayNode} node
     * @return {Object} An array avro schema field definition of a Record field.
     */
    visitArrayNode: function(node) {
        var me = this ;
        me.addFamilyField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.BooleanNode} node
     * @return {Object} A boolean avro schema field definition of a Record field.
     */
    visitBooleanNode: function(node) {
        var me = this ;
        me.addFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.BytesNode} node
     * @return {Object} A bytes avro schema field definition of a Record field.
     */
    visitBytesNode: function(node) {
        var me = this ;
        me.addFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.DoubleNode} node
     * @return {Object} A double avro schema field definition of a Record field.
     */
    visitDoubleNode: function(node) {
        var me = this ;
        me.addFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.EntityNode} node
     * @returns {Object} Json schema of the Entity
     */
    visitEntityNode: function(node) {
        var me = this ;

        node.eachChild(function(fieldNode) {
            fieldNode.accept(me) ;
        }) ;
    },

    /**
     * @param {GoraExplorer.model.avro.FloatNode} node
     * @return {Object} A float avro schema field definition of a Record field.
     */
    visitFloatNode: function(node) {
        var me = this ;
        me.addFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.IntNode} node
     * @return {Object} A int avro schema field definition of a Record field.
     */
    visitIntNode: function(node) {
        var me = this ;
        me.addFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.LongNode} node
     * @return {Object} A long avro schema field definition of a Record field.
     */
    visitLongNode: function(node) {
        var me = this ;
        me.addFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.MapNode} node
     * @return {Object} A map avro schema field definition of a Record field.
     */
    visitMapNode: function(node) {
        var me = this ;
        me.addFamilyField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.RecordNode} node
     * @return {Object} A record avro schema field definition of a Record field.
     */
    visitRecordNode: function(node) {
        var me = this ;
        me.addFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.StringNode} node
     * @return {Object} A string avro schema field definition of a Record field.
     */
    visitStringNode: function(node) {
        var me = this ;
        me.addFamilyColumnField(node) ;
    }

}) ;