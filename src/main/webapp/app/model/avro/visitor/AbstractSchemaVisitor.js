/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @abstract
 */
Ext.define('GoraExplorer.model.avro.visitor.AbstractSchemaVisitor', {

    visit: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitArrayNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitBooleanNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitBytesNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitDoubleNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitEntityNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitEntitiesNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitFieldNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitFloatNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitIntNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitLongNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitMapNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitRecordNode: function(node) {
        Ext.raise('Method not implemented') ;
    },

    visitStringNode: function(node) {
        Ext.raise('Method not implemented') ;
    }

}) ;