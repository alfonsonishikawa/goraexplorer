/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Given an EntityNode, generates the avro schema related.
 */
Ext.define('GoraExplorer.model.avro.visitor.JsonSchemaGeneratorVisitor', {
    extend: 'GoraExplorer.model.avro.visitor.AbstractSchemaVisitor',

    /**
     * @cfg {String} namespace - The namespace to use as configuration value. Recommended the username
     */
    namespace: null,

    /**
     * {Set} that holds the names of the generated entities to not generate twice.
     */
    generatedEntities: null,

    /**
     * {GoraExplorer.model.avro.EntitiesNode} The tree root holding all possible entities, one to be generated
     */
    rootEntitiesNode: null,

    /**
     * @param {String} namespace - The namespace to use as configuration value. Recommended the username :)
     * @param {GoraExplorer.model.avro.EntitiesNode} rootEntitiesNode - The root "EntitiesNode" of the schema tree, one
     *          of the EntityNodes can be generated (visited).
     */
    constructor: function(namespace, rootEntitiesNode) {
        var me = this ;

        if (!Ext.isDefined(namespace)) {
            Ext.raise('Missing "namespace" parameter at JsonSchemaGeneratorVisitor constructor') ;
        }

        if (!Ext.isDefined(rootEntitiesNode)) {
            Ext.raise('Missing "rootEntitiesNodes" parameter at JsonSchemaGeneratorVisitor constructor') ;
        }

        me.namespace = namespace ;
        me.rootEntitiesNode = rootEntitiesNode ;
        me.generatedEntities = new Set() ;
    },

    /**
     * This method is the main entrance
     * @param {GoraExplorer.model.avro.EntityNode} node - Entity to generate its json schema
     * @returns {Object} Json schema of the Entity
     * @public
     */
    generateJsonSchema: function(node) {
        var me = this ;
        // Clear the internal state to start a new generation
        me.generatedEntities = new Set() ;

        return node.accept(me) ;
    },

    /**
     * @param {GoraExplorer.model.avro.ArrayNode} node
     * @return {Object} An array avro schema field definition of a Record field.
     */
    visitArrayNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            itemsType = node.get('itemsType'),
            allowNullItems = node.get('allowNullItems'),
            itemsRecordName = node.get('itemsRecordName'),
            fieldJson = {
                name: fieldName,
                default: []
            } ;

        // items type
        itemsType = itemsType.toLowerCase() ;
        if (itemsType === 'record') {
            // Maybe have to generate the record definition!
            // Record definition: Simply the name or the full json field definition
            var recordAvroJsonOrId = itemsRecordName ; // Just the name
            if ( ! me.generatedEntities.has(itemsRecordName) ) {
                // Generate the field json
                var referencedEntityNode = me.rootEntitiesNode.findChild('name', itemsRecordName) ;
                recordAvroJsonOrId = referencedEntityNode.accept(me) ;
            }
            itemsType = recordAvroJsonOrId ;
        }

        if (allowNullItems) {
            itemsType = [ 'null' , itemsType ] ;
        }

        // type
        fieldJson.type = {
            type: 'array',
            items: itemsType
        } ;

        if ( nullableField ) {
            fieldJson.type = [ 'null', fieldJson.type ] ;
            if (defaultNullValue) {
                fieldJson.default = null ;
            }
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.BooleanNode} node
     * @return {Object} A boolean avro schema field definition of a Record field.
     */
    visitBooleanNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultBooleanValue'),
            fieldJson = {
                name: fieldName
            } ;

        // default value
        if ( nullableField && defaultNullValue ) {
            fieldJson.default = null ;
        } else {
            if ( Ext.isDefined(defaultValue) ) {
                fieldJson.default = defaultValue ;
            }
        }

        // type
        fieldJson.type = 'boolean' ;
        if ( nullableField ) {
            fieldJson.type = [ 'null', 'boolean' ] ;
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.BytesNode} node
     * @return {Object} A bytes avro schema field definition of a Record field.
     */
    visitBytesNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            fieldJson = {
                name: fieldName
            } ;

        // type
        fieldJson.type = 'bytes' ;
        if ( nullableField ) {
            fieldJson.type = [ 'null', 'bytes' ] ;
            if (defaultNullValue) {
                fieldJson.default = null ;
            }
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.DoubleNode} node
     * @return {Object} A double avro schema field definition of a Record field.
     */
    visitDoubleNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultDoubleValue'),
            fieldJson = {
                name: fieldName
            } ;

        // default value
        if ( nullableField && defaultNullValue ) {
            fieldJson.default = null ;
        } else {
            if ( Ext.isDefined(defaultValue) ) {
                fieldJson.default = defaultValue ;
            }
        }

        // type
        fieldJson.type = 'double' ;
        if ( nullableField ) {
            fieldJson.type = [ 'null', 'double' ] ;
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.EntityNode} node
     * @returns {Object} Json schema of the Entity
     */
    visitEntityNode: function(node) {
        var me = this,
            entityName = node.get('name'),
            entityJson = {
                type:      'record',
                namespace: me.namespace,
                name:      entityName,
                fields:    []
            } ;

        me.generatedEntities.add(entityName) ;

        if ( !Ext.isEmpty(node.get('doc')) ) {
            entityJson.doc = node.get('doc') ;
        }

        node.eachChild(function(fieldNode) {
            var fieldJson = fieldNode.accept(me) ;
            entityJson.fields.push(fieldJson) ;
        }) ;

        return entityJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.FloatNode} node
     * @return {Object} A float avro schema field definition of a Record field.
     */
    visitFloatNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultFloatValue'),
            fieldJson = {
                name: fieldName
            } ;

        // default value
        if ( nullableField && defaultNullValue ) {
            fieldJson.default = null ;
        } else {
            if ( Ext.isDefined(defaultValue) ) {
                fieldJson.default = defaultValue ;
            }
        }

        // type
        fieldJson.type = 'float' ;
        if ( nullableField ) {
            fieldJson.type = [ 'null', 'float' ] ;
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.IntNode} node
     * @return {Object} A int avro schema field definition of a Record field.
     */
    visitIntNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultIntValue'),
            fieldJson = {
                name: fieldName
            } ;

        // default value
        if ( nullableField && defaultNullValue ) {
            fieldJson.default = null ;
        } else {
            if ( Ext.isDefined(defaultValue) ) {
                fieldJson.default = defaultValue ;
            }
        }

        // type
        fieldJson.type = 'int' ;
        if ( nullableField ) {
            fieldJson.type = [ 'null', 'int' ] ;
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.LongNode} node
     * @return {Object} A long avro schema field definition of a Record field.
     */
    visitLongNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultLongValue'),
            fieldJson = {
                name: fieldName
            } ;

        // default value
        if ( nullableField && defaultNullValue ) {
            fieldJson.default = null ;
        } else {
            if ( Ext.isDefined(defaultValue) ) {
                fieldJson.default = defaultValue ;
            }
        }

        // type
        fieldJson.type = 'long' ;
        if ( nullableField ) {
            fieldJson.type = [ 'null', 'long' ] ;
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.MapNode} node
     * @return {Object} A map avro schema field definition of a Record field.
     */
    visitMapNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            valuesType = node.get('valuesType'),
            valuesNullable = node.get('valuesNullable'),
            valuesRecordName = node.get('valuesRecordName'),
            fieldJson = {
                name: fieldName,
                default: {}
            } ;

        // items type
        valuesType = valuesType.toLowerCase() ;
        if (valuesType === 'record') {
            // Maybe have to generate the record definition!
            // Record definition: Simply the name or the full json field definition
            var recordAvroJsonOrId = valuesRecordName ; // Just the name
            if ( ! me.generatedEntities.has(valuesRecordName) ) {
                // Generate the field json
                var referencedEntityNode = me.rootEntitiesNode.findChild('name', valuesRecordName) ;
                recordAvroJsonOrId = referencedEntityNode.accept(me) ;
            }
            valuesType = recordAvroJsonOrId ;
        }

        if (valuesNullable) {
            valuesType = [ 'null' , valuesType ] ;
        }

        // type and default value
        fieldJson.type = {
            type: 'map',
            values: valuesType
        } ;

        if ( nullableField ) {
            fieldJson.type = [ 'null', fieldJson.type ] ;
            if (defaultNullValue) {
                fieldJson.default = null ;
            }
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.RecordNode} node
     * @return {Object} A record avro schema field definition of a Record field.
     */
    visitRecordNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            recordFieldTypeName = node.get('recordFieldTypeName'),
            fieldJson = {
                name: fieldName
            } ;

        // Record definition: Simply the name or the full json field definition
        var recordAvroJsonOrId = recordFieldTypeName ; // Just the name
        if ( ! me.generatedEntities.has(recordFieldTypeName) ) {
            // Generate the field json
            var referencedEntityNode = me.rootEntitiesNode.findChild('name', recordFieldTypeName) ;
            recordAvroJsonOrId = referencedEntityNode.accept(me) ;
        }

        // type
        fieldJson.type = recordAvroJsonOrId ;
        if ( nullableField ) {
            fieldJson.type = [ 'null', recordAvroJsonOrId ] ;
            if (defaultNullValue) {
                fieldJson.default = null;
            }
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    },

    /**
     * @param {GoraExplorer.model.avro.StringNode} node
     * @return {Object} A string avro schema field definition of a Record field.
     */
    visitStringNode: function(node) {
        var me = this,
            fieldName = node.get('name'),
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultStringValue'),
            fieldJson = {
                name: fieldName
            } ;

        // default value
        if ( nullableField && defaultNullValue ) {
            fieldJson.default = null ;
        } else {
            if ( Ext.isDefined(defaultValue) ) {
                fieldJson.default = defaultValue ;
            }
        }

        // type
        fieldJson.type = 'string' ;
        if ( nullableField ) {
            fieldJson.type = [ 'null', 'string' ] ;
        }

        // doc
        if ( !Ext.isEmpty(node.get('doc')) ) {
            fieldJson.doc = node.get('doc') ;
        }

        return fieldJson ;
    }


}) ;