/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Given an EntitiesNode, validates that the mapping definitions are right.
 *  - Arrays and Maps can only have Family defined (not qualifier/column).
 *  - Other fields can only use a Family if it is not being used in an Array/Map.
 *  - Two fields can't use the same family+qualifier.
 *  - Every field in the Main Entity must have a mapping
 */
Ext.define('GoraExplorer.model.avro.visitor.HBaseMappingValidatorVisitor', {
    extend: 'GoraExplorer.model.avro.visitor.AbstractSchemaVisitor',

    /**
     * {Object} HashMap which will hold the used families and columns used and by who.
     *
     * Type: String -> String[]
     *
     * Key: "family+':'+qualifier". For Array/Map, the key will "family+':'"
     * Value : Array with the names of the fields using that path
     */
    familyAndColumnsRegistry: {},

    /**
     * {String[]} - Array of Fields names that are Array/Map but have a column/qualifier defined (should not have).
     */
    columnDefinedErrorFields: [],

    /**
     * {String[]} - Array of Fields that have missing mapping.
     */
    missingMappings: [],

    /**
     * {GoraExplorer.model.avro.EntitiesNode} The tree root holding all possible entities, one to be checked
     */
    rootEntitiesNode: null,

    /**
     * @param {GoraExplorer.model.avro.EntitiesNode} rootEntitiesNode - The root "EntitiesNode" of the schema tree. The
     *        Main Entity (the first of EntitiesNode's childrens) can be validated.
     */
    constructor: function(rootEntitiesNode) {
        var me = this ;

        if (!Ext.isDefined(rootEntitiesNode)) {
            Ext.raise('Missing "rootEntitiesNodes" parameter at HBaseMappingValidatorVisitor constructor') ;
        }

        me.rootEntitiesNode = rootEntitiesNode ;
        me.familyAndColumnsRegistry = {} ;
        me.columnDefinedErrorFields = [] ;
        me.missingMappings = [] ;
    },

    /**
     * Given a EntityNode, checks if the mapping definition are valid.
     * When there are errors, they can be checked with {@link #getErrors()}
     * @param {GoraExplorer.model.avro.EntityNode} node - Node to check the mapping
     * @return <code>true</code> when the mapping is correct. <code>false</code> when is invalid.
     */
    isValid: function(node) {
        var me = this ;

        // Reset the internal state to start a new validation
        me.familyAndColumnsRegistry = {} ;
        me.columnDefinedErrorFields = [] ;

        node.accept(me) ;
        if (me.columnDefinedErrorFields.length > 0) {
            return false ;
        }

        if (me.missingMappings.length > 0) {
            return false ;
        }

        var isValid = true ;
        Ext.Array.forEach(Ext.Object.getKeys(me.familyAndColumnsRegistry), function(key) {
            isValid = isValid && me.familyAndColumnsRegistry[key].length === 1 ;
        }) ;

        return isValid ;
    },

    /**
     * Returns the last validation errors
     * @return {Object} errors
     * @return {{String[]} errors.columnDefinedErrorFields - Array of entities with column mapping defined when not allowed
     * @return {Object} errors.familyAndColumnsRegistry - {String} Key -> {String[]} Entities colliding the usage of
     *          family:column
     */
    getErrors: function() {
        var me = this ;

        // From the family/columns errors, return only the ones with collisions (more than 1 entity using it
        var familyAndColumnsRegistryFiltered = {} ;
        Ext.Object
            .getKeys(me.familyAndColumnsRegistry)
            .filter(function(key) {
                return (me.familyAndColumnsRegistry[key].length > 1) ;
            })
            .forEach(function(key) {
                familyAndColumnsRegistryFiltered[key] = me.familyAndColumnsRegistry[key] ;
            }) ;

        return {
            columnDefinedErrorFields: me.columnDefinedErrorFields,
            missingMappings: me.missingMappings,
            familyAndColumnsRegistry: familyAndColumnsRegistryFiltered
        }
    },

    /**
     * Method to count a field != Array/Map
     * @param node
     */
    countFamilyColumnField: function(node) {
        var me = this,
            nodeName = node.get('name'),
            family = node.get('hbaseFamily'),
            column = node.get('hbaseColumn'),
            singleMapKey = family + ':' , // key used by Map/Array
            mapKey = family + ':' + column ;

        if (Ext.isEmpty(family) || Ext.isEmpty(column)) {
            me.missingMappings.push(nodeName) ;
        }

        // If any Array/Map already generated the family, add this usage
        if (Ext.isDefined(me.familyAndColumnsRegistry[singleMapKey])) {
            me.familyAndColumnsRegistry[singleMapKey].push(nodeName) ;
        }

        // Add the usage or create it to the family:column
        if (Ext.isDefined(me.familyAndColumnsRegistry[mapKey])) {
            me.familyAndColumnsRegistry[mapKey].push(nodeName) ;
        } else { // Else create it
            me.familyAndColumnsRegistry[mapKey] = [nodeName] ;
        }
    },

    /**
     * Method to count an Array/Map
     * @param node
     */
    countFamilyField: function(node) {
        var me = this,
            nodeName = node.get('name'),
            family = node.get('hbaseFamily'),
            column = node.get('hbaseColumn'),
            singleMapKey = family + ':'; // key used by Map/Array

        if (Ext.isEmpty(family)) {
            me.missingMappings.push(nodeName) ;
        }

        // If the Array/Map have a column defined, add the name to the columns defined errors array
        if (Ext.isDefined(column)) {
            me.columnDefinedErrorFields.push(nodeName) ;
        }

        // Check if any family:column field is using the family prefix
        var keys = Ext.Object.getKeys(me.familyAndColumnsRegistry) ;
        keys.filter(function(key) {
            return Ext.String.startsWith(key, singleMapKey) ;
        }).forEach(function(key) {
            me.familyAndColumnsRegistry[key].push(nodeName) ;
        }) ;

        // If any Array/Map already generated the family, add this usage
        if (Ext.isDefined(me.familyAndColumnsRegistry[singleMapKey])) {
            me.familyAndColumnsRegistry[singleMapKey].push(nodeName) ;
        } else { // Else create it
            me.familyAndColumnsRegistry[singleMapKey] = [nodeName] ;
        }

    },

    /**
     * @param {GoraExplorer.model.avro.ArrayNode} node
     */
    visitArrayNode: function(node) {
        this.countFamilyField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.BooleanNode} node
     */
    visitBooleanNode: function(node) {
        this.countFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.BytesNode} node
     */
    visitBytesNode: function(node) {
        this.countFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.DoubleNode} node
     */
    visitDoubleNode: function(node) {
        this.countFamilyColumnField(node) ;
    },

    /**
     * This method is the main entrance, although can be called in some recursivity
     * @param {GoraExplorer.model.avro.EntityNode} node
     */
    visitEntityNode: function(node) {
        var me = this ;
        node.eachChild(function(fieldNode) {
            fieldNode.accept(me) ;
        }) ;
    },

    /**
     * @param {GoraExplorer.model.avro.FloatNode} node
     */
    visitFloatNode: function(node) {
        this.countFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.IntNode} node
     */
    visitIntNode: function(node) {
        this.countFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.LongNode} node
     */
    visitLongNode: function(node) {
        this.countFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.MapNode} node
     */
    visitMapNode: function(node) {
        this.countFamilyField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.RecordNode} node
     */
    visitRecordNode: function(node) {
        this.countFamilyColumnField(node) ;
    },

    /**
     * @param {GoraExplorer.model.avro.StringNode} node
     */
    visitStringNode: function(node) {
        this.countFamilyColumnField(node) ;
    }

}) ;