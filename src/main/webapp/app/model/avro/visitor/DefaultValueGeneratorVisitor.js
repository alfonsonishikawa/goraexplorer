/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Given any SchemaNode, generates the default value of that node.
 * This visitor has several entry points:
 *
 * * {@link #createDefaultValue}: generates the default value
 * * {@link #createFirstLevelNonNullDefaultValue}: generates the default value, but the first level of the value will not be null
 *
 */
Ext.define('GoraExplorer.model.avro.visitor.DefaultValueGeneratorVisitor', {
    extend: 'GoraExplorer.model.avro.visitor.AbstractSchemaVisitor',

    /**
     * {GoraExplorer.model.avro.EntitiesNode} The tree root holding all possible entities
     */
    _rootEntitiesNode: null,

    /**
     * Flag to know when we are building the first level non-null value
     */
    _firstLevelNonNull: false,

    /**
     * @param {GoraExplorer.model.avro.EntitiesNode} rootEntitiesNode - The root "EntitiesNode" of the schema tree, where the visitor will be visiting
     */
    constructor: function(rootEntitiesNode) {
        var me = this ;

        if (!Ext.isDefined(rootEntitiesNode)) {
            Ext.raise('Missing "rootEntitiesNodes" parameter at DefaultValueGeneratorVisitor constructor') ;
        }

        me._rootEntitiesNode = rootEntitiesNode ;
    },

    /**
     * This method is a main entrance. Generates the default value for a node.
     *
     * @param {GoraExplorer.model.avro.EntityNode} node - Entity to generate the default value
     * @returns {Object/Numeric/String/Array/null} - The default value for that node
     * @public
     */
    createDefaultValue: function(node) {
        var me = this ;

        me._firstLevelNonNull = false ;
        return node.accept(me) ;
    },

    /**
     * This method is the main entrance. Generates the default value for a node, but the first level won't be null.
     *
     * @param {GoraExplorer.model.avro.EntityNode} node - Entity to generate a non-null the default value
     * @returns {Object/Numeric/String/Array} - The default value for that node, different of null
     * @public
     */
    createFirstLevelNonNullDefaultValue: function(node) {
        var me = this ;

        me._firstLevelNonNull = true ;

        return node.accept(me) ;
    },

    /**
     * @param {GoraExplorer.model.avro.ArrayNode} node - Array node to generate its default value
     * @return  - An empty array or null
     */
    visitArrayNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        } else {
            return [] ;
        }
    },

    /**
     * @param {GoraExplorer.model.avro.BooleanNode} node
     * @return {Boolean/null} Boolean default value or null
     */
    visitBooleanNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultBooleanValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        }

        if (!Ext.isEmpty(defaultValue)) {
            return defaultValue ;
        }

        return false ; // The default when no default
    },

    /**
     * @param {GoraExplorer.model.avro.BytesNode} node
     * @return {String/null} An empty string or null
     */
    visitBytesNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        } else {
            return '' ;
        }
    },

    /**
     * @param {GoraExplorer.model.avro.DoubleNode} node
     * @return {Numeric/null} The configured default value, or null.
     */
    visitDoubleNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultDoubleValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        }

        if (!Ext.isEmpty(defaultValue)) {
            return defaultValue ;
        }

        return 0 ; // The default when no default
    },

    /**
     * @param {GoraExplorer.model.avro.EntityNode} node
     * @returns {Object/null} - An object with the fields with its default values, or null
     */
    visitEntityNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = {
            } ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        }

        // At this moment, IGNORE ANY DEFAULT VALUE - almost all are wrong defined, not considering mandatory fields -
        me._firstLevelNonNull = false ; //We are no more at the first level
        node.eachChild(function(fieldNode) {
            var fieldDefaultValue = fieldNode.accept(me),
                fieldName = fieldNode.get('name');

            defaultValue[fieldName] = fieldDefaultValue ;
        }) ;

        return defaultValue ;
    },

    /**
     * @param {GoraExplorer.model.avro.FloatNode} node
     * @return {Numeric/null} The configured default value, or null.
     */
    visitFloatNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultFloatValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        }

        if (!Ext.isEmpty(defaultValue)) {
            return defaultValue ;
        }

        return 0 ; // The default when no default
    },

    /**
     * @param {GoraExplorer.model.avro.IntNode} node
     * @return {Numeric/null} The configured default value, or null.
     */
    visitIntNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultIntValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        }

        if (!Ext.isEmpty(defaultValue)) {
            return defaultValue ;
        }

        return 0 ; // The default when no default
    },

    /**
     * @param {GoraExplorer.model.avro.LongNode} node
     * @return {Numeric/null} The configured default value, or null.
     */
    visitLongNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultLongValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        }

        if (!Ext.isEmpty(defaultValue)) {
            return defaultValue ;
        }

        return 0 ; // The default when no default
    },

    /**
     * @param {GoraExplorer.model.avro.MapNode} node
     * @return {Object/null} - An empty map (Object) or null
     */
    visitMapNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        } else {
            return {} ;
        }
    },

    /**
     * Generates de default value, dereferencing the referenced Entity if needed
     * @param {GoraExplorer.model.avro.RecordNode} node
     * @return {Object/null} The same as EntityNode: An object with the fields with its default values, or null
     */
    visitRecordNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        } else {
            var recordFieldTypeName = node.get('recordFieldTypeName'),
                referencedEntityNode = me._rootEntitiesNode.findChild('name', recordFieldTypeName) ;

            return referencedEntityNode.accept(me) ;
        }
    },

    /**
     * @param {GoraExplorer.model.avro.StringNode} node
     * @return {Array/null} An empty array or null
     */
    visitStringNode: function(node) {
        var me = this,
            nullableField = node.get('nullable'),
            defaultNullValue = node.get('defaultNullValue'),
            defaultValue = node.get('defaultStringValue') ;

        if (nullableField && defaultNullValue && !me._firstLevelNonNull) {
            return null ;
        }

        if (!Ext.isEmpty(defaultValue)) {
            return defaultValue ;
        }

        return '' ; // The default when no default
    }


}) ;