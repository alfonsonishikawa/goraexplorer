/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.model.Table', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.identifier.Negative',
        'Ext.data.validator.Length',
        'Ext.data.validator.Presence'
    ],

    idProperty: 'id',
    identifier: {
        type: 'negative',
        prefix: '' // Without this, the identifier will be created as string-sequence
    },

    fields: [
        { name: 'id', type: 'integer' },
        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'avroSchema', type: 'string' },
        { name: 'mapping', type: 'string' },
        { name: 'keyClass', type: 'string', defaultValue: 'java.lang.String' },
        {
            name: 'connection',
            type: 'int',
            reference: {
                parent: 'GoraExplorer.model.Connection',
                role: 'connection',
                inverse: {
                    role: 'tables',
                    storeConfig: {
                        trackRemoved: true
                    }
                }
            }
        }
    ],

    validators: {
        name: [
            { type: 'presence' },
            { type: 'length', min: 1, max: 50}
        ],
        description: [
            { type: 'length', max: 500}
        ],
        keyClass:[
            { type: 'presence'},
            { type: 'length', max: 50}
        ],
        connection: { type: 'presence' },
        avroSchema: { type: 'presence' }
    },

    proxy: {
        type: 'rest',
        url: GoraExplorer.applicationHelper.getRestUrl('tables'),
        withCredentials: true
    }

});
