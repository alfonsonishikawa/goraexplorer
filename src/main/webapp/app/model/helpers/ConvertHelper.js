/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Model Helpers functions
 */
Ext.define('GoraExplorer.model.helpers.ConvertHelper', {
    singleton: true,

    /**
     * Generates a convert function that when the field is empty, returns a default value
     * @param defaultValue
     * @returns {Function}
     */
    valueIfEmpty: function(defaultValue) {
        return function(value) {
            if (Ext.isEmpty(value)) {
                return defaultValue ;
            }
            return value;
        }
    }

});
