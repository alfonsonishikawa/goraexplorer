/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Controller for the window to Create OR Edit a connection
 */
Ext.define('GoraExplorer.view.connections.NewEditTableController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.new-edit-table',

    uses: [
        'GoraExplorer.helpers.ApplicationHelper',
        'GoraExplorer.helpers.toast.ToastHelper'
    ],

    /**
     * Saves the connection from this session. If it is a new connection, adds it to the connections store.
     * Performs the sync
     */
    onSaveClick: function() {
        var me = this;

        if (me.lookupReference('tableForm').isValid()) {
            me.getView().mask(_('Saving')) ;

            // Saves the modification into the parent session
            me.getSession().save() ;

            var viewModel = me.getViewModel(),
                isEdit = me.getViewModel().get('isEdit') ;

            // Load now the 'table' instance from the parent session
            // "parent* " is the table in the "parent session"
            var newTableId = viewModel.get('table').get('id'),
                parentTable = me.getView().lookupSession(true).getRecord('GoraExplorer.model.Table', newTableId),
                parentTablesStore = me.getView().parentConnection.tables() ;

            if (!isEdit) {
                // ! Edit. New
                parentTable.set('connection', me.getView().parentConnection);
                parentTablesStore.add(parentTable) ;
            }

            // SYNC the connections store
            parentTablesStore.sync(
                GoraExplorer.applicationHelper.handleStoreSync({
                    successMessage: _('Table saved successfully'),
                    failureMessage: _('Error saving the table. Will try later'),
                    afterCompletion: function(batch, options) {
                        this.getView().close() ;
                    },
                    scope: me
                })
            ) ;
        } else {
            GoraExplorer.toastHelper.showErrorToast(
                _('Could not create the table because the data in the form is incomplete'),
                _('Invalid data')
            ) ;
        }
    },

    onCancelClick: function() {
        this.getView().close() ;
    }
});
