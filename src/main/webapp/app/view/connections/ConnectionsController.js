/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.ConnectionsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.connections',

    uses: [
        'GoraExplorer.model.avro.builder.HBaseSchemaTreeBuilder',
        'GoraExplorer.view.connections.NewEditConnection',
        'GoraExplorer.view.connections.NewEditTable',
        'GoraExplorer.view.connections.wizard.NewTable',
        'GoraExplorer.view.connections.wizard.NewTableModel'
    ],

    /**
     * Reload the connections shown in the grid
     */
    onRefreshConnections: function() {
        var grid = this.lookupReference('connectionsGrid') ;
        grid.getSelectionModel().deselectAll();
        this.getViewModel().get('currentUser').connections().load() ;
    },

    /**
     * When a connection is selected, the tables must be deselected. The grid maintains the selection in the store
     * alhough the store changes from the bind.
     * @param selMode
     * @param selectedRecords
     */
    onConnectionsGridSelectionChange: function (selMode, selectedRecords) {
        var me = this ;
        me.lookupReference('tablesGrid').getSelectionModel().deselectAll() ;
    },

    /**
     * Handler for the click to create a new connection
     */
    onNewConnectionClick: function() {
        var me = this ;
        Ext.widget('new-edit-connection').show() ;
    },

    /**
     * Handler for the click to edit a connection
     */
    onEditConnectionClick: function() {
        var me = this ;
        Ext.widget('new-edit-connection', {
            isEdit: true,
            editConnectionEntity: me.lookupReference('connectionsGrid').selection
        }).show() ;
    },

    /**
     * Handler for the click to delete a connection
     */
    onDeleteConnectionClick: function() {
        var me = this,
            connectionToDelete = me.lookupReference('connectionsGrid').selection ;

        // Ask for confirmation
        Ext.MessageBox.show({
            title: _('Delete connection?'),
            message: Ext.String.format(_('The connection "{0}" is going to be deleted'), connectionToDelete.get('name')),
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                var me = this;

                if (btn === 'ok') {
                    // Delete from store + sync
                    var connectionsStore = this.lookupReference('connectionsGrid').getStore(),
                        connectionToDelete = me.lookupReference('connectionsGrid').selection;
                    connectionsStore.remove(connectionToDelete);
                    connectionsStore.sync(
                        GoraExplorer.applicationHelper.handleStoreSync({
                            successMessage: _('Connection deleted successfully'),
                            failureMessage: _('Error deleting the connection')
                        })
                    );
                }
            },
            scope: me
        }) ;
    },

    /**
     * Handler for the click to create a new table
     */
    onNewTableClick: function() {
        var me = this ;
        Ext.widget('new-edit-table', {
            parentConnection: me.lookupReference('connectionsGrid').selection
        }).show() ;
    },

    /**
     * Handler for the click to create a new table using the wizzard that access the native backend metadata
     */
    onNewWizardTableClick: function () {
        var me = this ;
        Ext.widget('wizard-newtable', {
            parentConnection: me.lookupReference('connectionsGrid').selection
        }).show() ;
    },

    /**
     * Handler for the click to edit an existing table using the wizzard that access the native backend metadata
     */
    onEditWizardTableClick: function () {
        var me = this,
            wizardWindow = Ext.widget('wizard-newtable', {
                parentConnection: me.lookupReference('connectionsGrid').selection
                }),
            parentEditedTable = me.getViewModel().get('tablesGrid.selection'),
            sessionEditedTable = wizardWindow.getSession().getRecord('GoraExplorer.model.Table', parentEditedTable.get('id')),
            avroSchemaEntities = GoraExplorer.schemaTreeBuilder.buildFromJSON(parentEditedTable.get('avroSchema'));

        GoraExplorer.hbaseSchemaTreeBuilder.addMappingInfo(avroSchemaEntities, parentEditedTable.get('mapping'));

        // Delete the autocreated table
        wizardWindow.getViewModel().get('table').erase() ;
        // Set the table to be edit, got from the proper session
        wizardWindow.getViewModel().set('table', sessionEditedTable) ;
        wizardWindow.getViewModel().get('avroSchemaStore').setRoot(avroSchemaEntities) ;
        wizardWindow.getViewModel().set('nativeTableName', GoraExplorer.hbaseSchemaTreeBuilder.getNativeTableName(avroSchemaEntities, parentEditedTable.get('mapping'))) ;
        wizardWindow.show() ;
    },

    /**
     * Handler for the click to edit a table
     */
    onEditTableClick: function() {
        var me = this ;
        Ext.widget('new-edit-table', {
            isEdit: true,
            parentConnection: me.lookupReference('connectionsGrid').selection,
            editTableEntity: me.lookupReference('tablesGrid').selection
        }).show() ;
    },

    /**
     * Handler for the click to delete a table
     */
    onDeleteTableClick: function() {
        var me = this,
            tableToDelete = me.lookupReference('tablesGrid').selection ;

        // Ask for confirmation
        Ext.MessageBox.show({
            title: _('Delete table?'),
            message: Ext.String.format(_('The table "{0}" is going to be deleted'), tableToDelete.get('name')),
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                var me = this;

                if (btn === 'ok') {
                    // Delete from store + sync
                    var connection = me.lookupReference('connectionsGrid').selection,
                        tablesStore = connection.tables() ;
                    tablesStore.remove(tableToDelete) ;
                    tablesStore.sync(
                        GoraExplorer.applicationHelper.handleStoreSync({
                            successMessage: _('Table deleted successfully'),
                            failureMessage: _('Error deleting the table')
                        })
                    );
                }
            },
            scope: me
        }) ;
    },

    /**
     * Handler for the click to open the selected table in the grid
     */
    onOpenTableAction: function () {
        var me = this ;
        var tableEntity = me.lookupReference('tablesGrid').getSelection()[0];
        this.redirectTo(tableEntity) ;
    }

});