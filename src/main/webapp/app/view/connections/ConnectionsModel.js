/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * ViewModel of the Connections section
 */
Ext.define('GoraExplorer.view.connections.ConnectionsModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.connections',

    emptyStore: Ext.create('Ext.data.Store'),

    formulas: {

        tablesGridStore: {
            bind: '{connectionsGrid.selection}',
            get: function(connection) {
                return connection != null ? connection.tables() : this.emptyStore;
            }
        }
    }

});
