/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Controller for the window to Create OR Edit a connection
 */
Ext.define('GoraExplorer.view.connections.NewEditConnectionController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.new-edit-connection',

    uses: [
        'GoraExplorer.helpers.ApplicationHelper',
        'GoraExplorer.helpers.toast.ToastHelper'
    ],

    /**
     * Saves the connection from this session. If it is a new connection, adds it to the connections store.
     * Performs the sync
     */
    onSaveClick: function() {
        var me = this;

        if (me.lookupReference('connectionForm').isValid()) {
            me.getView().mask(_('Saving')) ;

            // Saves the modification into the parent session
            me.getSession().save() ;

            var viewModel = me.getViewModel(),
                connectionsStore = viewModel.get('currentUser').connections() ;

            var isEdit = me.getViewModel().get('isEdit') ;
            if (!isEdit) {
                // Since we're not editing, we have a newly inserted record. Grab the id of
                // that record that exists in the child session
                // Can't assign a record from one session directly to other session
                var newConnectionId = viewModel.get('connection').get('id');
                // lookupSession(true) retrieves the parent session
                connectionsStore.add(me.getView().lookupSession(true).getRecord('GoraExplorer.model.Connection', newConnectionId)) ;
            }

            // SYNC the connections store
            connectionsStore.sync(
                GoraExplorer.applicationHelper.handleStoreSync({
                    successMessage: _('Connection saved successfully'),
                    failureMessage: _('Error saving the connection. Will try later'),
                    afterCompletion: function(batch, options) {
                        this.getView().close() ;
                    },
                    scope: me
                })
            ) ;
        } else {
            GoraExplorer.toastHelper.showErrorToast(
                _('Could not create the connection because the data in the form is incomplete'),
                _('Invalid data')
            ) ;
        }
    },

    onCancelClick: function() {
        this.getView().close() ;
    }
});
