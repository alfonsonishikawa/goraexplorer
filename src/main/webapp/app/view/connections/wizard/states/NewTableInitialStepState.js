/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Common initial step state for the New Table Wizard.
 * The form is MainInfo form
 */
Ext.define('GoraExplorer.view.connections.wizard.states.NewTableInitialStepState', {
    extend: 'GoraExplorer.view.connections.wizard.states.AbstractNewTableState',

    uses: [
        'GoraExplorer.helpers.toast.ToastHelper',
        'GoraExplorer.view.connections.wizard.states.NewTableSchemaState'
    ],

    statics: {
        toastHelper: GoraExplorer.helpers.toast.ToastHelper
    },

    isInitialState: true,

    /**
     * @override
     */
    onEnterState: function(wizardView, previousState) {
        Ext.log({level: 'debug'}, 'Entered Initial Step State (main info)') ;

        var viewModel = wizardView.getViewModel() ;

        Ext.suspendLayouts() ;
        // Set step
        viewModel.set('wizardStep', 0) ;

        wizardView.setActiveItemOrCreate('wizard-newtable-maininfo') ;
        wizardView.center() ;
        Ext.resumeLayouts(true) ;
    },

    /**
     * Performs the operation of wanting to transition to the next state.
     * Checks if the data is correct.
     * If all is correct, transitions to the avro schema definition state.
     * @param {GoraExplorer.view.connections.wizard.NewTable} wizardView - The affected instance
     */
    nextState: function(wizardView) {
        var me = this,
            viewModel = wizardView.getViewModel(),
            mainInfoForm = wizardView.query('wizard-newtable-maininfo')[0] ;

        if ( !mainInfoForm.isValid() ) {
            GoraExplorer.toastHelper.showErrorToast(_('The form data is invalid')) ;
        } else {
            // Valid data -> Transition to new table schema state
            var nextState = Ext.create('GoraExplorer.view.connections.wizard.states.NewTableSchemaState') ;
            viewModel.setState(nextState) ;
        }
    }

}) ;