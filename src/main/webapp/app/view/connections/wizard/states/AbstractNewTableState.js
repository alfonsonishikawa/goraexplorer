/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.states.AbstractNewTableState', {
    extend: 'Ext.Component', // To have bindings to config

    config: {

        /**
         *@cfg {Boolean} isInitialState - Should be "true" for states that are initial
         */
        isInitialState: false,

        /**
         * @cfg {Bolean} isEndState - Should be "true" for states that are ending states
         */
        isEndState: false

    },

    /**
     * This method is first executed when an instance has just transitioned to this state.
     * This method should do:
     *
     * - Update Table View ViewModel's `wizardStep`
     * - Set the active card in the wizard (or create it if does not exists)
     *
     * @param {GoraExplorer.view.connections.wizard.NewTable} wizardView - The instance that has transitioned to this state
     * @param {GoraExplorer.view.connections.wizard.states.AbstractNewTableState} previousState
     */
    onEnterState: function(wizardView, previousState) {

    },

    /**
     * This method is executed just before an instance transitions to a new state and is leaving this state.
     * @param {GoraExplorer.view.connections.wizard.NewTable} wizardView - The instance leaving this state
     * @param {GoraExplorer.view.connections.wizard.states.AbstractNewTableState} nextState
     */
    onExitState: function(wizardView, nextState) {

    },

    /**
     * Performs the operation of wanting to transition to the next state
     * @param {GoraExplorer.view.connections.wizard.NewTable} wizardView - The affected instance
     */
    nextState: function(wizardView) {

    },

    /**
     * Perform the operation of wanting to transition to the prevous state
     * @param {GoraExplorer.view.connections.wizard.NewTable} wizardView - The affected instance
     */
    prevState: function(wizardView) {

    }


}) ;