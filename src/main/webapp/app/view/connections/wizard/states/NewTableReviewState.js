/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Ending state to review the result of the schema and mapping generated
 * OnNext saves the table.
 */
Ext.define('GoraExplorer.view.connections.wizard.states.NewTableReviewState', {
    extend: 'GoraExplorer.view.connections.wizard.states.AbstractNewTableState',

    uses: [
        'GoraExplorer.helpers.toast.ToastHelper',
        'GoraExplorer.helpers.ApplicationHelper',
        'GoraExplorer.model.avro.visitor.JsonSchemaGeneratorVisitor'
    ],

    isEndState: true,

    /**
     * @override
     */
    onEnterState: function(wizardView, previousState) {
        Ext.log({level: 'debug'}, 'Entered Review State') ;

        var viewModel = wizardView.getViewModel(),
            controller = wizardView.getController(),
            avroSchemaStore = viewModel.get('avroSchemaStore'),
            rootNode = avroSchemaStore.getRoot(),
            mainEntity = rootNode.firstChild ;

        Ext.suspendLayouts() ;
        // Set step
        viewModel.set('wizardStep', 3) ;

        // Generate the Avro JSON schema
        var schemaGenerator = Ext.create(
            'GoraExplorer.model.avro.visitor.JsonSchemaGeneratorVisitor',
            viewModel.get('currentUser.username'),
            rootNode) ;
        viewModel.set('table.avroSchema', JSON.stringify(schemaGenerator.generateJsonSchema(mainEntity), null, 2)) ;

        wizardView.setActiveItemOrCreate('wizard-newtable-review') ;
        wizardView.center() ;
        Ext.resumeLayouts(true) ;
    },

    prevState: function(wizardView) {
        var me = this,
            viewModel = wizardView.getViewModel(),
            backendType = viewModel.get('backendMetadata').get('type') ;

        Ext.MessageBox.confirm(_('Changes will be lost'),
            _('Any changes made to the schema or mapping in this card will be lost. Are you sure you want to go to previous card?'),
            function(buttonId) {
                if (buttonId === 'yes') {
                    // Transition back to widget backend-dependent
                    var nextState = Ext.create('GoraExplorer.view.connections.wizard.states.NewTable' + backendType + 'State') ;
                    viewModel.setState(nextState) ;
                }
            }
        ) ;
    },

    nextState: function(wizardView) {
        // FINISH
        var me = this,
            viewModel = wizardView.getViewModel(),
            tableToSave = viewModel.get('table');

        if (tableToSave.dirty === false) {
            // Nothing changed
            GoraExplorer.toastHelper.showInfoToast(_('Nothing changed')) ;
            wizardView.close() ;
            return ;
        }

        if (tableToSave.isValid()) {
            wizardView.mask(_('Saving')) ;

            // Save the session into the parent session
            wizardView.getSession().save() ;

            // Load now the 'table' instance from the parent session
            // "parent* " is the table in the "parent session"
            var parentTable = wizardView.lookupSession(true).getRecord('GoraExplorer.model.Table', tableToSave.get('id')),
                parentTablesStore = viewModel.get('parentConnection').tables() ;

            if (!parentTablesStore.contains(parentTable)) {
                parentTablesStore.add(parentTable);
            }

            // SYNC the connections store
            parentTablesStore.sync(
                GoraExplorer.applicationHelper.handleStoreSync({
                    successMessage: _('Table saved successfully'),
                    failureMessage: _('Error saving the table. Will try later'),
                    success: function (batch, options) {
                        wizardView.close() ;
                    },
                    failure: function (batch, options) {
                        wizardView.unmask() ;
                    },
                    scope: me
                })
            ) ;

        } else {
            GoraExplorer.toastHelper.showErrorToast(_('Some of the table values are erroneous. Can not save the table connection.'), _('Unexpected error!')) ;
        }

    }

}) ;