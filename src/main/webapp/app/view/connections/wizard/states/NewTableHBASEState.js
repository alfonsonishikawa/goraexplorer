/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * HBase especific state for a new table, to create the schema and mapping
 */
Ext.define('GoraExplorer.view.connections.wizard.states.NewTableHBASEState', {
    extend: 'GoraExplorer.view.connections.wizard.states.AbstractNewTableState',

    uses: [
        'GoraExplorer.helpers.toast.ToastHelper',
        'GoraExplorer.model.avro.EntityNode',
        'GoraExplorer.model.avro.visitor.HBaseMappingGeneratorVisitor',
        'GoraExplorer.model.avro.visitor.HBaseMappingValidatorVisitor',
        'GoraExplorer.model.nativemetadata.HBaseTableMetadata',
        'GoraExplorer.view.connections.wizard.NewTableHBaseMapping',
        'GoraExplorer.view.connections.wizard.NewTableReview',
        'GoraExplorer.view.connections.wizard.states.NewTableReviewState'
    ],

    /**
     *
     * @param wizardView
     * @param previousState
     * @verride
     */
    onEnterState: function(wizardView, previousState) {
        Ext.log({level: 'debug'}, 'Entered HBase mapping State') ;

        var viewModel = wizardView.getViewModel(),
            avroSchemaStore = viewModel.get('avroSchemaStore') ;

        Ext.suspendLayouts() ;
        // Set step
        viewModel.set('wizardStep', 2) ;

        // Load native table metadata (the table was selected in the previous step)
        var tableMetadata = Ext.create('GoraExplorer.model.nativemetadata.HBaseTableMetadata') ;
        tableMetadata.load({
            params: {
                connectionId: viewModel.get('table.connection').get('id'),
                tableName: viewModel.get('nativeTableName')
            },
            callback: function(tableMetadataModel) {
                viewModel.set('nativeTableMetadata', tableMetadataModel) ;
            }
        });

        /*
         * Before activating the panel, applies a FILTER to the ENTITIES STORE ('{avroSchemaStore}') in the ViewModel, since ExtJS
         * does not allow Chaining TreeStores.
         *
         * When getting out of this card, the filter must be reverted
         */
        avroSchemaStore.filterBy(
            function(item) {
                var isRootNode = (item instanceof GoraExplorer.model.avro.EntityNode) && item.parentNode === null ;
                var isMainEntity = (item instanceof GoraExplorer.model.avro.EntityNode) && item.isFirst() ;
                var isFieldOfMainEntity = false ;
                if ( !(item instanceof GoraExplorer.model.avro.EntityNode) ) {
                    isFieldOfMainEntity = item.parentNode && item.parentNode.isFirst() ;
                }
                return isRootNode || isMainEntity || isFieldOfMainEntity ;
            }
        ) ;

        wizardView.setActiveItemOrCreate('wizard-newtable-hbase-mapping') ;
        wizardView.center() ;
        Ext.resumeLayouts(true) ;
    },

    /**
     * Removes the applied filter of the ENTITIES STORE ('{avroSchemaStore}')
     *
     * @param {GoraExplorer.view.connections.wizard.NewTable} wizardView - The instance leaving this state
     * @param {GoraExplorer.view.connections.wizard.states.AbstractNewTableState} nextState
     * @override
     */
    onExitState: function(wizardView, nextState) {
        var viewModel = wizardView.getViewModel(),
            avroSchemaStore = viewModel.get('avroSchemaStore') ;
        avroSchemaStore.clearFilter() ;
    },

    prevState: function(wizardView) {
        // Transition to Schema definition state
        var me = this,
            viewModel = wizardView.getViewModel() ;

        var nextState = Ext.create('GoraExplorer.view.connections.wizard.states.NewTableSchemaState') ;
        viewModel.setState(nextState) ;
    },

    /**
     * Performs the operation of wanting to transition to the next state.
     * Checks if the data is correct.
     * If all is correct, generates the mapping and transitions to the review state
     * @param {GoraExplorer.view.connections.wizard.NewTable} wizardView - The affected instance
     */
    nextState: function(wizardView) {
        var me = this,
            viewModel = wizardView.getViewModel(),
            avroSchemaStore = viewModel.get('avroSchemaStore'),
            rootNode = avroSchemaStore.getRoot(),
            mainEntity = rootNode.firstChild ;

        // Validate HBase mapping
        var hbaseMappingValidatorVisitor = Ext.create('GoraExplorer.model.avro.visitor.HBaseMappingValidatorVisitor', rootNode) ;
        if (hbaseMappingValidatorVisitor.isValid(mainEntity)) {
            // Valid data
            // Generate mapping
            var mappingGeneratorVisitor = Ext.create(
                'GoraExplorer.model.avro.visitor.HBaseMappingGeneratorVisitor',
                viewModel.get('currentUser.username'),
                viewModel.get('nativeTableName'),
                viewModel.get('table.keyClass'),
                rootNode,
                true /*prettyPrint*/) ;
            viewModel.set('table.mapping', mappingGeneratorVisitor.generateHBaseMapping(mainEntity)) ;

            // Transition to review
            var nextState = Ext.create('GoraExplorer.view.connections.wizard.states.NewTableReviewState') ;
            viewModel.setState(nextState) ;
        } else {
            // Errors in the mapping
            var mappingErrors = hbaseMappingValidatorVisitor.getErrors() ;
            if (mappingErrors.columnDefinedErrorFields.length > 0) {
                var message = Ext.String.format(_('The following fields must not have a Column mapping defined: {0}'), mappingErrors.columnDefinedErrorFields.join()) ;
                GoraExplorer.toastHelper.showErrorToast(message) ;
            }
            if (mappingErrors.missingMappings.length > 0) {
                var message = Ext.String.format(_('The mapping is missing for the following fields: {0}'), mappingErrors.missingMappings.join()) ;
                GoraExplorer.toastHelper.showErrorToast(message) ;
            }
            if (Ext.Object.getKeys(mappingErrors.familyAndColumnsRegistry).length > 0) {
                Ext.Array.forEach(Ext.Object.getKeys(mappingErrors.familyAndColumnsRegistry), function(key){
                    var collidingFields = mappingErrors.familyAndColumnsRegistry[key] ;
                    var message = Ext.String.format(_('The following field\'s mappings collide: {0}'), collidingFields.join()) ;
                    GoraExplorer.toastHelper.showErrorToast(message) ;
                }) ;
            }
        }

    }

}) ;