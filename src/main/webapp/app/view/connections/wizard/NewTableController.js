/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.NewTableController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wizard-newtable',

    uses: [
        'GoraExplorer.helpers.ApplicationHelper'
    ],

    /**
     * Sets the configuration parameter 'parentConnection' of NewTable into the ViewModel.
     * Loads the native information about the backend, and fills the natives tables combobox
     * @param viewModel
     */
    initViewModel: function(viewModel) {
        var me = this,
            view = me.getView(),
            parentConnection = view.parentConnection,
            parentConnectionId = parentConnection.get('id');

        if ( parentConnectionId < 0 ) {
            Ext.raise(_('Can not create a table connection from a database connection waiting to be persisted'))
        }
        var initialState = Ext.create('GoraExplorer.view.connections.wizard.states.NewTableInitialStepState') ;
        viewModel.setState(initialState) ;
        viewModel.set('parentConnection', parentConnection) ;
        if (viewModel.get('table.connection') === null) {
            // new table
            viewModel.set('table.connection', parentConnectionId) ;
        }
        viewModel.get('backendMetadata').load({
            params: {
                connectionId: parentConnectionId
            },
            scope: me,
            callback: function (records, operation, success) {
                GoraExplorer.applicationHelper.handleFailedLoadResponse(operation, success) ;
            }
        }) ;

        this.callParent(arguments) ;
    },

    /**
     * Handles the "cancel" button click. Simply close the window (and gets destroye)
     */
    onCancelClick: function(button, e, eOpts) {
        this.getView().close() ;
    },

    /**
     * Executes the command prevState on the current state to try to transition to the previous card
     */
    onPrevClick: function(button, e, eOpts) {
        var me = this,
            wizardView = me.getView(),
            viewModel = me.getViewModel() ;

        viewModel.get('wizardState').prevState(wizardView) ;
    },

    /**
     * Executes the command nextState on the current state to try to transition to the next card
     */
    onNextClick: function(button, e, eOpts) {
        var me = this,
            wizardView = me.getView(),
            viewModel = me.getViewModel() ;

        viewModel.get('wizardState').nextState(wizardView) ;
    }

}) ;