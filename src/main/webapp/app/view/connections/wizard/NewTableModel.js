/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.NewTableModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.wizard-newtable',

    requires: [
        'GoraExplorer.view.connections.wizard.states.NewTableInitialStepState',
        'GoraExplorer.model.nativemetadata.BackendMetadata'
    ],

    uses: [
        'GoraExplorer.store.avro.AvroSchemaStore',
        'GoraExplorer.model.Table'
    ],

    /**
     * Makes the associated view to transition to a new state.
     * @param {'GoraExplorer.view.connections.wizard.states.AbstractNewTableState'} newState - The new state
     */
    setState: function(nextState) {
        var me = this,
            view = me.getView(),
            previousState = me.get('wizardState') ;

        if (previousState) {
            previousState.onExitState(view, nextState);
        }
        me.set('wizardState', nextState) ;
        nextState.onEnterState(view, previousState) ;
    },

    data: {
        /**
         * The native table name the user wants to connect to. Can be one already existing at the backend or a new name
         */
        nativeTableName: '',

        /**
         * {GoraExplorer.view.connections.wizard.states.AbstractNewTableState} - Holds the state of the table model wizard
         */
        wizardState: null,

        /**
         * {Boolean} - Flag that must be true when the backend is unsupported.
         */
        unsupportedBackend: false,

        /**
         * {Number} - The total number of state-steps in the wizard
         */
        wizardNumSteps: 4,

        /**
         * {Number} - Current step number [0, wizardNumSteps-1]
         */
        wizardStep: 0,

        /**
         * {Object} - The native table metadata of the table being connected to. Null if no data available.
         *          - The class depends on the especific backend. Can be:
         *          <ul>
         *              <li><code>GoraExplorer.model.nativemetadata.HBaseTableMetadata</code></li>
         *          </ul>
         */
        nativeTableMetadata: null
    },

    links: {
        /**
         * Table connection being created
         */
        table: {
            type: 'GoraExplorer.model.Table',
            create: true
        },

        /**
         * {GoraExplorer.model.nativemetadata.BackendMetadata} - Will hold the information from the backend metadata loaded when creating the NewTable view.
         */
        backendMetadata: {
            type: 'GoraExplorer.model.nativemetadata.BackendMetadata',
            create: true
        }
    },

    stores: {
        avroSchemaStore: {
            type: 'avroschemastore',
            create: true
        }
    },

    formulas: {
        /**
         * Creates a store with the list of tables, suitable for a combobox
         * @return {Ext.data.Store}
         */
        nativeTablesNameStore: {
            bind: '{backendMetadata.tables}',
            get: function(tablesArray) {
                var tablesComboboxStore = Ext.create('Ext.data.Store', {
                    fields: ['text']
                }) ;
                if (tablesArray) {
                    Ext.Array.forEach(tablesArray, function (tableName) {
                        tablesComboboxStore.add({text: tableName});
                    });
                }
                return tablesComboboxStore ;
            }
        },

        /**
         * Flag with Previous Button disabled state. Must be disabled when:
         * - The state of wizardState is initial
         * OR
         * - unsupportedBackend is true
         * @param get
         */
        prevDisabled: function (get) {
            var wizardState = get('wizardState.isInitialState'),
                unsupportedBackend = get('unsupportedBackend') ;
            return wizardState || unsupportedBackend ;
        },

        /**
         * ProgressBar value between 0 and 1, which depends on the number of steps and the current step.
         */
        progress: function (get) {
            var numSteps = get('wizardNumSteps'),
                currentStep = get('wizardStep') ;

            if (numSteps <= 1) {
                return 1;
            }
            return currentStep/(numSteps-1) ;
        }

    }

}) ;