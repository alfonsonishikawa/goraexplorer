/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.NewTableHBaseMappingModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.wizard-newtable-hbase-mapping',

    formulas: {

        // "isEntity" collides with ExtJS internal names :(
        selectedIsEntity: function(get) {
            return get('hbaseeEntitiesTreePanel.selection') instanceof GoraExplorer.model.avro.EntityNode ;
        },

        selectedIsMainEntity: function(get) {
            return get('selectedIsEntity') && get('hbaseeEntitiesTreePanel.selection').isFirst() ;
        },

        isArrayField: function(get) {
            return get('hbaseeEntitiesTreePanel.selection') instanceof GoraExplorer.model.avro.ArrayNode ;
        },

        isMapField: function(get) {
            return get('hbaseeEntitiesTreePanel.selection') instanceof GoraExplorer.model.avro.MapNode ;
        },

        /**
         * An HBase column must NOT be selected if it is an Array or Map. But the column must be disabled if
         * the main entity is selected.
         * @param get
         */
        hbaseColumnDisabled: function(get) {
            return get('selectedIsMainEntity') || get('isArrayField') || get('isMapField') ;
        },

        /**
         * Returns a store with the family names from native backend
         * @return {Ext.data.Store}
         */
        columnFamiliesStore: {
            /**
             * {GoraExplorer.model.nativemetadata.HBaseTableMetadata} - HBase table native metadata
             */
            bind: '{nativeTableMetadata.columnFamilies}',
            get: function(columnFamiliesArray) {
                var familiesStore = Ext.create('Ext.data.Store', {
                    fields: ['text']
                }) ;
                if (Ext.isArray(columnFamiliesArray)) {
                    if (columnFamiliesArray !== null && columnFamiliesArray.length > 0) {
                        Ext.Array.forEach(columnFamiliesArray, function (familyName) {
                            familiesStore.add({text: familyName});
                        });
                    }
                }
                return familiesStore ;
            }
        }

    }

}) ;