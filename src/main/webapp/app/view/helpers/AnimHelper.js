/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Animations Helper to animate Components, Elements, Views, etc...
 */
Ext.define('GoraExplorer.view.helpers.AnimHelper', {
    singleton:true,

    requires: [
        'Ext.fx.Easing'
    ],

    /**
     * Shakes horizontally the view element/component/view
     * @param target - The target element/component/view to shake
     * @param centerX [optional] - Initial and end (the same) coordenade to shake around
     */
    shake:function(targetView) {
        if (!Ext.fx.Easing.shake) {
            Ext.fx.Easing.self.addMembers({
                /**
                 * 'Shake' easing shakes around the "to" parameter.
                 * The "from" parameter determines the amplitude. For example, x from 20 to 45 means a
                 * shake around 45 with amplitude 15.
                 *
                 * @param n
                 * @return {number}
                 */
                shake: function(n) {
                    return Math.sin(n*2*Math.PI) + 1 ;
                }
            }) ;
        }
        targetView.animate({
            from: {
                x: targetView.getX()-20 // In "shake" easing, this determines the amplitude
            },
            to: {
                x: targetView.getX()
            },
            duration: 200,
            easing: 'shake'
        }) ;
    }
});