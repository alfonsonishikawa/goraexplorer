/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.ViewController', {
    extend: 'Ext.app.ViewController',

    uses: [
        'GoraExplorer.helpers.ApplicationHelper',
        'GoraExplorer.model.User'
    ],

    privates: {

        /**
         * Before action that checks authentication
         * @private
         */
        loginBeforeAction: function () {
            var me = this;
            var resumeOrStop = arguments[arguments.length - 1];
            me.ifLoggedIn()
                .then(
                    function () { // fullfill
                        Ext.create('Ext.util.DelayedTask', function () {
                            resumeOrStop.resume();
                        }).delay(10);
                    },
                    function ( failureValue ) { // failureValue = {record, operation}
                        resumeOrStop.stop(true);
                        GoraExplorer.applicationHelper.handleFailedLoadResponse(failureValue.operation, false, false) ;
                    }
                );
        },

        /**
         * Returns a promise that checks if the user is logged in, and try to load it's data if not.
         * @returns {Ext.Promise} - onSuccess and onFailure returns
         * ```
         * {
         *     record: {GoraExplorer.model.User},
         *     operation: {Ext.data.operation.Operation}
         * }
         * ```
         * @private
         */
        ifLoggedIn: function () {
            var me = this,
                deferred = new Ext.Deferred();

            if (me.getViewModel().get('currentUser') && me.getViewModel().get('currentUser').get('enabled')) {
                deferred.resolve();
            } else {
                GoraExplorer.model.User.load(null,{
                    scope: me,
                    failure: function(record, operation) {
                        deferred.reject({record: record, operation: operation});
                    },
                    success: function(record, operation) {
                        if (record.get('enabled')) {
                            if (!Ext.isEmpty(me.getViewModel().get('currentUser'))) {
                                me.getSession().evict(me.getViewModel().get('currentUser')) ;
                            }
                            me.getViewModel().set('currentUser', record);
                            deferred.resolve({record: record, operation: operation});
                        } else {
                            // Disabled user, exists, but should not operate
                            deferred.reject({record: record, operation: operation});
                        }
                    }
                },
                me.getSession()) ;
            }

            return deferred.promise;
        }
    }
});
