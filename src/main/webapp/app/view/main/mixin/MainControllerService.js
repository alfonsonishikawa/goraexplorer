/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Mixin implementing the MainController services. Implemented as a mixin since it is tightly coupled with the
 * MainController. The only reason is to keep the code clean.
 */
Ext.define('GoraExplorer.view.main.mixin.MainControllerService', {
    extend: 'Ext.Mixin',

    /**
     * Opens the tab for the given table in "Tables" section (new if does not exists)
     * @param {Number} tableId
     *
     * @return {Ext.promise.Promise} - Returns a promise whose 'then' function gets the created/shown card item
     */
    selectTable: function(tableId) {
        var me = this ;
        //  Go to section but do not change the url history
        me.lookupReference('navigationTreeList').suspendEvents() ;
        me.setCurrentView('tables') ;
        me.lookupReference('navigationTreeList').resumeEvents(true) ;

       return GoraExplorer.view.tables.TablesHelper.showOrCreateTableTab(tableId, me.lookupReference('tablestabpanel')) ;
    }

}) ;