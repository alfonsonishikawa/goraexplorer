/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.main.UserMenuController', {
    extend: 'GoraExplorer.view.ViewController',

    alias: 'controller.currentusertoolbarimagemenu',

    /**
     * Navigates to the settings page, redirecting to #settings
     */
    onSettingsClick: function() {
        var me = this;
        me.redirectTo('settings') ;
    },

    /**
     * Performs the logout calling /logout and redirecting to #home
     */
    onLogoutClick: function () {
        var me = this ;

        Ext.Ajax.request({
            url: GoraExplorer.applicationHelper.getRestUrl('logout'),
            method: 'GET',
            disableCaching: true
        }).then(
            function(response, opts) {
                GoraExplorer.getApplication().reloadApplication();
            },
            function (response, opts) {
                Ext.Msg.alert(_('Error on logout: ') + response.status, response.statusText) ;
            }
        );
    }

});