/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    uses: [
        'GoraExplorer.view.helpers.AnimHelper',
        'GoraExplorer.helpers.ApplicationHelper'
    ],

    /**
     * Submits the form to log in and fills the user data on success
     * @ param {Ext.button.Button} button
     * @ param {Event} event
     * @ param {Object} eOpts
     */
    doLoginClick: function (/*button, event, eOpts*/) {
        var me = this ;
        var form = me.lookupReference('form') ;

        me.getViewModel().set('loginError', false) ;

        if (!form.isValid()) {
            GoraExplorer.view.helpers.AnimHelper.shake(me.getView()) ;
            return;
        }

        // Form.submit does not work with CORS. Let's do an ajax submit
        // form.submit({
        //     clientValidation: true,
        //     url: GoraExplorer.applicationHelper.getRestUrl('login'),
        //     scope: me,
        //     success: me.onSuccess,
        //     failure: me.onFailure
        // });
        Ext.Ajax.request({
            url: GoraExplorer.applicationHelper.getRestUrl('login'),
            method: 'POST',
            withCredentials: true, // Very important for CORS. Otherwise, Cookies receiving will not be saved on client
            cors: true,
            useDefaultXhrHeader : true,
            params: form.getValues(),
            disableCaching: true,
            success: me.onSuccess,
            failure: me.onFailure,
            scope: me
        });
    },

    onSpecialKeyPress: function(field, e){
        var me = this ;

        if (e.getKey() == e.ENTER) {
            me.doLoginClick();
        }
    },

    privates: {

        /**
         * Loads the user profile information and hides the login window for the doLoginClick action
         * @param form
         * @param action
         */
        onSuccess: function(response, opts) {
            var me = this ;

            GoraExplorer.model.User.load(null,{
                scope: me,
                failure: function(record, operation) {
                    Ext.Msg.alert(_('Error'), Ext.String.format('{0} {1} - {2}',
                            operation.getError().status,
                            operation.getError().statusText,
                            operation.request.getUrl())) ;
                },
                success: function(record, operation){
                    var form = me.lookupReference('form') ;
                    me.getViewModel().getParent().set('currentUser', record) ;
                    form.reset();
                    me.getView().close() ;
                    // Redirect home it the login is the first action (no menu option selected => selection is undefined)
                    if (Ext.isEmpty(Ext.ComponentQuery.query('treelist[itemId=navigationTreeList]')[0].getSelection())) {
                        me.redirectTo('home') ;
                    }
                }
            }) ;
        },

        /**
         * Marks the login window as "login error", shakes the window, and waits for another try.
         * Used on doLoginClick action
         *
         * @param {Ext.form.Basic} form
         * @param {Ext.form.action.Submit} action
         */
        onFailure: function(response, opts) {
            var me = this ;

            if (response) {
                switch(response.status) {
                    case 0:
                        Ext.Msg.alert(_('Connection Error'), _('Error connecting the server')) ;
                        break;

                    case 401:
                        me.getViewModel().set('loginError', true) ;
                        GoraExplorer.view.helpers.AnimHelper.shake(me.getView()) ;
                        me.lookupReference('usernameField').focus().selectText() ;
                        break ;

                    default:
                        Ext.Msg.alert(_('Unknown error ') + response.status, response.statusText) ;
                        break ;
                }

            } else {
                Ext.Msg.alert(_('Unknown error'), action.method + ' ' + action.url) ;
            }
        }

    }

});
