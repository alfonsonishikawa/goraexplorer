/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.tables.TableTabController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tabletabcontroller',

    uses: [
        'GoraExplorer.helpers.ApplicationHelper',
        'GoraExplorer.helpers.toast.ToastHelper',
        'GoraExplorer.model.avro.visitor.DefaultValueGeneratorVisitorFactory',
        'GoraExplorer.model.GoraPersistent'
    ],

    /**
     * After rendering the table tab with the grid, start async load of the data
     * @param component
     * @param eOpts
     */
    onAfterTableTabRender: function( component, eOpts ) {
        var me = this;

        me.getViewModel().getStore('tableStore').load({
            scope: me,
            callback: function (records, operation, success) {
                GoraExplorer.applicationHelper.handleFailedLoadResponse(operation, success) ;
            }
        }) ;
    },

    /**
     * When an user clicks on a row, the root data values card of that row must be shown.
     * If the row is a new row, the values region has to be emptied first.
     *
     * @param {Ext.data.Model} selectedRecord - The selected record
     * @param htmlElementRow
     * @param {Number} rowIndex - The row index selected
     */
    onTableRowClick: function (table, selectedRecord, htmlElementRow, rowIndex) {
        var me = this,
            schemaEntitiesTree = me.getViewModel().get('tableSchemaEntitiesTree'),
            mainEntitySchemaNode = schemaEntitiesTree.firstChild, // When selecting a row, its schema is the first child of Entities Tree
            selectedRowRecordData = selectedRecord.getData(),
            valuesRegion = me.lookupReference('valuesRegion'),
            valuesRegionRecordData = valuesRegion.getViewModel().get('recordData') ;

        if (valuesRegionRecordData == null || valuesRegionRecordData.__id__ !== selectedRowRecordData.__id__) {
            Ext.suspendLayouts() ;

            // New row selected, delete all cards
            valuesRegion.removeAll(true);

            // Change region data
            valuesRegion.getViewModel().set('recordData', selectedRowRecordData) ;
            valuesRegion.getViewModel().set('editing', false) ;
            // Add new root card
            var cardItem = valuesRegion.addNewCard({
                bindPath: 'recordData',
                schemaNode: mainEntitySchemaNode
            }) ;
            // Select root card (cardId='recordData' (bindPath))
            valuesRegion.setSelectedCardId(md5('recordData')) ;

            Ext.resumeLayouts(false) ;
        }
    },

    /**
     * Handles before row selection: when editing, must give the possibility to cancel changing the row and losing the changes.
     * @param table
     * @param selectingRecord
     * @param rowIndex
     * @return {boolean}
     */
    onTableRowBeforeSelectClick: function(table, selectingRecord, rowIndex) {
        var me = this,
            valuesRegion = me.lookupReference('valuesRegion'),
            valuesRegionEditing = valuesRegion.getViewModel().get('editing');

        if (valuesRegionEditing) {
            Ext.Msg.confirm(
                _('Changes will be lost'),
                _('Changes not saved will be lost. Are you sure you want to quit editing?'),
                function(response) {
                    if (response === 'yes') {
                        valuesRegion.getViewModel().set('editing', false) ;
                        table.select(rowIndex) ;
                    }
                }
            ) ;
            return false ;
        }

    },

    /**
     * Handles the Refresh click on Filters Region reloading the table with the last filters
     */
    onRefreshClick: function() {
        var me = this ;
        me.lookupReference('tableGrid').getSelectionModel().deselectAll() ;
        me.lookupReference('valuesRegion').clear() ;
        me.getViewModel().get('tableStore').reload() ;
    },

    /**
     * Handles the Clear click on Filters Region when clearing the filter form
     * @param button
     */
    onClearClick: function() {
        var me = this ;
        me.lookupReference('filtersRegion').reset() ;
    },

    /**
     * Handles the Search click on Filters Region
     */
    onSearchClick: function() {
        var me = this,
            filtersForm = me.lookupReference('filtersRegion') ;

        if (!filtersForm.isValid()) {
            GoraExplorer.toastHelper.showInfoToast(_('Search filter is invalid')) ;
            return ;
        }

        me.lookupReference('tableGrid').getStore().removeAll() ;
        me.lookupReference('tableGrid').getStore().loadNextRows() ;
    },

    /**
     * Prompts for a new row key, and inserts the new record with that key
     */
    onAddNewRowClick: function() {
        var me = this,
            tableStore = me.lookupReference('tableGrid').getStore() ;

        Ext.Msg.prompt(_('Add new row'), _('Input the new row\'s key'),
            function(choice, newKey) {
                if (choice === 'ok') {
                    var rootEntitiesNode = me.getViewModel().get('tableSchemaEntitiesTree'),
                        defaultValueVisitor = GoraExplorer.defaultValueGeneratorVisitorFactory.getDefaultValueGeneratorVisitor(rootEntitiesNode),
                        itemDefaultValue = defaultValueVisitor.createDefaultValue(rootEntitiesNode.firstChild) ;

                    if (!Ext.isDefined(newKey)) {
                        GoraExplorer.helpers.toast.ToastHelper.showErrorToast(_('Unexpected error. The key seems not to be defined.')) ;
                        return ;
                    }

                    if (tableStore.getById(newKey) !== null) {
                        GoraExplorer.helpers.toast.ToastHelper.showErrorToast(_('The key already exists.')) ;
                        return ;
                    }

                    // else all OK
                    // Add the value to the store
                    //itemDefaultValue['__id__'] = newKey ;
                    var newRowModel = me.getSession().createRecord('GoraExplorer.model.GoraPersistent', itemDefaultValue) ;
                    newRowModel.setId(newKey) ; // The creation lacks id so the model is created as phantom
                    newRowModel.save(
                        GoraExplorer.applicationHelper.handleModelOperation({
                            successMessage: _('Row added successfully'),
                            failureMessage: _('Error adding the row'),
                            success: function(batch, operation) {
                                tableStore.add(newRowModel) ;
                            },
                            failure: function(batch, operation) {
                                me.getSession().evict(newRowModel) ;
                            },
                            params: {
                                tableId: me.getViewModel().get('tableEntity').getId()
                            },
                            scope: me
                        })
                    ) ;
                }
            }
        ) ;

    },

    /**
     * Deletes the current selected row
     */
    onDeleteRowClick: function() {
        var me = this,
            tableGrid = me.lookupReference('tableGrid'),
            tableStore = tableGrid.getStore() ;
            selectedRow = me.lookupReference('tableGrid').selection ;

        Ext.Msg.confirm(_('The row will be deleted'), _('Are you sure you want to delete the row?'),
            function(response) {
                if (response === 'yes') {
                    selectedRow.erase(
                        GoraExplorer.applicationHelper.handleModelOperation({
                            successMessage: _('Row deleted successfully'),
                            failureMessage: _('Error deleting the row'),
                            failure: function(batch, operation) {
                                GoraExplorer.applicationHelper.handleFailedLoadResponse(operation, false) ;
                            },
                            params: {
                                tableId: me.getViewModel().get('tableEntity').getId()
                            },
                            scope: me
                        })
                    ) ;
                    var valuesRegion = me.lookupReference('valuesRegion') ;
                    valuesRegion.removeAll(true);
                }
            }
        ) ;
    },

    onLoadMoreRowsClick: function() {
        var me = this,
            tableGrid = me.lookupReference('tableGrid'),
            tableStore = tableGrid.getStore(),
            storeAtEnd = tableStore.atEnd ;

        if (storeAtEnd) {
            GoraExplorer.toastHelper.showInfoToast(_('End reached')) ;
        } else {
            tableStore.loadNextRows() ;
        }
    }

}) ;