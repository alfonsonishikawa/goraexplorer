/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * GoraStore is the store for a table. It hold 'GoraPersistent' Models (each one is a row in a table).
 * This store is used only to retrieve data from the server with the "scan" service.
 * No data should be pushed remotely from this store because being NoSQL lacks structure. Creating and updating
 * the entites must be done by hand with GoraPersistentDiff and GoraPersistent models.
 */
Ext.define('GoraExplorer.data.GoraStore', {
    extend: 'Ext.data.Store',

    alias: 'store.gorastore',
    uses: [
        'GoraExplorer.helpers.ApplicationHelper',
        'GoraExplorer.helpers.toast.ToastHelper'
    ],

    /**
     * @property {Boolean} Flags when the store contains all the data.
     * @readonly
     */
    atEnd: false,

    pageSize: 0,

    config: {
        /**
         * @cfg {String} [startKey] - Start key for the store data. Will be sent when loading more rows and filtering.
         */
        startKey: null,

        /**
         * @cfg {String} [endKey] - End key for the store data. Will be sent when loading more rows and filtering.
         */
        endKey: null,

        /**
         * @cfg {Number} [scanSize] - Ammount of data to request on each load. Min. value: 2.  Will be sent when loading more rows and filtering.
         */
        scanSize: 10
    },

    proxy: {
        type: 'rest',
        appendId: false,
        withCredentials: true,
        api: {
            read: GoraExplorer.applicationHelper.getRestUrl('tables/scan/${tableId}')
        }
    },

    listeners: {
        /**
         * When the data is clear, the 'at end' flag
         */
        clear: function() {
            this.atEnd = false ;
        }
    },

    model: 'GoraExplorer.model.GoraPersistent',

    /**
     * Verifies that the new scanSize is a positive number >= 2
     * @param {Number} newScanSize - Number of rows to retrieve on each load
     * @returns {Number}
     */
    applyScanSize: function(newScanSize) {
        if (newScanSize < 2) {
            return ;
        } else {
            return newScanSize;
        }
    },

    /**
     * Load the next rows starting from the start key configured or the last key found in the previous load.
     * @param options - Search options to override the default values
     * ```
     * {
     *   scanSize: {Number}, // Configured scan size in GoraStore from search filter
     *   endKey:   {String}, // Configured end key in GoraStore from search filter
     *   startKey: {String}  // The start key will be taken from the last element in the store
     * }
     * ```
     *
     * @returns {Ext.Promise} - onSuccess and onFailure returns
     * ```
     * {
     *     records: {GoraExplorer.model.GoraPersistent[]},
     *     operations: {Ext.data.operation.Operation}
     * }
     * ```
     * When the store is at the end, the records will be an empty array and operations will be null.
     */
    loadNextRows: function(options) {
        var me = this,
            deferred = new Ext.Deferred();

        if (me.atEnd === true) {
            // When at end, fake an empty result
            Ext.create('Ext.util.DelayedTask', function () {
                deferred.resolve({records: [], operations: null});
            }).delay(10);
            return deferred.promise;
        }

        var startKey = me.getStartKey();
        if (me.getCount() > 0) {
            startKey = me.data.last().get('__id__') ;
        }

        // Copy options into a new object so as not to mutate passed in objects
        options = Ext.apply({
            scanSize: me.getScanSize(),
            endKey: me.getEndKey(),
            startKey: startKey
        }, options);

        me.load({
            params: options,
            addRecords: true,
            scope: me,
            callback: function (records, operation, success) {
                // The viewConfig parameters preserveScrollOnReload and preserveScrollOnRefresh don't work
                // So we handle the preserver scroll by hand :(
                // For that, we return a Promise
                var me = this,
                    promiseResult = {records:records, operation:operation} ;

                if (success) {
                    //Sets the end flag when the load retrieves less than scanSize number of elements
                    if (records.length < this.getScanSize()) {
                        me.atEnd = true ;
                        Ext.defer(function() {
                            GoraExplorer.toastHelper.showInfoToast(_('End reached')) ;
                        },
                        20) ;
                    }
                    deferred.resolve(promiseResult) ;
                } else {
                    GoraExplorer.applicationHelper.handleFailedLoadResponse(operation, false, false) ;
                    deferred.reject(promiseResult)
                }
            }
        }) ;
        return deferred.promise;
    },

    /**
     * When removing all records from the store, the `atEnd` flag is cleared to allow new loads
     *
     * @param {Boolean} [silent=false] Pass `true` to prevent the `{@link #event-clear}` event from being fired.
     *
     * @return {Ext.data.Model[]} The removed records.
     */
    removeAll: function(silent) {
        var me = this ;
        me.atEnd = false ;
        me.callParent(arguments) ;
    }

}) ;