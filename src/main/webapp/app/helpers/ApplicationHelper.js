/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Helper class with methods for the global application
 */
Ext.define('GoraExplorer.helpers.ApplicationHelper', {
    singleton: true,

    uses: [
        'GoraExplorer.helpers.toast.ToastHelper',
        'GoraExplorer.Config'
    ],

    /**
     * Given a REST method, returns the URL to that method. URLs to REST methods must be passed throught this
     * method (without head slash)
     *
     * @param {String} restMethod - REST method with format '.../name' without head slash
     * @returns {String} - The full URL including the server if a server URL has been configured in app/Config.js
     */
    getRestUrl: function(restMethod) {
        return GoraExplorer.Config.APP_URL + restMethod;
    },

    /**
     * Given an operation with an error, returns the real response text, since ExtJS does not handles the
     * <code>success : false</code> when the HTTP answer is not a 2xx
     * @param {Ext.data.operation.Operation} operation
     * @return {Integer} return.status - The HTTP status of the operation
     * @return {String} return.message - The server error message
     * @return {String} return.url - The requested URL
     */
    _getErrorInfoFromOperation: function(operation) {
        var operationError = operation.getError(),
            status = operationError ? operationError.status : null,
            responseObject = Ext.JSON.decode(operationError.response.responseText,true),
            errorText = operationError ? operationError.statusText : '' ;

        if (status === 0) {
            errorText = _('Failed connecting') ;
        }
        if (responseObject != null) {
            errorText = responseObject.message ;
        }

        return {
            status: status,
            message: errorText,
            url: operation.getRequest().getUrl()
        }
    },

    /**
     * Handles the failed event after an async load. If success == true, it does nothing.
     *
     * @param {Ext.data.operation.Operation} operation - The failed operation
     * @param {Boolean} success - true if the response was sucessful, false if error
     * @param {Boolean} [silent=false] - If false, a MessageBox will be shown on error. If true, only log will be written.
     */
    handleFailedLoadResponse: function(operation, success, silent) {
        var me = this ;

        if (!success) {
            var message = null ;
            switch(operation.getError().status) {
                case 401: // Unauthorized
                    GoraExplorer.getApplication().redirectTo('login') ;
                    break ;
                case 403: // Forbidden
                    message = Ext.String.format('403 - Forbidden to access {0} {1}.<br/>History token: #{2}', operation.getRequest().getMethod(), operation.getRequest().getUrl(), Ext.History.getToken()) ;
                    Ext.log({level: 'warn', stack: true}, message) ;
                    if (!silent) {
                        Ext.MessageBox.show({
                            title: _('Access denied'),
                            message: message,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                    break;
                default:
                    message = Ext.String.format('Unexpected response status {0} when calling {2} {3}.<br/><br/> "{1}"<br/></br>History token: #{4}',
                        operation.getError().status, me._getErrorInfoFromOperation(operation).message,
                        operation.getRequest().getMethod(), operation.getRequest().getUrl(), Ext.History.getToken()) ;
                    Ext.log({level: 'warn', stack: true}, message) ;
                    if (!silent) {
                        Ext.MessageBox.show({
                            title: _('Unexpected response status'),
                            message: message,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                    break;
            }
        }
    },


    /**
     * Wraps all the sync response handling simplifying the toast messages when having queued operations.
     *
     * The main example is:
     *
     * ```
     * connectionsStore.sync(
     *     GoraExplorer.applicationHelper.handleStoreSync({
     *         params: { // If using URL placeholders
     *             param1: value1,
     *             param2: value2
     *         },
     *         successMessage: _('Connection created successfully'),
     *         failureMessage: _('Error creating the connection'),
     *         success: function(batch, options) {
     *             this.getView().close() ;
     *         },
     *         failure: function(batch, options) {
     *             this.getView().unmask() ;
     *         },
     *         afterCompletion: function(batch, options) {
     *             // stuff
     *         },
     *         scope: me
     *     })
     * ) ;
     * ```
     *
     * This method returns the proper configuration for `#sync()` that will show the following behaviour:
     *
     * * If the operation success will show the successMessage, and if there was more than one operation in the queue
     *   will inform about the number of operations.
     *
     * * If the operation fails, and it is the only operation, will show the failureMessage. If there were more operations
     *   with mixed results, will inform about the number of succeeding and failing operations.
     *
     * @param {Object} options Object containing one or more properties supported by the sync method (these get
     *      passed along to the underlying proxy's {@link Ext.data.Proxy#batch batch} method):
     *
     * @param {String} [options.successMessage] - Default message if the sync has only 1 operation and succeeds
     *
     * @param {String} [options.failureMessage] - Default message if the sync has only 1 operation and fails
     *
     * @param {Object} [options.params] - Object with parameters for URL placeholders
     *
     * @param {Function} [options.success] - The function to be called upon successful completion of the sync. The
     *      success function is called only if no exceptions were reported in any operations. If one or more exceptions
     *      occurred then the failure function will be called instead. The success function is called
     *      with the following parameters:
     * @param {Ext.data.Batch} options.success.batch - The {@link Ext.data.Batch batch} that was processed,
     *      containing all operations in their current state after processing
     * @param {Object} options.success.options - The options argument that was originally passed into sync
     *
     * @param {Function} [options.failure] - The function to be called upon unsuccessful completion of the sync. The
     *      failure function is called when one or more operations returns an exception during processing (even if some
     *      operations were also successful). In this case you can check the batch's {@link Ext.data.Batch#exceptions
     *      exceptions} array to see exactly which operations had exceptions. The failure function is called with the
     *      following parameters:
     * @param {Ext.data.Batch} options.failure.batch - The {@link Ext.data.Batch} that was processed, containing all
     *      operations in their current state after processing
     * @param {Object} options.failure.options - The options argument that was originally passed into sync
     *
     * @param {Function} [options.afterCompletion] - The function to be called after completion independently of
     *      success or failure. It is executed after succes/failure callbacks. The function is called with with the
     *      following parameters:
     * @param {Ext.data.Batch} options.afterCompletion.batch - The {@link Ext.data.Batch} that was processed, containing all
     *      operations in their current state after processing
     * @param {Object} options.afterCompletion.options - The options argument that was originally passed into sync
     *
     * @param {Object} [options.scope] - The scope in which to execute any callbacks (i.e. the `this` object inside
     *      the callback, success and/or failure functions). Defaults to the store's proxy.
     *
     * @return {{success: success, failure: failure, scope: scope}} to be used in {@link Ext.data.ProxyStore#sync}
     */
    handleStoreSync: function(options) {
        var me = this,
            successMessage = options.successMessage,
            failureMessage = options.failureMessage,
            successFunction = options.success,
            failureFunction = options.failure,
            afterCompletion = options.afterCompletion,
            scope = options.scope,
            params = options.params ;

        var syncOptions = {
            success: function (batch, options) {
                if (!Ext.isEmpty(successMessage)) {
                    GoraExplorer.toastHelper.showSuccessToast(successMessage);
                }

                var operations = batch.getOperations(),
                    numOperations = operations.length ;

                if (numOperations > 1) {
                    var toastMessage = Ext.String.format(_n('{0} queued operation succeeded', '{0} queued operations succeeded', numOperations), numOperations) ;
                    GoraExplorer.toastHelper.showSuccessToast(toastMessage, _('All the queued operations succeeded')) ;
                }
                if (!Ext.isEmpty(successFunction)) {
                    successFunction.call(this, batch, options);
                }
                if (!Ext.isEmpty(afterCompletion)) {
                    afterCompletion.call(this, batch, options) ;
                }
            },
            failure: function (batch, options) {
                var exceptions = batch.getExceptions(),
                    operations = batch.getOperations(),
                    numOperations = operations.length,
                    numExceptions = exceptions.length,
                    numSuccessOperations = numOperations - numExceptions;

                if (numExceptions === 1 && numOperations === 1) {
                    if (!Ext.isEmpty(failureMessage)) {
                        GoraExplorer.toastHelper.showErrorToast(failureMessage, _('Operation failed'));
                    }
                }
                else {
                    var toastMessage = null ;
                    if (numSuccessOperations > 0) {
                        toastMessage = Ext.String.format(_n('{0} queued operation succeeded', '{0} queued operations succeeded', numSuccessOperations), numSuccessOperations) ;
                        GoraExplorer.toastHelper.showSuccessToast(toastMessage)
                    }
                    toastMessage = Ext.String.format(_n('{0} queued operation failed', '{0} queued operations failed', numExceptions), numExceptions) ;
                    GoraExplorer.toastHelper.showErrorToast(toastMessage, _('Queued operations failed')) ;
                    exceptions.forEach(function (operation) {
                        var errorInfo = GoraExplorer.applicationHelper._getErrorInfoFromOperation(operation);
                        Ext.log({level: 'error', stack: true}, errorInfo.url + ' - ' + errorInfo.message);
                    }, this);
                }
                if (!Ext.isEmpty(failureFunction)) {
                    failureFunction.call(this, batch, options);
                }
                if (!Ext.isEmpty(afterCompletion)) {
                    afterCompletion.call(this, batch, options) ;
                }
            }
        } ;

        if (!Ext.isEmpty(scope)) {
            syncOptions.scope = scope;
        }

        if (!Ext.isEmpty(params)) {
            syncOptions.params = params ;
        }

        return syncOptions ;
    },

    /**
     * When loading a model with {@link Ext.data.Model.load}, handles wraps all the response handling simplifying the
     * toast messages when having queued operations.
     *
     * The main example is:
     *
     * ```
     * aModelInstance.load(
     *     GoraExplorer.applicationHelper.handleModelOperation({
     *         params: { // If using URL placeholders
     *             param1: value1,
     *             param2: value2
     *         },
     *         successMessage: _('Model loaded successfully'),
     *         failureMessage: _('Error loading the model'),
     *         success: function(batch, options) {
     *         },
     *         failure: function(batch, options) {
     *         },
     *         afterCompletion: function(batch, options) {
     *         },
     *         scope: me
     *     })
     * ) ;
     *
     * aModelInstance.save(
     *     GoraExplorer.applicationHelper.handleModelOperation( ... )
     * ) ;
     *
     * aModelInstance.erase(
     *     GoraExplorer.applicationHelper.handleModelOperation( ... )
     * ) ;
     * ```
     *
     * * If the operation success will show the successMessage
     * * If the operation fails will show the failureMessage.
     *
     * @param {Object} options Object with the options
     *
     * @param {String} [options.successMessage] - Default message if the operation success
     *
     * @param {String} [options.failureMessage] - Default message if the operation fails
     *
     * @param {Object} [options.params] - Object with parameters for URL placeholders
     *
     * @param {Function} [options.success] - The function to be called upon successful completion of the load/save/erase.
     *      The success function is called with the following parameters:
     * @param {Ext.data.Model} options.success.record - The record saved/loaded/erased
     * @param {Ext.data.operation.Operation} options.success.operation - The operation save/load/erase
     *
     * @param {Function} [options.failure] - The function to be called upon failed completion of the load/save/erase.
     *      The failure function is called with the following parameters:
     * @param {Ext.data.Model} options.success.record - The record saved/loaded/erased
     * @param {Ext.data.operation.Operation} options.success.operation - The operation save/load/erase
     *
     * @param {Function} [options.afterCompletion] - The function to be called after completion independently of
     *      success or failure. It is executed after succes/failure callbacks. The function is called with with the
     *      following parameters:
     * @param {Ext.data.Model} options.afterCompletion.record - The record saved/loaded/erased
     * @param {Ext.data.operation.Operation} options.afterCompletion.operation - The operation save/load/erase
     *
     * @param {Object} [options.scope] - The scope in which to execute any callbacks (i.e. the `this` object inside
     *      the callback, success and/or failure functions). Defaults to the models proxy
     *
     * @return {{success: success, failure: failure, scope: scope}} to be used in {@link Ext.data.Model#save}, {@link Ext.data.Model#load}
     *      or {@link Ext.data.Model#erase}
     */
    handleModelOperation: function(options) {
        var me = this,
            successMessage = options.successMessage,
            failureMessage = options.failureMessage,
            successFunction = options.success,
            failureFunction = options.failure,
            afterCompletion = options.afterCompletion,
            scope = options.scope,
            params = options.params ;

        var syncOptions = {
            success: function (record, operation) {
                if (Ext.isDefined(successMessage)) {
                    GoraExplorer.toastHelper.showSuccessToast(successMessage) ;
                }

                if (!Ext.isEmpty(successFunction)) {
                    successFunction.call(this, record, operation);
                }
                if (!Ext.isEmpty(afterCompletion)) {
                    afterCompletion.call(this, record, operation) ;
                }
            },
            failure: function (record, operation) {
                if (Ext.isDefined(failureMessage)) {
                    GoraExplorer.toastHelper.showErrorToast(failureMessage, _('Operation failed')) ;
                }

                GoraExplorer.applicationHelper.handleFailedLoadResponse(operation, false) ;

                if (!Ext.isEmpty(failureFunction)) {
                    failureFunction.call(this, record, operation);
                }
                if (!Ext.isEmpty(afterCompletion)) {
                    afterCompletion.call(this, record, operation) ;
                }
            }
        } ;

        if (!Ext.isEmpty(scope)) {
            syncOptions.scope = scope;
        }

        if (!Ext.isEmpty(params)) {
            syncOptions.params = params ;
        }

        return syncOptions ;
    }

},
function (ApplicationHelper){
    GoraExplorer.applicationHelper = ApplicationHelper ;
}) ;