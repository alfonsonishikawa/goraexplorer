/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.settings.Language', {
    extend: 'Ext.button.Split',
    alias: 'widget.language',

    requires: [
        'GoraExplorer.view.settings.LanguageController'
    ],

    controller: 'language',

    width: 130,

    menu: {
        xtype: 'menu',
        defaults: {
            listeners: {
                click: 'onMenuItemClick'
            }
        },
        items: [
            {
                xtype: 'menuitem',
                iconCls: 'en',
                text: 'English'
            },
            {
                xtype: 'menuitem',
                iconCls: 'es',
                text: 'Español'
            },
            {
                xtype: 'menuitem',
                iconCls: 'gl',
                text: 'Galego'
            },
            {
                xtype: 'menuitem',
                iconCls: 'eo',
                text: 'Esperanto'
            }
        ]
    }
}) ;