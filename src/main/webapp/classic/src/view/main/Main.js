/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Main view a viewport
 */
Ext.define('GoraExplorer.view.main.Main', {
    extend: 'Ext.container.Viewport',
    xtype: 'app-main',

    requires: [
        'Ext.window.MessageBox',
        'Ext.list.Tree',
        'Ext.Img',

        'GoraExplorer.view.main.MainController',
        'GoraExplorer.view.main.MainModel',
        'GoraExplorer.view.main.MainContainerWrap',
        'GoraExplorer.store.NavigationTree'
    ],

    controller: 'main',
    viewModel: { type : 'main' } ,

    cls: 'sencha-dash-viewport',
    itemId: 'mainView',

    session: true,

    listeners: {
        afterlayout: 'onAfterLayout'
    },

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'toolbar',
            cls: 'sencha-dash-dash-headerbar shadow',
            height: 64,
            itemId: 'headerBar',
            items: [
                {
                    xtype: 'component',
                    reference: 'logo',
                    cls: 'logo',
                    html: '<div class="main-logo"><img src="resources/images/goraexplorer-logo.svg">GoraExplorer</div>',
                    width: 250
                },
                {
                    margin: '0 0 0 8',
                    ui: 'header',
                    iconCls: 'x-fa fa-navicon',
                    id: 'main-navigation-btn',
                    handler: 'onToggleNavigationSize'
                },
                '->',
                {
                    xtype: 'tbtext',
                    cls: 'top-user-name',
                    bind: '{currentUser.username}'
                },
                {
                    xtype: 'image',
                    cls: 'header-right-profile-image',
                    reference: 'currentUserToolbarImage',
                    height: 35,
                    width: 35,
                    alt: _('current user image'),
                    bind: {
                        src: '{currentUser.imageUrl}'
                    },
                    listeners: {
                        click: {
                            element: 'el',
                            fn: 'profileImageClick'
                        }
                    }
                }
            ]
        },
        {
            xtype: 'maincontainerwrap',
            id: 'main-view-detail-wrap',
            reference: 'mainContainerWrap',
            minHeight: 350,
            flex: 1,
            items: [
                {
                    xtype: 'treelist',
                    reference: 'navigationTreeList',
                    itemId: 'navigationTreeList',
                    ui: 'navigation',
                    store: 'NavigationTree',
                    width: 250,
                    minHeight: 350,
                    expanderFirst: false,
                    expanderOnly: false,
                    listeners: {
                        selectionchange: 'onNavigationTreeSelectionChange',
                        beforerender: function () {
                            // Workaround to bug on treelist and binding a store:
                            // https://www.sencha.com/forum/showthread.php?309606-Treelist-Store-not-loaded
                            this.setBind({store: '{navigationTree}'});
                        }
                    }
                },
                {
                    xtype: 'container',
                    flex: 1,
                    minHeight: 350,
                    reference: 'mainCardPanel',
                    cls: 'sencha-dash-right-main-container',
                    itemId: 'contentPanel',
                    layout: {
                        type: 'card'
                    }
                }
            ]
        }
    ]
});
