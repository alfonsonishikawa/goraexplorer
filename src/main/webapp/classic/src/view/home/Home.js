/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.home.Home', {
    extend: 'Ext.Container',

    alias: 'widget.home',

    uses: [
        'GoraExplorer.view.home.Version',
        'GoraExplorer.view.home.Welcome'
    ],
    componentCls: 'home-body',
    
    items: [
        {
            xtype: 'welcome',
            cls: 'shadow'
        },
        {
            xtype: 'version',
            cls: 'shadow'
        }
    ]
});

