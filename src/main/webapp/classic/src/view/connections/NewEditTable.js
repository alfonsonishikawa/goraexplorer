/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Window used both to create a new table, and for edit an existing table.
 *
 * When creating the windows, must be configured with the parameters:
 *
 * ```
 * {
 *     isEdit: {Boolean}, //default false
 *     parentConnection: {GoraExplorer.model.Connection}, // The connection to which the table belongs
 *     [, editTableEntity: {GoraExplorer.model.Table}]
 * }
 * ```
 *
 * <ul>
 *     <li><code>isEdit: true</code> when the window will be used to edit an existing table. In this case the
 *          parameter <code>editTableEntity</code> must hold the table instance to be edited.</li>
 *     <li><code>isEdit: false</code> when the window will be used to create a new table.</li>
 * </ul>
 *
 * This widget class internally self-configures a viewModel and <code>session: true</code>, so the updates
 * are done in a new session.
 *
 */
Ext.define('GoraExplorer.view.connections.NewEditTable', {
    extend: 'Ext.window.Window',
    alias: 'widget.new-edit-table',

    requires: [
        'GoraExplorer.view.connections.NewEditTableController',
        'GoraExplorer.form.field.AppendFile'
    ],

    controller: 'new-edit-table',

    session: true,

    /**
     * @cfg {Boolean} [isEdit=false] - Sets the component to be a "new table" or "edit table".
     *                         If the value is <code>true</code>, the component will be configured to edit an existing
     *                         table (see {@link editTable}. If the value is <code>false</code> the component will be configured
     *                         to create a new table.
     */
    /**
     * @readonly
     * @cfg {GoraExplorer.model.Table} editTableEntity - When <code>isEdit === true</code>, the table model to be edited
     *                                        must be configured here.
     */
    /**
     * @readonly
     * @cfg {GoraExplorer.model.Connection} parentConnection - The connection to which this table belongs
     */

    /**
     * Updates the configuration with the proper viewModel if does not have a viewModel.
     * This viewModel will hold the "isEdit" state  and the table entity being created/updated.
     * @param {Object} [config] Config object.
     */
    constructor: function(config) {
        var me = this,
            defaultViewModel = null;

        if (config.isEdit) {
            // Edit table
            if (Ext.isEmpty(config.editTableEntity)) {
                Ext.raise({msg: _('Missing configuration parameter editTableEntity when creating the widget NewEditTable for editing')}) ;
            }
            defaultViewModel = {
                data: {
                    isEdit: true
                },
                links: {
                    table: config.editTableEntity
                }
            } ;
        } else {
            // New table
            if (Ext.isEmpty(config.parentConnection)) {
                Ext.raise({msg: _('Missing configuration parameter parentConnection when creating the widget NewEditTable for a new Connection')}) ;
            }
            defaultViewModel = {
                data: {
                    isEdit: false
                },
                links: {
                    table: {
                        type: 'GoraExplorer.model.Table',
                        create: true
                    }
                }
            } ;
        }
        // By default the viewModel created here will be used, but can be overriden by the user
        var updatedConfig = Ext.merge(
            {
                viewModel: defaultViewModel
            },
            config
        );
        me.callParent([updatedConfig]);
    },

    bind: {
        title: '{isEdit:pick("' + _('New table') + '","' + _('Edit table') + '")}'
    },
    modal: true,
    maximizable: true,
    minWidth: 400,
    minHeight: 440,
    bodyPadding: 10,
    scrollable: true,
    layout: 'fit',

    buttons: [
        {
            name: 'createUpdate',
            bind: {
                text: '{isEdit:pick("' + _('Create') + '","' + _('Update') + '")}'
            },
            handler: 'onSaveClick'
        },
        {
            name: 'cancel',
            text: _('Cancel'),
            handler: 'onCancelClick'
        }
    ],

    defaults: {
        labelWidth: 150,
        modelValidation: true
    },
    items: [
        {
            xtype: 'form',
            reference: 'tableForm',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: _('Name'),
                    bind: '{table.name}',
                    maxWidth: 550
                },
                {
                    xtype: 'textfield',
                    name: 'description',
                    fieldLabel: _('Description'),
                    bind: '{table.description}'
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    padding: '0 0 10 0', // Separate from the component bellow
                    items: [
                        {
                            xtype: 'textarea',
                            name: 'avroSchema',
                            fieldLabel: _('Avro schema'),
                            bind: '{table.avroSchema}',
                            reference: 'avroSchema',
                            flex: 1
                        },
                        {
                            xtype: 'appendfilefield',
                            name: 'avroSchemaAppendFile',
                            appendTo: 'avroSchema' // reference name to the textarea to append the content of the file
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    padding: '0 0 10 0', // Separate from the component bellow
                    items: [
                        {
                            xtype: 'textarea',
                            name: 'mapping',
                            fieldLabel: _('Mapping'),
                            bind: '{table.mapping}',
                            reference: 'mapping',
                            flex: 1
                        },
                        {
                            xtype: 'appendfilefield',
                            name: 'hadoopConfigurationAppendFile',
                            appendTo: 'mapping' // reference name to the textarea to append the content of the file
                        }
                    ]
                },
                {
                    xtype: 'combobox',
                    name: 'keyClass',
                    fieldLabel: _('Key class'),
                    bind: '{table.keyClass}',
                    forceSelection: true,
                    editable: false,
                    autoSelect: true,
                    queryMode: 'local',
                    store: [ 'java.lang.String', 'java.lang.Integer','java.lang.Long' ],
                    readOnly: true
                }
            ]
        }
    ]

});