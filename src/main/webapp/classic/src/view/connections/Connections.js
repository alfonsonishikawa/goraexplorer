/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.Connections', {
    extend: 'Ext.panel.Panel',

    requires: [
        'GoraExplorer.view.connections.ConnectionsController',
        'GoraExplorer.view.connections.ConnectionsGrid',
        'GoraExplorer.view.connections.TablesGrid',
        'GoraExplorer.view.connections.ConnectionsModel',
        'Ext.container.ButtonGroup'
    ],

    uses: [],

    controller: 'connections',
    viewModel: 'connections',

    alias: 'widget.connections',

    minHeight: 380,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    dockedItems:[{
        xtype : 'toolbar',
        dock: 'top',
        items:[
            {
                xtype: 'buttongroup',
                columns: 2,
                items: [
                    {
                        text: _('Open table'),
                        iconCls: 'fa fa-folder-open-o',
                        rowspan: 2,
                        iconAlign: 'top',
                        scale: 'large',
                        bind: {
                            disabled: '{!tablesGrid.selection}'
                        },
                        handler: 'onOpenTableAction'
                    },
                    {
                        text: _('Connection'),
                        iconCls: 'fa fa-plug',
                        menu: {
                            xtype: 'menu',
                            items: [
                                {
                                    text: _x('Connection','New'),
                                    iconCls: 'x-fa fa-plus-square',
                                    handler: 'onNewConnectionClick'
                                },
                                {
                                    text: _x('Connection','Edit'),
                                    iconCls: 'x-fa fa-pencil',
                                    handler: 'onEditConnectionClick',
                                    bind: {
                                        disabled: '{!connectionsGrid.selection}'
                                    }
                                },
                                {
                                    text: _x('Connection','Delete'),
                                    iconCls: 'x-fa fa-trash',
                                    handler: 'onDeleteConnectionClick',
                                    bind: {
                                        disabled: '{!connectionsGrid.selection}'
                                    }
                                }
                            ]
                        }
                    },
                    {
                        text: _('Table'),
                        iconCls: 'fa fa-table',
                        bind: {
                            disabled: '{!connectionsGrid.selection}'
                        },
                        menu: {
                            xtype: 'menu',
                            items: [
                                {
                                    text: _('Table','New'),
                                    iconCls: 'x-fa fa-plus-square',
                                    handler: 'onNewTableClick',
                                    bind: {
                                        disabled: '{!connectionsGrid.selection}'
                                    }
                                },
                                {
                                    text: _('Table','New wizard'),
                                    iconCls: 'x-fa fa-magic',
                                    handler: 'onNewWizardTableClick',
                                    bind: {
                                        disabled: '{!connectionsGrid.selection}'
                                    }
                                },
                                {
                                    text: _('Table','Edit'),
                                    iconCls: 'x-fa fa-pencil',
                                    handler: 'onEditTableClick',
                                    bind: {
                                        disabled: '{!tablesGrid.selection}'
                                    }
                                },
                                {
                                    text: _('Table','Edit wizard'),
                                    iconCls: 'x-fa fa-magic',
                                    handler: 'onEditWizardTableClick',
                                    bind: {
                                        disabled: '{!tablesGrid.selection}'
                                    }
                                },
                                {
                                    text: _('Table','Delete'),
                                    iconCls: 'x-fa fa-trash',
                                    handler: 'onDeleteTableClick',
                                    bind: {
                                        disabled: '{!tablesGrid.selection}'
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        ]
    }],

    items: [
        {
            xtype: 'connectionsgrid',
            reference: 'connectionsGrid',
            minHeight: 150,
            listeners: {
                selectionchange: 'onConnectionsGridSelectionChange'
            },
            flex: 1
        },
        {
            xtype: 'tablesgrid',
            reference: 'tablesGrid',
            minHeight: 150,
            flex: 1
        }
    ]
});