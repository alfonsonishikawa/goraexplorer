/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.NewTableReview', {
    extend: 'Ext.form.Panel',

    alias: 'widget.wizard-newtable-review',

    requires: [
        'GoraExplorer.view.connections.wizard.NewTableReviewController',
        'GoraExplorer.view.connections.wizard.NewTableReviewModel'
    ],

    controller: 'wizard-newtable-review',
    viewModel: 'wizard-newtable-review',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'panel',
            title: _('Schema'),
            layout: 'fit',
            margin: '0 10 0 0',
            items: [
                {
                    xtype: 'textarea',
                    name: 'schemaTextArea',
                    bind: '{table.avroSchema}'
                }
            ],
            flex: 1
        },
        {
            xtype: 'panel',
            title: _('Mapping'),
            layout: 'fit',
            items: [
                {
                    xtype: 'textarea',
                    name: 'mappingTextArea',
                    bind: '{table.mapping}'
                }
            ],
            flex: 1
        }
    ]

}) ;