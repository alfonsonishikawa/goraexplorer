/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.NewTableSchema', {
    extend: 'Ext.form.Panel',

    alias: 'widget.wizard-newtable-schema',

    requires: [
        'GoraExplorer.view.connections.wizard.NewTableSchemaController',
        'GoraExplorer.view.connections.wizard.NewTableSchemaModel',
        'Ext.tree.Panel'
    ],

    controller: 'wizard-newtable-schema',
    viewModel: 'wizard-newtable-schema',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    defaults: {
        modelValidation: true
    },
    items: [
        {
            xtype: 'treepanel',
            title: _('Entities'),
            width: 300,
            scrollable: true,
            margin: '0 10 0 0',
            iconCls: 'x-fa fa-address-book-o',
            reference: 'entitiesTreePanel',
            useArrows: true,
            rootVisible: false,
            displayField: 'name',
            bind: '{avroSchemaStore}',
            viewConfig: {
                markDirty: false
            },
            tools: [
                {
                    type: 'plus',
                    tooltip: _('Add Entity'),
                    handler: 'onAddEntity'
                },
                {
                    type: 'minus',
                    text: _('Remove Entity'),
                    handler: 'onRemoveEntity',
                    bind: {
                        disabled: '{!entitiesTreePanel.selection}'
                    }
                }
            ]
        },
        {
            xtype: 'panel',
            title: _('Field/Entity'),
            bodyPadding: 10,
            scrollable: true,
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            dockedItems:[
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            text: _('Add Field'),
                            iconCls: 'x-fa fa-plus',
                            bind: {
                                disabled: '{!entitiesTreePanel.selection}'
                            },
                            menu: [
                                {
                                    text: _('Boolean'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Boolean'
                                        }
                                    }
                                },
                                {
                                    text: _('Int'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Int'
                                        }
                                    }
                                },
                                {
                                    text: _('Long'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Long'
                                        }
                                    }
                                },
                                {
                                    text: _('Float'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Float'
                                        }
                                    }
                                },
                                {
                                    text: _('Double'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Double'
                                        }
                                    }
                                },
                                {
                                    text: _('String'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'String'
                                        }
                                    }
                                },
                                {
                                    text: _('Bytes'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Bytes'
                                        }
                                    }
                                },
                                {
                                    text: _('Record'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Record'
                                        }
                                    }
                                },
                                {
                                    text: _('Array'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Array'
                                        }
                                    }
                                },
                                {
                                    text: _('Map'),
                                    listeners: {
                                        click: {
                                            fn: 'onAddFieldClick',
                                            fieldType: 'Map'
                                        }
                                    }
                                },
                                '-',
                                {
                                    text: _('Delete Field'),
                                    iconCls: 'x-fa fa-minus',
                                    handler: 'onDeleteFieldClick'
                                }
                            ]
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: _('Name'),
                    bind: '{entitiesTreePanel.selection.name}',
                    allowBlank: false
                },
                {
                    xtype: 'combobox',
                    name: 'type',
                    fieldLabel: _('Type'),
                    bind: '{entitiesTreePanel.selection.type}',
                    forceSelection: true,
                    editable: false,
                    readOnly: true,
                    autoSelect: true,
                    queryMode: 'local',
                    store: ['boolean', 'int', 'long', 'float', 'double', 'string', 'bytes', 'record', 'array', 'map', 'entity'],
                    maxWidth: 250
                },
                {
                    xtype: 'textarea',
                    name: 'documentation',
                    fieldLabel: _('Documentation'),
                    resizable: true,
                    resizeHandles: 's',
                    cls: 'textarea-resizable-all-width',
                    bind: '{entitiesTreePanel.selection.doc}'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'checkbox',
                            name: 'allowNull',
                            fieldLabel: _('Allow Null field'),
                            bind: {
                                value: '{entitiesTreePanel.selection.nullable}',
                                hidden: '{!isField}'
                            }
                        },
                        {
                            xtype: 'checkbox',
                            name: 'defaultNullValue',
                            margin: '0 0 0 10',
                            fieldLabel: _('Set Null as default field value'),
                            labelWidth: 190,
                            bind: {
                                value: '{entitiesTreePanel.selection.defaultNullValue}',
                                disabled: '{!entitiesTreePanel.selection.nullable}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'combobox',
                    name: 'defaultBooleanValue',
                    fieldLabel: _('Default value'),
                    forceSelection: true,
                    editable: false,
                    autoSelect: true,
                    queryMode: 'local',
                    store: ['true', 'false'],
                    maxWidth: 250,
                    hidden: true,
                    bind: {
                        value: '{entitiesTreePanel.selection.defaultBooleanValue}',
                        hidden: '{!isBooleanField}',
                        disabled: '{disableDefaultValueInput}'
                    }
                },
                {
                    xtype: 'numberfield',
                    name: 'defaultIntValue',
                    fieldLabel: _('Default value'),
                    hidden: true,
                    maxWidth: 250,
                    allowDecimals: false,
                    maxValue: 2147483647,
                    minValue: -2147483648,
                    bind: {
                        value: '{entitiesTreePanel.selection.defaultIntValue}',
                        hidden: '{!isIntField}',
                        disabled: '{disableDefaultValueInput}'
                    }
                },
                {
                    xtype: 'numberfield',
                    name: 'defaultLongValue',
                    fieldLabel: _('Default value'),
                    hidden: true,
                    maxWidth: 350,
                    allowDecimals: false,
                    maxValue: 9223372036854775807,
                    minValue: -9223372036854775808,
                    bind: {
                        value: '{entitiesTreePanel.selection.defaultLongValue}',
                        hidden: '{!isLongField}',
                        disabled: '{disableDefaultValueInput}'
                    }
                },
                {
                    xtype: 'numberfield',
                    name: 'defaultFloatValue',
                    fieldLabel: _('Default value'),
                    hidden: true,
                    maxWidth: 350,
                    maxValue: 3.4028234663852886E38,
                    minValue: -3.4028234663852886E38,
                    bind: {
                        value: '{entitiesTreePanel.selection.defaultFloatValue}',
                        hidden: '{!isFloatField}',
                        disabled: '{disableDefaultValueInput}'
                    }
                },
                {
                    xtype: 'numberfield',
                    name: 'defaultDoubleValue',
                    fieldLabel: _('Default value'),
                    hidden: true,
                    maxWidth: 350,
                    bind: {
                        value: '{entitiesTreePanel.selection.defaultDoubleValue}',
                        hidden: '{!isDoubleField}',
                        disabled: '{disableDefaultValueInput}'
                    }
                },
                {
                    xtype: 'textfield',
                    name: 'defaultStringValue',
                    fieldLabel: _('Default value'),
                    hidden: true,
                    bind: {
                        value: '{entitiesTreePanel.selection.defaultStringValue}',
                        hidden: '{!isStringField}',
                        disabled: '{disableDefaultValueInput}'
                    }
                },
                {
                    // ARRAYS
                    xtype: 'fieldset',
                    title: _('Array Items'),
                    hidden: true,
                    bind: {
                        hidden: '{!isArrayField}'
                    },
                    items: [
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'combobox',
                                    name: 'itemsType',
                                    fieldLabel: _('Type'),
                                    bind: '{entitiesTreePanel.selection.itemsType}',
                                    forceSelection: true,
                                    editable: false,
                                    autoSelect: true,
                                    queryMode: 'local',
                                    store: ['boolean', 'int', 'long', 'float', 'double', 'string', 'bytes', 'record'],
                                    maxWidth: 250
                                },
                                {
                                    xtype: 'checkbox',
                                    name: 'allowNullItems',
                                    boxLabel: _('Allow null items'),
                                    padding: '0 0 0 5',
                                    bind: '{entitiesTreePanel.selection.itemsNullable}'
                                }
                            ]
                        },
                        {
                            xtype: 'combobox',
                            fieldLabel: _('Record name'),
                            name: 'itemsRecordName',
                            editable: false,
                            forceSelection: true,
                            autoSelect: true,
                            displayField: 'name',
                            queryMode: 'local',
                            bind: {
                                value: '{entitiesTreePanel.selection.itemsRecordName}',
                                store: '{definedEntitiesStore}',
                                disabled: '{!itemsTypeIsRecord}'
                            }
                        }
                    ]
                },
                {
                    // MAPS
                    xtype: 'fieldset',
                    title: _('Map Values'),
                    hidden: true,
                    bind: {
                        hidden: '{!isMapField}'
                    },
                    items: [
                        {
                            xtype: 'fieldcontainer',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'combobox',
                                    name: 'valuesType',
                                    fieldLabel: _('Type'),
                                    bind: '{entitiesTreePanel.selection.valuesType}',
                                    forceSelection: true,
                                    editable: false,
                                    autoSelect: true,
                                    queryMode: 'local',
                                    store: ['boolean', 'int', 'long', 'float', 'double', 'string', 'bytes', 'record'],
                                    maxWidth: 250
                                },
                                {
                                    xtype: 'checkbox',
                                    name: 'allowNullValues',
                                    boxLabel: _('Allow null values'),
                                    padding: '0 0 0 5',
                                    bind: '{entitiesTreePanel.selection.valuesNullable}'
                                }
                            ]
                        },
                        {
                            xtype: 'combobox',
                            fieldLabel: _('Record name'),
                            name: 'valuesRecordName',
                            editable: false,
                            forceSelection: true,
                            autoSelect: true,
                            displayField: 'name',
                            queryMode: 'local',
                            bind: {
                                value: '{entitiesTreePanel.selection.valuesRecordName}',
                                store: '{definedEntitiesStore}',
                                disabled: '{!valuesTypeIsRecord}'
                            }
                        }
                    ]
                },
                {
                    // RECORD
                    xtype: 'fieldset',
                    title: _('Record Values'),
                    hidden: true,
                    bind: {
                        hidden: '{!isRecordField}'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            fieldLabel: _('Record name'),
                            name: 'recordFieldTypeName',
                            editable: false,
                            forceSelection: true,
                            autoSelect: true,
                            displayField: 'name',
                            queryMode: 'local',
                            bind: {
                                value: '{entitiesTreePanel.selection.recordFieldTypeName}',
                                store: '{definedEntitiesStore}'
                            }
                        }
                    ]
                }
            ]
        }
    ]

}) ;