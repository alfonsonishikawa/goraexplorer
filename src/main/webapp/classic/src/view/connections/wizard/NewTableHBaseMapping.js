/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.NewTableHBaseMapping', {
    extend: 'Ext.form.Panel',

    alias: 'widget.wizard-newtable-hbase-mapping',

    requires: [
        'GoraExplorer.view.connections.wizard.NewTableHBaseMappingController',
        'GoraExplorer.view.connections.wizard.NewTableHBaseMappingModel'
    ],

    controller: 'wizard-newtable-hbase-mapping',
    viewModel: 'wizard-newtable-hbase-mapping',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'treepanel',
            title: _('Entities'),
            width: 300,
            minHeight: 460,
            margin: '0 10 0 0',
            iconCls: 'x-fa fa-address-book-o',
            reference: 'hbaseeEntitiesTreePanel',
            useArrows: true,
            rootVisible: false,
            displayField: 'name',
            bind: '{avroSchemaStore}',
            viewConfig: {
                markDirty: false
            }
        },
        {
            xtype: 'panel',
            title: _('Mapping'),
            bodyPadding: 10,
            scrollable: true,
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'combobox',
                    fieldLabel: _('Family'),
                    name: 'family',
                    queryMode: 'local',
                    allowBlank: false,
                    disabled: true,
                    bind: {
                        value: '{hbaseeEntitiesTreePanel.selection.hbaseFamily}',
                        store: '{columnFamiliesStore}',
                        disabled: '{selectedIsMainEntity}'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: _('Column'),
                    name: 'column',
                    allowBlank: false,
                    disabled: true,
                    bind: {
                        value: '{hbaseeEntitiesTreePanel.selection.hbaseColumn}',
                        disabled: '{hbaseColumnDisabled}'
                    }
                }
            ]
        }
    ]

}) ;