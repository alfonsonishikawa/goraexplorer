/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.NewTableMainInfo', {
    extend: 'Ext.form.Panel',

    alias: 'widget.wizard-newtable-maininfo',

    modelValidation: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'textfield',
            fieldLabel: _('Name'),
            bind: '{table.name}',
            name: 'name',
            maxWidth: 550
        },
        {
            xtype: 'textfield',
            fieldLabel: _('Description'),
            name: 'description',
            bind: '{table.description}'
        },
        {
            xtype: 'combobox',
            name: 'keyClass',
            fieldLabel: _('Key class'),
            bind: '{table.keyClass}',
            forceSelection: true,
            editable: false,
            autoSelect: true,
            queryMode: 'local',
            store: [ 'java.lang.String', 'java.lang.Integer','java.lang.Long' ],
            readOnly: true,
            maxWidth: 550
        },
        {
            xtype: 'combobox',
            fieldLabel: _('Native table'),
            name: 'nativeTable',
            editable: true,
            queryMode: 'local',
            allowBlank: false,
            bind: {
                store: '{nativeTablesNameStore}',
                value: '{nativeTableName}'
            }
        }
    ]
}) ;