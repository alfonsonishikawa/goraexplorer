/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Tables grid can't have own ViewModel because some type of grid-grid stores strange bug.
 *
 */
Ext.define('GoraExplorer.view.connections.TablesGrid', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.tablesgrid',

    title: _('Tables'),

    bind: {
        store: '{tablesGridStore}'
    },

    columns: [
        { text: _('Name'), dataIndex: 'name', width: 150 },
        { text: _('Description'), dataIndex: 'description', flex: 1 }
    ]
});