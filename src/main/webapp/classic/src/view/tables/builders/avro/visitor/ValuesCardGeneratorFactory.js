/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Factory that returns a ValuesCardGeneratorVisitor for a Schema Entity Tree
 */
Ext.define('GoraExplorer.view.tables.builders.avro.visitor.ValuesCardGeneratorFactory', {
    singleton: true,

    uses: [
        'GoraExplorer.view.tables.builders.avro.visitor.ValuesCardGeneratorVisitor'
    ],

    _cache: new WeakMap(),

    getGeneratorVisitor: function(schemaEntityTree) {
        var me = this ;

        if ( ! me._cache.has(schemaEntityTree)) {
            var newVisitor = Ext.create('GoraExplorer.view.tables.builders.avro.visitor.ValuesCardGeneratorVisitor', schemaEntityTree) ;
            me._cache.set(schemaEntityTree, newVisitor) ;
        }

        return me._cache.get(schemaEntityTree) ;
    },

    /**
     * Invalidates the cache
     */
    invalidateCache: function() {
        var me = this ;
        me._cache.clear() ;
    }


}) ;