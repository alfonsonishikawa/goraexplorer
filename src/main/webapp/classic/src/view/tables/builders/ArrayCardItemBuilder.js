/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Generates the item config for an array field
 */
Ext.define('GoraExplorer.view.tables.builders.ArrayCardItemBuilder', {
    singleton: true,

    uses: [
        'GoraExplorer.view.tables.builders.ItemBuilderHelper',
        'GoraExplorer.view.tables.cards.ShowChildCardButton'
    ],

    /**
     * Generates the Values Card Item from an ArrayNode (of an avro schema tree node)
     *
     * @param {GoraExplorer.model.avro.ArrayNode} node - The array node to generate its item definition
     * @param {GoraExplorer.model.avro.EntitiesNode} tableSchemaEntitiesTree - The root of the entities schema tree
     * @param {String} currentBindPath
     * @return {Object} Item definition
     */
    generateItemDefinition: function(node, tableSchemaEntitiesTree, currentBindPath) {
        var me = this,
            nodeName = node.get('name'),
            nodeNullable = node.get('nullable'),
            nodeDoc = node.get('doc') ,
            nodeItemsType = node.get('itemsType'),
            nodeItemsRecordName = node.get('itemsRecordName') ;

        if (nodeItemsType === 'record') {
            nodeItemsType = nodeItemsRecordName ;
        }

        var itemDefinition = {
            xtype : 'carditemcontainer',
            items: [
                {
                    xtype: 'valuescarditemlabel',
                    text: Ext.String.format ('{0} (array:{1}):', nodeName, nodeItemsType),
                    tooltip: nodeDoc
                },
                {
                    xtype: 'box',
                    flex: 1
                },
                {
                    xtype: 'show-childcard-button',
                    schemaNode: node,
                    bindPath: currentBindPath
                }
            ]
        };

        // Add null stuff
        Ext.Array.insert(
            itemDefinition.items,
            itemDefinition.items.length,
            GoraExplorer.itemBuilderHelper.createNullItemsDefinition(currentBindPath, node, nodeNullable, ['add', 'delete', 'null'])
        ) ;

        return itemDefinition ;
    }

},
function (ArrayCardItemBuilder){
    GoraExplorer.arrayCardItemBuilder = ArrayCardItemBuilder ;
}) ;