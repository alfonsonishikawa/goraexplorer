/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Generates the item config for a checkbox field
 */
Ext.define('GoraExplorer.view.tables.builders.CheckboxCardItemBuilder', {
    singleton: true,

    uses: [
        'GoraExplorer.view.tables.builders.ItemBuilderHelper'
    ],

    /**
     * Generates the Values Card Item from a BooleanNode (of an avro schema tree node)
     *
     * @param {GoraExplorer.model.avro.BooleanNode} node - The boolean node to generate its item definition
     * @param {GoraExplorer.model.avro.EntitiesNode} tableSchemaEntitiesTree - The root of the entities schema tree
     * @param {String} currentBindPath
     * @return {Object} Item definition
     */
    generateItemDefinition: function(node, tableSchemaEntitiesTree, currentBindPath) {
        var me = this,
            bindPathExpression = '{' + currentBindPath + '}',
            nodeName = node.get('name'),
            nodeNullable = node.get('nullable'),
            nodeDoc = node.get('doc') ;

        var itemDefinition = {
            xtype : 'carditemcontainer',
            items: [
                {
                    xtype: 'valuescarditemlabel',
                    text: nodeName + ':',
                    tooltip: nodeDoc
                },
                {
                    xtype: 'box',
                    flex: 1
                },
                {
                    xtype: 'checkbox',
                    readOnly: true,
                    bind: bindPathExpression,
                    flex: 1
                }
            ]
        };
        // Add null stuff
        Ext.Array.insert(
            itemDefinition.items,
            itemDefinition.items.length,
            GoraExplorer.itemBuilderHelper.createNullItemsDefinition(currentBindPath, null, nodeNullable, ['delete', 'null'])
        ) ;

        return itemDefinition ;
    }

},
function (CheckboxCardItemBuilder){
    GoraExplorer.checkboxCardItemBuilder = CheckboxCardItemBuilder ;
}) ;