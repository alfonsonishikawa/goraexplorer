/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Generates the item config for a record field
 */
Ext.define('GoraExplorer.view.tables.builders.RecordCardItemBuilder', {
    singleton: true,

    uses: [
        'GoraExplorer.view.tables.builders.ItemBuilderHelper',
        'GoraExplorer.view.tables.cards.ShowChildCardButton'
    ],

    /**
     * Generates the Values Card Item from a RecordNode (of an avro schema tree node)
     *
     * @param {GoraExplorer.model.avro.RecordNode} node - The record node to generate its item definition
     * @param {GoraExplorer.model.avro.EntitiesNode} tableSchemaEntitiesTree - The root of the entities schema tree
     * @param {String} currentBindPath
     * @return {Object} Item definition
     */
    generateItemDefinition: function(node, tableSchemaEntitiesTree, currentBindPath) {
        var me = this,
            entityName = node.get('recordFieldTypeName'),
            entitySchema = tableSchemaEntitiesTree.findChild('name', entityName), // The data schema is not this node, but the EntityNode one referenced
            nodeName = node.get('name'),
            nodeNullable = node.get('nullable'),
            nodeDoc = node.get('doc') || entitySchema.get('doc') ;

        // Record node is a field inside other card
        var itemDefinition = {
            xtype : 'carditemcontainer',
            items: [
                {
                    xtype: 'valuescarditemlabel',
                    text: Ext.String.format ('{0} (record):', nodeName),
                    tooltip: nodeDoc
                },
                {
                    xtype: 'box',
                    flex: 1
                },
                {
                    xtype: 'show-childcard-button',
                    schemaNode: entitySchema,
                    bindPath: currentBindPath
                }
            ]
        };
        // Add null stuff
        Ext.Array.insert(
            itemDefinition.items,
            itemDefinition.items.length,
            GoraExplorer.itemBuilderHelper.createNullItemsDefinition(currentBindPath, node, nodeNullable, ['add', 'delete', 'null'])
        ) ;

        return itemDefinition ;
    }

},
function (RecordCardItemBuilder){
    GoraExplorer.recordCardItemBuilder = RecordCardItemBuilder ;
}) ;