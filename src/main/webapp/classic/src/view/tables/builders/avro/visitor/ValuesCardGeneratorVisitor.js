/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Given an EntityNode, generates the avro schema related.
 */
Ext.define('GoraExplorer.view.tables.builders.avro.visitor.ValuesCardGeneratorVisitor', {
    extend: 'GoraExplorer.model.avro.visitor.AbstractSchemaVisitor',

    uses: [
        'GoraExplorer.view.tables.builders.RecordCardItemBuilder',
        'GoraExplorer.view.tables.builders.CheckboxCardItemBuilder',
        'GoraExplorer.view.tables.builders.IntCardItemBuilder',
        'GoraExplorer.view.tables.builders.LongCardItemBuilder',
        'GoraExplorer.view.tables.builders.FloatCardItemBuilder',
        'GoraExplorer.view.tables.builders.DoubleCardItemBuilder',
        'GoraExplorer.view.tables.builders.StringCardItemBuilder',
        'GoraExplorer.view.tables.builders.BytesCardItemBuilder',
        'GoraExplorer.view.tables.builders.RecordCardItemBuilder',
        'GoraExplorer.view.tables.builders.MapCardItemBuilder',
        'GoraExplorer.view.tables.builders.ArrayCardItemBuilder'
    ],

    /**
     * @cfg {GoraExplorer.model.avro.EntitiesNode} tableSchemaEntitiesTree - Root node of the Entitites Tree
     */
    tableSchemaEntitiesTree: null,

    /**
     * @cfg {GoraExplorer.model.avro.EntityNode/
     *       GoraExplorer.model.avro.MapNode/
     *       GoraExplorer.model.avro.ArrayNode} mainNode - The main node schema being generated, needed to know
     *       when visiting a node to know if the card is being build, or a field inside a card instead.
     */
    mainNode: null,

    /**
     * The current path being used when visiting. Before calling `#visit(me)`, the currentBindPath must be set.
     */
    currentBindPath: null,

    /**
     * The current data at thepath {@link currentBindPath}
     */
    currentDataInPath: null,

    /**
     * @constructor
     * @param {String} namespace - The namespace to use as configuration value. Recommended the username :)
     * @param {GoraExplorer.model.avro.EntitiesNode rootEntitiesNode - The root "EntitiesNode" of the schema tree, one
     *          of the EntityNodes can be generated (visited).
     */
    constructor: function(tableSchemaEntitiesTree) {
        var me = this ;

        if (!Ext.isDefined(tableSchemaEntitiesTree)) {
            Ext.raise('Missing "tableSchemaEntitiesTree" parameter at ValuesCardGeneratorVisitor constructor') ;
        }

        me.tableSchemaEntitiesTree = tableSchemaEntitiesTree ;
    },

    /**
     * Given a schema node, generates the ValuesCard
     * @param {GoraExplorer.model.avro.EntityNode /
     *         GoraExplorer.model.avro.MapNode /
     *         GoraExplorer.model.avro.ArrayNode} node - The schema node to be generated
     * @param {String} bindPath - Bind path to the node being generated
     * @param {Object} dataInPath - Data at the bindPath
     */
    generateCard: function(node, bindPath, dataInPath) {
        var me = this ;

        me.mainNode = node ;
        me.currentBindPath = bindPath ;
        me.currentDataInPath = dataInPath ;
        return node.accept(me) ;
    },

    /**
     * Entry point. Given a schema node, generates an array item (with the delete button)
     *
     * @param {GoraExplorer.model.avro.SchemaNode} node - The schema node to be generated
     * @param {String} bindPath - Bind path to the node being generated
     * @param {Object} dataInPath - Data at the bindPath
     *
     * @return {Array} An array item definition (container)
     */
    generateArrayItem: function(node, bindPath, dataInPath) {
        var me = this ;

        me.mainNode = null ; //Force to generate always the item
        me.currentBindPath = bindPath ;
        me.currentDataInPath = dataInPath ;
        var itemDefinition = node.accept(me) ;

        itemDefinition.items.unshift(
            {
                xtype: 'deletearrayitembutton',
                bindPath: bindPath
            },
            {
                xtype: 'box',
                width: 16
            }
        );

        return itemDefinition ;
    },

    /**
     * Entry point. Given a schema node, generates a map item (with the delete button)
     *
     * @param {GoraExplorer.model.avro.SchemaNode} node - The schema node to be generated
     * @param {String} bindPath - Bind path to the node being generated
     * @param {Object} dataInPath - Data at the bindPath
     *
     * @return {Array} A map item definition (container)
     */
    generateMapItem: function(node, bindPath, dataInPath) {
        var me = this ;

        me.mainNode = null ; //Force to generate always the item
        me.currentBindPath = bindPath ;
        me.currentDataInPath = dataInPath ;
        var itemDefinition = node.accept(me) ;

        itemDefinition.items.unshift(
            {
                xtype: 'deletemapitembutton',
                bindPath: bindPath
            },
            {
                xtype: 'box',
                width: 16
            }
        );

        return itemDefinition ;
    },

    /**
     * Generates an array item, or an array card (depending on `me.mainNode`)
     * @param {GoraExplorer.model.avro.ArrayNode} node - The array node to generate. Item or card.
     * @return {Object} definition
     */
    visitArrayNode: function(node) {
        var me = this,
            bindPath = me.currentBindPath,
            dataInPath = me.currentDataInPath ;

        if (node === me.mainNode) {

            // Generating the Array Values Card
            var nodeDoc = node.get('doc'),
                tmpItemNode = node.getItemTypeNode() ;

            var items = [
                {
                    xtype: 'documentationlabel',
                    text: nodeDoc,
                    hidden: Ext.isEmpty(nodeDoc)
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'addarrayitembutton',
                            bindPath: bindPath,
                            itemSchema: tmpItemNode
                        }
                    ]
                }
            ] ;

            Ext.Array.forEach(dataInPath, function (key, index) {
                tmpItemNode.set('name', index.toString());
                var arrayBindPath = bindPath,
                    itemBindPath = arrayBindPath + '.' + index ;
                me.currentBindPath = itemBindPath; // When calling accept, this itemBindPath could be changed
                me.currentDataInPath = dataInPath[index]; // item data in path - not needed by now?

                var itemDefinition = tmpItemNode.accept(me);

                // Add delete item button
                itemDefinition.items.unshift(
                    {
                        xtype: 'deletearrayitembutton',
                        bindPath: itemBindPath
                    },
                    {
                        xtype: 'box',
                        width: 16
                    }
                );
                items.push(itemDefinition);
            }, me) ;

            return items;

        } else {
            // Generating an Array Item
            var me = this ;
            return GoraExplorer.arrayCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
        }

    },

    visitBooleanNode: function(node) {
        var me = this ;
        return GoraExplorer.checkboxCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
    },

    visitBytesNode: function(node) {
        var me = this ;
        return GoraExplorer.bytesCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
    },

    visitDoubleNode: function(node) {
        var me = this ;
        return GoraExplorer.doubleCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
    },

    visitEntityNode: function(node) {
        var me = this,
            recordBindPath = me.currentBindPath,
            nodeName = node.get('name'),
            nodeDoc = node.get('doc'),
            items = [ // Base items structure
                {
                    xtype: 'documentationlabel',
                    text: nodeDoc,
                    hidden: Ext.isEmpty(nodeDoc)
                }
            ];

        node.eachChild(function(childNode) {
            me.currentBindPath = recordBindPath + '.' + childNode.get('name') ;
            items.push(childNode.accept(me)) ;
        }) ;

        return items;
    },

    visitFloatNode: function(node) {
        var me = this ;
        return GoraExplorer.floatCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
    },

    visitIntNode: function(node) {
        var me = this ;
        return GoraExplorer.intCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
    },

    visitLongNode: function(node) {
        var me = this ;
        return GoraExplorer.longCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
    },

    visitMapNode: function(node) {
        var me = this,
            bindPath = me.currentBindPath,
            dataInPath = me.currentDataInPath ;

        if (node === me.mainNode) {

            // Generating the Map Values Card
            var nodeDoc = node.get('doc'),
                tmpValueNode = node.getValueTypeNode() ;

            var items = [
                {
                    xtype: 'documentationlabel',
                    text: nodeDoc,
                    hidden: Ext.isEmpty(nodeDoc)
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'addmapitembutton',
                            bindPath: bindPath,
                            itemSchema: tmpValueNode
                        }
                    ]
                }
            ];

            Ext.Array.forEach(Object.keys(dataInPath), function (key) {
                tmpValueNode.set('name', key) ;
                var mapBindPath = bindPath,
                    itemBindPath = mapBindPath + '.' + Ext.String.escapeBind(key) ;
                me.currentBindPath = itemBindPath ;
                me.currentDataInPath = dataInPath[key] ; // item data in path - not needed by now?

                var itemDefinition = tmpValueNode.accept(me) ;

                // Add delete item button
                itemDefinition.items.unshift(
                    {
                        xtype: 'deletemapitembutton',
                        bindPath: itemBindPath
                    },
                    {
                        xtype: 'box',
                        width: 16
                    }
                );
                items.push(itemDefinition);
            }, me) ;

            return items;

        } else {
            // Generating a Map Item
            var me = this ;
            return GoraExplorer.mapCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
        }

    },

    /**
     * Generates a record node field.
     *
     * @param {GoraExplorer.model.avro.RecordNode} node - Record schema node
     * @return {Array}
     */
    visitRecordNode: function(node) {
        var me = this ;
        return GoraExplorer.recordCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
    },

    visitStringNode: function(node) {
        var me = this ;
        return GoraExplorer.stringCardItemBuilder.generateItemDefinition(node, me.tableSchemaEntitiesTree, me.currentBindPath) ;
    }

}) ;