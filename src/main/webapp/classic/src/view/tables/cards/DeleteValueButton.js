/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A Delete Button that deletes (write null) the value at "bindPath" when clicked.
 *
 * Depends on variable 'editing' in the ViewModel.
 */
Ext.define('GoraExplorer.view.tables.cards.DeleteValueButton', {
    extend: 'Ext.button.Button',

    alias: 'widget.deletevaluebutton',

    config: {
        /**
         * @cfg {String} bindPath - The bind path to the value this button can delete
         */
        bindPath: null
    },

    /**
     * Applies the bindPath configuration to the ViewModel internally
     * @param config
     * @return {*|Object}
     */
    initConfig: function(config) {
        var me = this ;

        Ext.apply(config, {
            viewModel: {
                formulas: {
                    editingAndNotNull: {
                        bind: {
                            value: '{' + config.bindPath + '}'
                        }
                    }
                }
            }
        }) ;

        return me.callParent(arguments) ;
    },

    iconCls: 'fa fa-trash',
    tooltip: _('Set the value to null'),
    disabled: true,
    bind: {
        disabled: '{!editingAndNotNull}'
    },
    viewModel: {
        formulas: {
            editingAndNotNull: {
                bind: {
                    value: '', // Will be overwritten at #initComponent()
                    editing: '{editing}'
                },
                get: function (bindValues) {
                    return bindValues.editing && bindValues.value !== null;
                }
            }
        }
    },
    listeners: {
        click: 'deleteValueClick'
    }

}) ;