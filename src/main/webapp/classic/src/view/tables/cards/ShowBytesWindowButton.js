/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A "show more" button for byte values region field.
 *
 * There is a required configuration:
 *
 * ```
 * {
 *     xtype: 'show-byteswindow-button',
 *     bindPath: {String}
 * },
 * ```
 *
 */
Ext.define('GoraExplorer.view.tables.cards.ShowBytesWindowButton', {
    extend: 'Ext.button.Button',

    alias: 'widget.showbyteswindowbutton',

    iconCls: 'fa fa-list',

    config: {
        /**
         * @cfg {String} bindPath - Bind path with the data of this field
         */
        bindPath: null
    },

    bind: {
        disabled: '{isNull}'
    },
    viewModel: {
        formulas: {
            isNull: {
                bind: '', // Will be overwritten at #initComponent()
                get: function (value) {
                    return value === null;
                }
            }
        }
    },

    /**
     * Applies the bindPath configuration to the ViewModel internally
     * @param config
     * @return {*|Object}
     */
    initConfig: function(config) {
        var me = this ;

        Ext.apply(config, {
            viewModel: {
                formulas: {
                    isNull: {
                        bind: '{' + config.bindPath + '}'
                    }
                }
            }
        }) ;

        return me.callParent(arguments) ;
    },

    listeners: {
        click: 'onShowBytesWindowClick'
    }

});