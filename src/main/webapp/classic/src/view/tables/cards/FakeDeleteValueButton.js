/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A Fake Delete Button that is shown always disabled
 */
Ext.define('GoraExplorer.view.tables.cards.FakeDeleteValueButton', {
    extend: 'Ext.button.Button',

    alias: 'widget.fakedeletevaluebutton',

    cls: 'button-fake',

    iconCls: 'fa fa-trash',
    tooltip: _('Set the value to null'),
    disabled: true
}) ;