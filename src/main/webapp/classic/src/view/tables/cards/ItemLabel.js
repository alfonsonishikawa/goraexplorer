/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * An item label that shows the documentation.
 *
 * The following properties are mandatory to configure:
 *
 * * text (the label text)
 * * nodeDoc (the label documentation)
 *
 * Depends on the ViewModel variables:
 *
 * * {labelsWidth}
 *
 */
Ext.define('GoraExplorer.view.tables.cards.ItemLabel', {
    extend: 'Ext.form.Label',
    alias: 'widget.valuescarditemlabel',

    config: {
        /**
         * @cfg {String} [tooltip] - The tooltip text to show
         */
        tooltip: null
    },

    width: 125,

    bind: {
        width: '{labelsWidth}'
    },

    resizable: {
        minWidth:125
    },
    resizeHandles: 'e',

    listeners: {
        resize: 'onLabelResize',
        render: function () {
            var me = this,
                tooltip = me.getTooltip();

            if (!Ext.isEmpty(tooltip)) {
                Ext.QuickTips.register({
                    target: me.getEl(),
                    text: tooltip
                });
            }
        }
    },

    /**
     * Cleanup the quicktip
     * @override
     */
    doDestroy: function() {
        var me = this,
            tooltip = me.getTooltip();

        if (!Ext.isEmpty(tooltip)) {
            Ext.QuickTips.unregister(me.getEl());
        }
        me.callParent(arguments) ;
    }

}) ;