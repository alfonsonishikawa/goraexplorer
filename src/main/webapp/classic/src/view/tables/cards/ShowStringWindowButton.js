/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A "show more" button for string values region field.
 *
 * There is a required configuration:
 *
 * ```
 * {
 *     xtype: 'showstringwindowbutton',
 *     bindPath: {String}
 * },
 * ```
 *
 */
Ext.define('GoraExplorer.view.tables.cards.ShowStringWindowButton', {
    extend: 'Ext.button.Button',

    alias: 'widget.showstringwindowbutton',

    iconCls: 'fa fa-list',

    viewModel: true,

    config: {
        /**
         * @cfg {String} bindPath - Bind path with the data of this field
         */
        bindPath: null
    },

    listeners: {
        click: 'onShowStringWindowClick'
    },

    /**
     * Applies the bindPath configuration to the disabled status
     * @param config
     * @return {*|Object}
     */
    initConfig: function(config) {
        var me = this ;

        Ext.apply(config, {
            bind: {
                disabled: '{!' + config.bindPath + '}'
            }
        }) ;

        return me.callParent(arguments) ;
    }

});