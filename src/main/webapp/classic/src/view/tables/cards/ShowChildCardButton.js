/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A "show more" button for values region field that must be shown in a new card.
 * Any parent controller can listen the click event with the method #onShowChildCardClick().
 *
 * There is some required configuration:
 *
 * ```
 * {
 *     xtype: 'show-childcard-button',
 *     schemaNode: node,
 *     bindPath: currentBindPath
 * },
 * ```
 *
 * On *Click*, executes 'onShowChildCardClick' method of any controller holding this button
 *
 */
Ext.define('GoraExplorer.view.tables.cards.ShowChildCardButton', {
    extend: 'Ext.button.Button',

    alias: 'widget.show-childcard-button',

    iconCls: 'fa fa-list',

    viewModel: true,

    config: {
        /**
         * @cfg {GoraExplorer.model.avro.SchemaNode} - The node schema this button will show
         */
        schemaNode: null,

        /**
         * @cfg {String} bindPath - Bind path with the data of this field
         */
        bindPath: null
    },

    listeners: {
        click: 'onShowChildCardClick'
    },

    /**
     * Sets the bindings with the config data
     */
    beforeRender: function () {
        var me = this;
        me.setBind({
            disabled: '{!' + me.getBindPath() + '}'
        }) ;
        me.callParent(arguments);
    }

});