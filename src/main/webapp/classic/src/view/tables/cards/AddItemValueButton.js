/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * An Add defaut value button to create a new value to null fields.
 *
 * Has the following mandatory config parameters:
 *
 * * bindPath
 * * valueSchema
 *
 * Depends on variable 'editing' in the ViewModel.
 */
Ext.define('GoraExplorer.view.tables.cards.AddItemValueButton', {
    extend: 'Ext.button.Button',

    alias: 'widget.additemvaluebutton',

    config: {
        /**
         * @cfg {String} bindPath - The bind path to the value this button can delete
         */
        bindPath: null,
        /**
         * @cfg {GoraExplorer.model.avro.SchemaNode} itemSchema - the schema node of the values to create a
         *          default data values
         */
        itemSchema: null
    },

    /**
     * Applies the bindPath configuration to the ViewModel internally
     * @param config
     * @return {*|Object}
     */
    initConfig: function(config) {
        var me = this ;

        Ext.apply(config, {
            viewModel: {
                formulas: {
                    editingAndNull: {
                        bind: {
                            value: '{' + config.bindPath + '}'
                        }
                    }
                }
            }
        }) ;

        return me.callParent(arguments) ;
    },

    iconCls: 'fa fa-plus-circle',
    tooltip: _('Create a new value for the field'),
    disabled: true,
    bind: {
        disabled: '{!editingAndNull}'
    },
    viewModel: {
        formulas: {
            editingAndNull: {
                bind: {
                    value: '', // Will be overwritten at #initComponent()
                    editing: '{editing}'
                },
                get: function (bindValues) {
                    return bindValues.editing && bindValues.value === null;
                }
            }
        }
    },

    listeners: {
        click: 'addValueClick'
    }

}) ;