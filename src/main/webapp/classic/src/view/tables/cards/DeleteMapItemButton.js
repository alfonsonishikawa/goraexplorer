/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Button to be used at Map Values Card. It will be used to delete a map item element
 */
Ext.define('GoraExplorer.view.tables.cards.DeleteMapItemButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.deletemapitembutton',

    config: {
        /**
         * @cfg {String} bindPath - The bind path to the map value this button can delete
         */
        bindPath: null
    },

    iconCls: 'fa fa-trash',
    tooltip: _('Delete map item'),
    disabled: true,
    bind: {
        disabled: '{!editing}'
    },

    listeners: {
        click: 'deleteMapItemClick'
    }

}) ;