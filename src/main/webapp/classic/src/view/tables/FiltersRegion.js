/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.tables.FiltersRegion', {
    extend: 'Ext.form.Panel',

    uses: [],

    alias: 'widget.filtersregion',

    title: _('Filters'),

    reference: 'filtersRegion',
    split: false,
    collapsible: true,
    collapsed: true,
    glyph: 'xf0b0@FontAwesome',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    minHeight: 190,
    maxHeight: 190,
    minWidth: 360,

    defaults: {
        labelWidth: 105
    },

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [
            {
                text: _('Refresh'),
                iconCls: 'x-fa fa-refresh',
                listeners: {
                    click: 'onRefreshClick'
                }
            },
            {
                text: _('Search'),
                iconCls: 'x-fa fa-search',
                listeners: {
                    click: 'onSearchClick'
                }
            },
            {
                text: _('Clear'),
                iconCls: 'x-fa fa-eraser',
                listeners: {
                    click: 'onClearClick'
                }
            }
        ]
    }],

    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            defaults: {
                labelWidth: 105
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'startKey',
                    fieldLabel: _('Start key'),
                    value: '',
                    bind: '{tableStore.startKey}',
                    flex: 1
                },
                {
                    xtype: 'textfield',
                    name: 'endKey',
                    fieldLabel: _('End key'),
                    value: '',
                    bind: '{tableStore.endKey}',
                    flex: 1
                }
            ]
        },
        {
            xtype: 'numberfield',
            name: 'scanSize',
            fieldLabel: _('Num. rows'),
            value: 10,
            bind: '{tableStore.scanSize}',
            minValue: 2,
            maxWidth: 200
        }
    ]
});