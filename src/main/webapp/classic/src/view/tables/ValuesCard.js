/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class that represents a Values card holding elements of one given level.
 *
 * @example
 *
 * Ext.widget('valuescard', {
 *     items: items
 * }) ;
 *
 */
Ext.define('GoraExplorer.view.tables.ValuesCard', {
    extend: 'Ext.form.Panel',

    alias: 'widget.valuescard',

    bodyCls: 'valuescard-body',

    uses: [
        'GoraExplorer.view.component.CardItemContainer',
        'GoraExplorer.view.tables.ValuesCardModel'
    ],

    config: {
        /**
         * @cfg {String} cardId (required) - ValuesCard Id (usually MD5(bindPath))
         */
        cardId: null,

        /**
         * @cfg {String} bindPath (required) - Bind path of the data shown in this card
         */
        bindPath: null,

        /**
         * @cfg {SchemaNode} schemaNode (required) - Avro type schema of the data shown in this card
         */
        schemaNode: null
    },

    /**
     * @property {String} bindPathKey - Key in the data structure (last token of bindPath)
     * @readonly
     */
    bindPathKey: undefined,

    /**
     * @property {String} bindPathRoute - Route to the bindPathKey in the bindPath. `a.b.c` in bindPath `a.b.c.key`
     * @readonly
     */
    bindPathRoute: undefined,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    scrollable: true,

    /**
     * Returns the `bindPath` key component (the last token). `key` in `a.b.c.key`
     *
     * @return {String} The last token of the bind path
     */
    getBindPathKey: function() {
        return this.bindPathKey ;
    },

    /**
     * Returns the `bindPath` route component. `a.b.c` in `a.b.c.key`
     * @returns {String} the route component of the bind path
     */
    getBindPathRoute: function() {
        return this.bindPathRoute ;
    },

    /**
     * Updates the property {@link bindPathKey} with the last token from bindPath
     *
     * @param {String} newBindPath
     */
    updateBindPath: function(newBindPath) {
        var me = this,
            tokens = Ext.String.splitByUnescapedDots(newBindPath),
            bindPathRouteLength; // Expected length of the bind path route string

        // Extracts `key` from `a.b.c.key`
        me.bindPathKey = tokens.pop() ;
        bindPathRouteLength = newBindPath.length - me.bindPathKey.length ;
        if (tokens.length !== 0) {
            // Several tokens, must delete the '.' after bind path route: 'a.b.c.'
            bindPathRouteLength-- ;
        }
        // Extracts `a.b.c` from `a.b.c.key`
        me.bindPathRoute = newBindPath.substr(0, bindPathRouteLength) ;
    }

});