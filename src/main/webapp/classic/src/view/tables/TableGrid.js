/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.tables.TableGrid', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.tablegrid',

    layout: 'fit',

    publishes: 'atEnd',

    viewConfig: {
        preserveScrollOnReload: true,
        enableTextSelection: true,
        markDirty: false
    },

    config: {
        atEnd: false
    },

    viewModel: {
        formulas: {
            atEnd: function(get) {
                var me = this,
                    store = me.getView().getStore() ;

                return store.atEnd ;
            }
        }
    },

    bind: {
        store: '{tableStore}'
    },
    columnLines: true,

    listeners: {
        select: 'onTableRowClick',
        beforeselect: 'onTableRowBeforeSelectClick'
    },

    dockedItems:[{
        xtype: 'toolbar',
        dock: 'top',
        items: [
            {
                text: _('Add new row'),
                iconCls: 'x-fa fa-plus',
                handler: 'onAddNewRowClick'
            },
            {
                text: _('Delete row'),
                iconCls: 'x-fa fa-minus',
                handler: 'onDeleteRowClick',
                bind: {
                    disabled: '{!tableGrid.selection}'
                }
            },
            '->',
            {
                text: _('Load more rows'),
                iconCls: 'x-fa fa-angle-double-down',
                handler: 'onLoadMoreRowsClick'
            }
        ]
    }],

    /**
     * Adds the column Key configuration to the list of columns
     * @param config
     */
    initConfig: function (config) {
        config.columns.unshift(
            {
                xtype: 'rownumberer'
            },
            {
                header: _('Key'),
                dataIndex: '__id__'
            }
        );
        this.callParent([config]) ;
    },

    /**
     * Hooks the "scrollend" event handler
     */
    initComponent: function() {
        var me = this ;
        me.callParent() ;
        me.getView().getScrollable().on('scrollend', Ext.Function.alias(me, 'onScrollEnd')) ;
    },

    /**
     * When ending scrolling, handles the autoload of more rows
     * @param scrollable
     * @param xPosition
     * @param yPosition
     */
    onScrollEnd: function(scrollable, xPosition, yPosition) {
        var me = this,
            maxYPosition = scrollable.getMaxPosition().y ;

        if( yPosition + 20 > maxYPosition && !me.getStore().atEnd) {
            me.getStore().loadNextRows() ;
        }
    },

    /**
     * Validates that the cofigured store inherits from {@link GoraExplorer.data.GoraStore}.
     * Can only be configured with a GoraStore.
     *
     * @param {GoraExplorer.data.GoraStore} newStore
     * @returns {GoraExplorer.data.GoraStore}
     */
    applyStore: function(newStore) {
        if ( !(newStore instanceof GoraExplorer.data.GoraStore) ) {
            Ext.log({level: 'error', stack: true}, 'Invalid store class ' + Ext.getClassName(newStore) + '. Must inherit from GoraExplorer.data.GoraStore.') ;
            return null;
        }
        return newStore;
    }

});