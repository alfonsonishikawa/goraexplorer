
/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.login.Login',{
    extend: 'Ext.window.Window',

    alias: 'widget.login',

    requires: [
        'GoraExplorer.view.login.LoginController',
        'GoraExplorer.view.login.LoginModel',
        'GoraExplorer.view.settings.Language'
    ],

    controller: 'login',
    viewModel: 'login',

    cls: 'login-window',

    defaultFocus: 'textfield:focusable:not([hidden]):not([disabled]):not([value])',
    onEsc: Ext.emptyFn,

    layout: 'fit',
    iconCls: 'x-fa fa-key',
    title: _('Login'),
    closeAction: 'hide',
    closable: false,
    draggable: false,
    resizable: false,
    modal: true,

    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                {
                    xtype: 'language',
                    name: 'language'
                },
                '->',
                {
                    xtype: 'button',
                    name: 'loginNext',
                    formBind: true,
                    iconCls: 'x-fa fa-sign-in fa-lg',
                    text: _('Next'),
                    listeners: {
                        click: 'doLoginClick'
                    }
                },
                '->'
            ]
        }
    ],

    items: [
        {
            xtype: 'form',
            reference: 'form',
            bodyPadding: '15px 15px 0px 15px',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                xtype: 'textfield',
                vtype: 'alphanum',
                minLength: 5,
                allowBlank: false
            },
            items: [
                {
                    name: 'username',
                    fieldLabel: _('Username'),
                    reference: 'usernameField',
                    maxLength: 50,
                    listeners: {
                        specialkey: 'onSpecialKeyPress'
                    }
                },
                {
                    inputType: 'password',
                    name: 'password',
                    fieldLabel: _('Password'),
                    listeners: {
                        specialkey: 'onSpecialKeyPress'
                    }
                },
                {
                    xtype: 'label',
                    reference: 'errorlabel',
                    text: _('Bad username/password'),
                    cls: 'errorText',
                    hidden: true,
                    bind: {
                        hidden: '{!loginError}'
                    }
                },
                {
                    xtype: 'checkbox',
                    boxLabel: _('Remember me'),
                    name: 'remember-me'
                }
            ]
        }
    ],

    /**
     * This will set the window visible message when on the livedemo.
     */
    afterShow: function() {
        var me = this ;

        if (Ext.Array.contains(Ext.manifest.tags,"livedemo")){
            var errorLabel = me.lookupReference("errorlabel") ;
            errorLabel.setText(_('User: "admin", password: "admin"')) ;
            me.getViewModel().set("loginError", true) ; // Will show the tip message
        }
    }
});
