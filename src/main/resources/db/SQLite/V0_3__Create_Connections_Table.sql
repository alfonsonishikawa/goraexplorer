create table connections (
	id integer primary key autoincrement not null,
	name varchar(50) not null,
	description varchar(500),
	type varchar(50) not null,
	gora_properties text,
	hadoop_configuration text,
	username varchar(50) not null,

	constraint fk_connections_users foreign key(username) references users(username)
) ;
