create table users(
	username varchar(50) not null primary key,
	password varchar(100) not null,
	email varchar(100),
	image_url varchar(1024),
	enabled boolean not null
) WITHOUT ROWID ;

create table authorities (
	username varchar(50) not null,
	authority varchar(50) not null,
	primary key (username, authority),
	constraint fk_authorities_users foreign key(username) references users(username)
) WITHOUT ROWID ;

create table groups (
	id integer primary key autoincrement not null,
	group_name varchar(50) not null
);

create table group_authorities (
	group_id integer not null,
	authority varchar(50) not null,
	constraint fk_group_authorities_group foreign key(group_id) references groups(id)
);

create table group_members (
	id integer primary key autoincrement not null,
	username varchar(50) not null,
	group_id integer not null,
	constraint fk_group_members_group foreign key(group_id) references groups(id)
);

create table persistent_logins (
	username varchar(64) not null,
	series varchar(64) primary key,
	token varchar(64) not null,
	last_used timestamp not null
) WITHOUT ROWID ;