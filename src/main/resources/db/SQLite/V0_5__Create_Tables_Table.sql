create table tables (
	id integer primary key autoincrement not null,
	name varchar(50) not null,
	description varchar(500),
	avro_schema text,
	mapping text,
	key_class varchar(500) not null,
	connection_id integer not null,
	
	constraint fk_tables_connections foreign key(connection_id) references connections(id)
) ;
