INSERT INTO hibernate_sequence(sequence_name, next_val)
VALUES
	('connections', 100),
	('tables', 100),
	('groups', 100),
	('group_members', 100)
;