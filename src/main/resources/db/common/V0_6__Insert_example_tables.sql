INSERT INTO tables(id, name, description, avro_schema, mapping, key_class, connection_id)
VALUES
	(1, 'webpage', 'Nutch crawled webpages', '{ 
  "name": "WebPage",
  "type": "record",
  "namespace": "org.apache.nutch.storage",
  "doc": "WebPage is the primary data structure in Nutch representing crawl data for a given WebPage at some point in time",
  "fields": [
    {
      "name": "baseUrl",
      "type": [
        "null",
        "string"
      ],
      "doc": "The original associated with this WebPage.",
      "default": null
    },
    {
      "name": "status",
      "type": "int",
      "doc": "A crawl status associated with the WebPage, can be of value STATUS_UNFETCHED - WebPage was not fetched yet, STATUS_FETCHED - WebPage was successfully fetched, STATUS_GONE - WebPage no longer exists, STATUS_REDIR_TEMP - WebPage temporarily redirects to other page, STATUS_REDIR_PERM - WebPage permanently redirects to other page, STATUS_RETRY - Fetching unsuccessful, needs to be retried e.g. transient errors and STATUS_NOTMODIFIED - fetching successful - page is not modified",
      "default": 0
    },
    {
      "name": "fetchTime",
      "type": "long",
      "doc": "The system time in milliseconds for when the page was fetched.",
      "default": 0
    },
    {
      "name": "prevFetchTime",
      "type": "long",
      "doc": "The system time in milliseconds for when the page was last fetched if it was previously fetched which can be used to calculate time delta within a fetching schedule implementation",
      "default": 0
    },
    {
      "name": "fetchInterval",
      "type": "int",
      "doc": "The default number of seconds between re-fetches of a page. The default is considered as 30 days unless a custom fetch schedle is implemented.",
      "default": 0
    },
    {
      "name": "retriesSinceFetch",
      "type": "int",
      "doc": "The number of retried attempts at fetching the WebPage since it was last successfully fetched.",
      "default": 0
    },
    {
      "name": "modifiedTime",
      "type": "long",
      "doc": "The system time in milliseconds for when this WebPage was modified by the WebPage author, if this is not available we default to the server for this information. This is important to understand the changing nature of the WebPage.",
      "default": 0
    },
    {
      "name": "prevModifiedTime",
      "type": "long",
      "doc": "The system time in milliseconds for when this WebPage was previously modified by the author, if this is not available then we default to the server for this information. This is important to understand the changing nature of a WebPage.",
      "default": 0
    },
    {
      "name": "protocolStatus",
      "type": [
        "null",
        {
          "name": "ProtocolStatus",
          "type": "record",
          "namespace": "org.apache.nutch.storage",
          "doc": "A nested container representing data captured from web server responses.",
          "fields": [
            {
              "name": "code",
              "type": "int",
              "doc": "A protocol response code which can be one of SUCCESS - content was retrieved without errors, FAILED - Content was not retrieved. Any further errors may be indicated in args, PROTO_NOT_FOUND - This protocol was not found. Application may attempt to retry later, GONE - Resource is gone, MOVED - Resource has moved permanently. New url should be found in args, TEMP_MOVED - Resource has moved temporarily. New url should be found in args., NOTFOUND - Resource was not found, RETRY - Temporary failure. Application may retry immediately., EXCEPTION - Unspecified exception occured. Further information may be provided in args., ACCESS_DENIED - Access denied - authorization required, but missing\/incorrect., ROBOTS_DENIED - Access denied by robots.txt rules., REDIR_EXCEEDED - Too many redirects., NOTFETCHING - Not fetching., NOTMODIFIED - Unchanged since the last fetch., WOULDBLOCK - Request was refused by protocol plugins, because it would block. The expected number of milliseconds to wait before retry may be provided in args., BLOCKED - Thread was blocked http.max.delays times during fetching.",
              "default": 0
            },
            {
              "name": "args",
              "type": {
                "type": "array",
                "items": "string"
              },
              "doc": "Optional arguments supplied to compliment and\/or justify the response code.",
              "default": [
                
              ]
            },
            {
              "name": "lastModified",
              "type": "long",
              "doc": "A server reponse indicating when this page was last modified, this can be unreliable at times hence this is used as a default fall back value for the preferred ''modifiedTime'' and ''preModifiedTime'' obtained from the WebPage itself.",
              "default": 0
            }
          ]
        }
      ],
      "default": null
    },
    {
      "name": "content",
      "type": [
        "null",
        "bytes"
      ],
      "doc": "The entire raw document content e.g. raw XHTML",
      "default": null
    },
    {
      "name": "contentType",
      "type": [
        "null",
        "string"
      ],
      "doc": "The type of the content contained within the document itself. ContentType is an alias for MimeType. Historically, this parameter was only called MimeType, but since this is actually the value included in the HTTP Content-Type header, it can also include the character set encoding, which makes it more than just a MimeType specification. If MimeType is specified e.g. not None, that value is used. Otherwise, ContentType is used. If neither is given, the DEFAULT_CONTENT_TYPE setting is used.",
      "default": null
    },
    {
      "name": "prevSignature",
      "type": [
        "null",
        "bytes"
      ],
      "doc": "An implementation of a WebPage''s previous signature from which it can be identified and referenced at any point in time. This can be used to uniquely identify WebPage deltas based on page fingerprints.",
      "default": null
    },
    {
      "name": "signature",
      "type": [
        "null",
        "bytes"
      ],
      "doc": "An implementation of a WebPage''s signature from which it can be identified and referenced at any point in time. This is essentially the WebPage''s fingerprint represnting its state for any point in time.",
      "default": null
    },
    {
      "name": "title",
      "type": [
        "null",
        "string"
      ],
      "doc": "The title of the WebPage.",
      "default": null
    },
    {
      "name": "text",
      "type": [
        "null",
        "string"
      ],
      "doc": "The textual content of the WebPage devoid from native markup.",
      "default": null
    },
    {
      "name": "parseStatus",
      "type": [
        "null",
        {
          "name": "ParseStatus",
          "type": "record",
          "namespace": "org.apache.nutch.storage",
          "doc": "A nested container representing parse status data captured from invocation of parsers on fetch of a WebPage",
          "fields": [
            {
              "name": "majorCode",
              "type": "int",
              "doc": "Major parsing status'' including NOTPARSED (Parsing was not performed), SUCCESS (Parsing succeeded), FAILED (General failure. There may be a more specific error message in arguments.)",
              "default": 0
            },
            {
              "name": "minorCode",
              "type": "int",
              "doc": "Minor parsing status'' including SUCCESS_OK - Successful parse devoid of anomalies or issues, SUCCESS_REDIRECT - Parsed content contains a directive to redirect to another URL. The target URL can be retrieved from the arguments., FAILED_EXCEPTION - Parsing failed. An Exception occured which may be retrieved from the arguments., FAILED_TRUNCATED - Parsing failed. Content was truncated, but the parser cannot handle incomplete content., FAILED_INVALID_FORMAT - Parsing failed. Invalid format e.g. the content may be corrupted or of wrong type., FAILED_MISSING_PARTS - Parsing failed. Other related parts of the content are needed to complete parsing. The list of URLs to missing parts may be provided in arguments. The Fetcher may decide to fetch these parts at once, then put them into Content.metadata, and supply them for re-parsing., FAILED_MISING_CONTENT - Parsing failed. There was no content to be parsed - probably caused by errors at protocol stage.",
              "default": 0
            },
            {
              "name": "args",
              "type": {
                "type": "array",
                "items": "string"
              },
              "doc": "Optional arguments supplied to compliment and\/or justify the parse status code.",
              "default": [
                
              ]
            }
          ]
        }
      ],
      "default": null
    },
    {
      "name": "score",
      "type": "float",
      "doc": "A score used to determine a WebPage''s relevance within the web graph it is part of. This score may change over time based on graph characteristics.",
      "default": 0
    },
    {
      "name": "reprUrl",
      "type": [
        "null",
        "string"
      ],
      "doc": "In the case where we are given two urls, a source and a destination of a redirect, we should determine and persist the representative url. The logic used to determine this is based largely on Yahoo!''s Slurp Crawler",
      "default": null
    },
    {
      "name": "headers",
      "type": {
        "type": "map",
        "values": [
          "null",
          "string"
        ]
      },
      "doc": "Header information returned from the web server used to server the content which is subsequently fetched from. This includes keys such as TRANSFER_ENCODING, CONTENT_ENCODING, CONTENT_LANGUAGE, CONTENT_LENGTH, CONTENT_LOCATION, CONTENT_DISPOSITION, CONTENT_MD5, CONTENT_TYPE, LAST_MODIFIED and LOCATION.",
      "default": {
        
      }
    },
    {
      "name": "outlinks",
      "type": {
        "type": "map",
        "values": [
          "null",
          "string"
        ]
      },
      "doc": "Embedded hyperlinks which direct outside of the current domain.",
      "default": {
        
      }
    },
    {
      "name": "inlinks",
      "type": {
        "type": "map",
        "values": [
          "null",
          "string"
        ]
      },
      "doc": "Embedded hyperlinks which link to pages within the current domain.",
      "default": {
        
      }
    },
    {
      "name": "markers",
      "type": {
        "type": "map",
        "values": [
          "null",
          "string"
        ]
      },
      "doc": "Markers flags which represent user and machine decisions which have affected influenced a WebPage''s current state. Markers can be system specific and user machine driven in nature. They are assigned to a WebPage on a job-by-job basis and thier values indicative of what actions should be associated with a WebPage.",
      "default": {
        
      }
    },
    {
      "name": "metadata",
      "type": {
        "type": "map",
        "values": [
          "null",
          "bytes"
        ]
      },
      "doc": "A multi-valued metadata container used for storing everything from structured WebPage characterists, to ad-hoc extraction and metadata augmentation for any given WebPage.",
      "default": {
        
      }
    },
    {
      "name": "batchId",
      "type": [
        "null",
        "string"
      ],
      "doc": "A batchId that this WebPage is assigned to. WebPage''s are fetched in batches, called fetchlists. Pages are partitioned but can always be associated and fetched alongside pages of similar value (within a crawl cycle) based on batchId.",
      "default": null
    },
    {
      "name": "sitemaps",
      "type": {
        "type": "map",
        "values": [
          "null",
          "string"
        ]
      },
      "doc": "Sitemap urls in robot.txt",
      "default": {}
    },
    {
      "name": "stmPriority",
      "type": "float",
      "doc": "",
      "default": 0
    }
  ]
}', '<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<!--
In addition to family ''name'' and ''maxVersions'' attributes, 
individual table families can ve defined with several other
attributes such as
  compression="$$$" - the compression option to use in HBase.
  blockCache="$$$"  - an LRU cache that contains three levels of block priority 
                      to allow for scan-resistance and in-memory ColumnFamilies.
  blockSize="$$$"   - The blocksize can be configured for each ColumnFamily in 
                      a table, and this defaults to 64k.
  bloomFilter="$$$" - Bloom Filters can be enabled per-ColumnFamily.
  maxVersions="$$$" - The maximum number of row versions to store is configured 
                      per column family via HColumnDescriptor.
  timeToLive="$$$"  - ColumnFamilies can set a TTL length in seconds, and HBase 
                      will automatically delete rows once the expiration time is 
                      reached.
  inMemory="$$$"    - ColumnFamilies can optionally be defined as in-memory.

You should consult the current Gora gora-hbase documentation
for further information on properties and mapping configurtion.
http://gora.apache.org/current/gora-hbase.html
-->

<gora-orm>
    
    <table name="webpage">
        <family name="p" maxVersions="1"/>
        <family name="f" maxVersions="1"/>
        <family name="s" maxVersions="1"/>
        <family name="il" maxVersions="1"/>
        <family name="ol" maxVersions="1"/>
        <family name="stm" maxVersions="1"/>
        <family name="h" maxVersions="1"/>
        <family name="mtdt" maxVersions="1"/>
        <family name="mk" maxVersions="1"/>
    </table>
    <class table="webpage" keyClass="java.lang.String" name="org.apache.nutch.storage.WebPage">
        
        <!-- fetch fields                                       -->
        <field name="baseUrl" family="f" qualifier="bas"/>
        <field name="status" family="f" qualifier="st"/>
        <field name="prevFetchTime" family="f" qualifier="pts"/>
        <field name="fetchTime" family="f" qualifier="ts"/>
        <field name="fetchInterval" family="f" qualifier="fi"/>
        <field name="retriesSinceFetch" family="f" qualifier="rsf"/>
        <field name="reprUrl" family="f" qualifier="rpr"/>
        <field name="content" family="f" qualifier="cnt"/>
        <field name="contentType" family="f" qualifier="typ"/>
        <field name="protocolStatus" family="f" qualifier="prot"/>
        <field name="modifiedTime" family="f" qualifier="mod"/>
        <field name="prevModifiedTime" family="f" qualifier="pmod"/>
        <field name="batchId" family="f" qualifier="bid"/>
     	  <field name="sitemaps" family="stm"/>
     

        <!-- parse fields                                       -->
        <field name="title" family="p" qualifier="t"/>
        <field name="text" family="p" qualifier="c"/>
        <field name="parseStatus" family="p" qualifier="st"/>
        <field name="signature" family="p" qualifier="sig"/>
        <field name="prevSignature" family="p" qualifier="psig"/>
        
        <!-- score fields                                       -->
        <field name="score" family="s" qualifier="s"/>
        <field name="stmPriority" family="s" qualifier="sp"/>

        <field name="headers" family="h"/>
        <field name="inlinks" family="il"/>
        <field name="outlinks" family="ol"/>
        <field name="metadata" family="mtdt"/>
        <field name="markers" family="mk"/>
    </class>
    
    <table name="host">
      <family name="mtdt" maxVersions="1"/>
      <family name="il" maxVersions="1"/>
      <family name="ol" maxVersions="1"/>
    </table>
    
    <class table="host" keyClass="java.lang.String" name="org.apache.nutch.storage.Host">
      <field name="metadata" family="mtdt"/>
      <field name="inlinks" family="il"/>
      <field name="outlinks" family="ol"/>
    </class>
    
</gora-orm>
', 'java.lang.String', 1),





	(2, 'tweets', 'Tweets from Twitter', '{"name": "Tweets",
 "type": "record",
 "namespace": "goraexplorer.twitter",
 "doc": "Tweet data",
 "fields": [
        {"name": "id",                 "type": "long", "default": 0}, 
        {"name": "status",             "type": "string", "default": ""}, 
        {"name": "creado",             "type": ["null","string"], "default": null},
        {"name": "latitud",            "type": ["null","double"], "default": null},
        {"name": "longitud",           "type": ["null","double"], "default": null},
        {"name": "inReplyToUserLogin", "type": ["null","string"], "default": null},
        {"name": "inReplyToStatusId",  "type": ["null","long"], "default": null},
        {"name": "inReplyToUserId",    "type": ["null","long"], "default": null},
        {"name": "vecesRetweet",       "type": "long", "default": 0},
        {"name": "source",             "type": "string", "default": ""},
        {"name": "esFavorito",         "type": "boolean", "default":false},
        {"name": "esRetweet",          "type": "boolean", "default":false},
        {"name": "estaTruncado",       "type": "boolean", "default":false},
        
        {"name": "usuario", "default":{}, "type": {
            "name": "User",
            "type": "record",
            "doc": "User information",
            "namespace": "goraexplorer.twitter",
            "fields": [
                {"name": "id",                    "type": "long", "default": 0},
                {"name": "nombre",                "type": ["null","string"], "default": null},
                {"name": "login",                 "type": "string", "default": ""},
                {"name": "creado",                "type": ["null","string"], "default": null},
                {"name": "descripcion",           "type": ["null","string"], "default": null},
                {"name": "numFavoritos",          "type": "int", "default": 0},
                {"name": "numSeguidores",         "type": "int", "default": 0},
                {"name": "numAmigos",             "type": "int", "default": 0},
                {"name": "lenguajePreferido",     "type": ["null","string"], "default": null},
                {"name": "numListas",             "type": "int", "default": 0},
                {"name": "lugar",                 "type": ["null","string"], "default": null},
                {"name": "numStatuses",           "type": "int", "default": 0},
                {"name": "zonaHoraria",           "type": ["null","string"], "default": null},
                {"name": "url",                   "type": ["null","string"], "default": null},
                {"name": "utcOffset",             "type": ["null","int"], "default": null},
                {"name": "permiteContribuyentes", "type": "boolean", "default": false},
                {"name": "geoActivo",             "type": "boolean", "default": false},
                {"name": "protegido",             "type": "boolean", "default": false},
                {"name": "esCelebridadVerificada","type": "boolean", "default": false},
                {"name": "esTraductor",           "type": "boolean", "default": false},
                {"name": "colorFondo",            "type": ["null","string"], "default": null},
                {"name": "urlImagenFondo",        "type": ["null","string"], "default": null},
                {"name": "urlImagenFondoHttps",   "type": ["null","string"], "default": null},
                {"name": "colorLink",             "type": ["null","string"], "default": null},
                {"name": "colorBorde",            "type": ["null","string"], "default": null},
                {"name": "colorRelleno",          "type": ["null","string"], "default": null},
                {"name": "colorTexto",            "type": ["null","string"], "default": null},
                {"name": "fondoMosaico",          "type": "boolean", "default": false},
                {"name": "usarImagenFondo",       "type": "boolean", "default": false}
            ]
        }},

        {"name": "lugar", "default": null, "type": ["null", {
            "name": "Place",
            "type": "record",
            "doc": "Place",
            "namespace": "goraexplorer.twitter",
            "fields": [
                {"name": "id",             "type": "string", "default": ""},
                {"name": "pais",           "type": ["null","string"], "default": null},
                {"name": "codigoPais",     "type": ["null","string"], "default": null},
                {"name": "tipo",           "type": ["null","string"], "default": null},
                {"name": "url",            "type": ["null","string"], "default": null},
                {"name": "nombre",         "type": ["null","string"], "default": null},
                {"name": "nombreCompleto", "type": ["null","string"], "default": null},
                {"name": "direccion",      "type": ["null","string"], "default": null}
            ]
        }]},
        
        {"name": "hashtags", "default": {}, "type": {"type": "map", "values": {
            "name": "Hashtag",
            "type": "record",
            "doc": "Hashtags",
            "namespace": "goraexplorer.twitter",
            "fields": [
                {"name": "texto",  "type": "string", "default": ""},
                {"name": "inicio", "type": ["null","int"], "default": null},
                {"name": "fin",    "type": ["null","int"], "default": null}
            ]
        }}},

        {"name": "contribuyentes", "type": {"type": "map", "values": "long"}, "default": {}},

        {"name": "medias", "default": {}, "type": {"type": "map", "values": {
            "name": "Media",
            "type": "record",
            "doc": "Medias",
            "namespace": "goraexplorer.twitter",
            "fields": [
                {"name": "id",            "type": "long", "default": 0},
                {"name": "url",           "type": ["null","string"], "default": null},
                {"name": "urlDisplay",    "type": ["null","string"], "default": null},
                {"name": "urlMediaHttp",  "type": ["null","string"], "default": null},
                {"name": "urlMediaHttps", "type": ["null","string"], "default": null},
                {"name": "tipo",          "type": ["null","string"], "default": null},
                {"name": "inicio",        "type": ["null","int"], "default": null},
                {"name": "fin",           "type": ["null","int"], "default": null}
            ]
        }}},

        {"name": "urls", "default": {}, "type": {"type": "map", "values": {
            "name": "Url",
            "type": "record",
            "doc": "Urls",
            "namespace": "goraexplorer.twitter",
            "fields": [
                {"name": "url",         "type": "string", "default": ""},
                {"name": "displayUrl",  "type": ["null","string"], "default": null},
                {"name": "expandedUrl", "type": ["null","string"], "default": null},
                {"name": "inicio",      "type": ["null","int"], "default": null},
                {"name": "fin",         "type": ["null","int"], "default": null}
            ]
        }}},

        {"name": "menciones", "default": {}, "type": {"type": "map", "values": {
            "name": "Mencion",
            "type": "record",
            "namespace": "goraexplorer.twitter",
            "doc": "Mentions",
            "fields": [
                {"name": "userId",    "type": "long", "default": 0},
                {"name": "nombre",    "type": ["null","string"], "default": null},
                {"name": "userLogin", "type": ["null","string"], "default": null},
                {"name": "inicio",    "type": ["null","int"], "default": null},
                {"name": "fin",       "type": ["null","int"], "default": null}
            ]
        }}},
        
        {"name": "markers", "type": {"type": "map", "values":"string"}, "default": {}, "doc": "Markers"}

   ]
}', '<?xml version="1.0" encoding="UTF-8"?>
<gora-orm>
    <table name="twitterraw">
        <family name="default"          maxVersions="1"/>
        <family name="contribuyentes"   maxVersions="1"/>
        <family name="hashtags"         maxVersions="1"/>
        <family name="medias"           maxVersions="1"/>
        <family name="urls"             maxVersions="1"/>
        <family name="menciones"        maxVersions="1"/>
        <family name="markers"          maxVersions="1"/>
    </table>

    <class table="twitterraw" keyClass="java.lang.String" name="goraexplorer.twitter.Tweets">
        <field name="id"                    family="default" qualifier="id" />
        <field name="status"                family="default" qualifier="status" />
        <field name="creado"                family="default" qualifier="creado" />
        <field name="latitud"               family="default" qualifier="latitud" />
        <field name="longitud"              family="default" qualifier="longitud" />
        <field name="inReplyToUserLogin"    family="default" qualifier="inReplyToUserLogin" />
        <field name="inReplyToStatusId"     family="default" qualifier="inReplyToStatusId" />
        <field name="inReplyToUserId"       family="default" qualifier="inReplyToUserId" />
        <field name="lugar"                 family="default" qualifier="lugar" />
        <field name="vecesRetweet"          family="default" qualifier="vecesRetweet" />
        <field name="source"                family="default" qualifier="source" />
        <field name="esFavorito"            family="default" qualifier="esFavorito" />
        <field name="esRetweet"             family="default" qualifier="esRetweet" />
        <field name="estaTruncado"          family="default" qualifier="estaTruncado" />
        <field name="usuario"               family="default" qualifier="usuario" />

        <field name="contribuyentes"        family="contribuyentes" />
        <field name="hashtags"              family="hashtags"/>
        <field name="medias"                family="medias" />
        <field name="urls"                  family="urls" />
        <field name="menciones"             family="menciones" />
        <field name="markers"               family="markers" />        
    </class>

</gora-orm>', 'java.lang.String', 2)
;