-- bcrypt generator: http://bcrypthashgenerator.apphb.com
INSERT INTO users(username, password, email, enabled)
VALUES
	('admin','$2a$10$jsWSIQjF5aW2k3knOhDkBuy24BlxZ8gJUbP5tbJjzXckQJcmOyNxK','admin@no.com',1), -- admin:admin
	('alfonso','$2a$06$1hwx3MXIuqAHjUnB3vtSsOrG3H9nIEI8hRhu3jY4M8fnmqdWHtcb6', 'alfonso.nishikawa@gmail.com', 1), -- alfonso:aaaaa
	('pepe1', '$2a$06$8w7Ms8DsZjfEOXJ8wW9BMOqJ7VU5bvexrr7Ap.VGBJEnvMJChLyNi', 'pepe@no.com', 1), -- pepe1:bbbbb
	('jose1', '$2a$06$227NNFCpyk0QHcxg7oFWXunddSXoiIMNfiYCco8IiZio.4jHpcSV6', 'jose@no.com', 1) -- jose1:ccccc
;

INSERT INTO groups(id, group_name)
VALUES
	(1, 'GROUP_ADMIN'),
	(2, 'GROUP_USER')
;

INSERT INTO group_authorities(group_id, authority)
VALUES
	(1, 'ROLE_ADMIN'),
	(1, 'ROLE_USER'),
	(2, 'ROLE_USER')
;

INSERT INTO group_members(id, username, group_id)
VALUES
	(0, 'admin', 1),
	(1, 'alfonso', 1),
	(2, 'pepe1', 2)
;
