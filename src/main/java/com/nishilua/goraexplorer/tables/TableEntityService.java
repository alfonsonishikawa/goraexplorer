/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.Map;

import org.apache.gora.persistency.Persistent;
import org.apache.gora.persistency.impl.PersistentBase;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.nishilua.goraexplorer.tables.request.TableRequest;

public interface TableEntityService {

    /**
     * Retrieves the tables for a given connection.
     * @param connectionId - Connection Id
     * @param principal - Logged user
     * @return
     */
    public Collection<TableEntity> getTables(Long connectionId, Principal principal) ;
    
    /**
     * Retrieves the given table from the given principal
     * @param tableId - Table Id
     * @param principal - Principal with the table
     * @return
     */
    public TableEntity getTable(Long tableId, Principal principal) ;
    
    /**
     * Given a table id, perform a scan of the first 10 rows.
     * Table must exist.
     * @param tableId
     * @param String startKey - Starting key | null
     * @param String endkey - End key | null
     * @param limit - Max. num. of elements to retrive
     * @return Map key -> value with the row keys and Persistent values
     * @throws Exception
     */
    public Map<Object, Persistent> scan(Long tableId, String startKey, String endKey, Long limit) throws Exception  ;

    /**
     * Adds a new table to the given connection.
     * Checks if the connection belongs to the user
     * 
     * @param newTable - Data for the table. The connection id info must be transformed from Long to Connection
     * @param principal - User to check that the connection belongs
     * @return
     */
    public TableEntity addNewTable(TableRequest newTable, Principal principal);
 
    /**
     * Updates the given table (by id) with the values passed in the JsonNode
     * @param newTable - Table id to update
     * @param updateValues - Values to write
     * @param principal - User principal
     * @return The updated table
     */
    public TableEntity updateTable(Long tableId, JsonNode updateValues, Principal principal) ;

    /**
     * Deletes the given table (by id)
     * @param tableId - Table id to delete
     * @param principal - User to check permissions
     */
    public void deleteTable(Long tableId, Principal principal) ;
    
    /**
     * Checks if a given table belongs to a given user
     * WARNING: Clears the Entity Manager!
     * 
     * @param tableId - Table identifier
     * @param principal - Logged user
     * @return true if the table belongs to the user, false otherwise.
     */
    public boolean checkTableBelongsToUser(Long tableId, Principal principal) ;
    
    /**
     * Creates or updates a row with the given deep-diff values
     * 
     * @param tableId - Table identifier
     * @param rowKey - Row identifier
     * @param rowDiff - deep-diff format diff JSON object with the updates, or null when creating a new row.
     * @param principal - Logged in user
     * @return The updated Entity
     */
    public PersistentBase createOrUpdateRow(Long tableId, String rowKey, ArrayNode rowDiff, Principal principal) throws IOException ;
    
    /**
     * Checks if a row with the given key exists in a table
     * 
     * @param tableId - Table identifier
     * @param rowKey - Row identifier to check its existence
     * @param principal - Logged in user
     * @return true if the row exists, false otherwise
     */
    public Boolean existsRow(Long tableId, String rowKey, Principal principal) throws IOException ;
    
    /**
     * Deletes a row from a table given it's key
     * 
     * @param tableId - Table identifier
     * @param rowKey - Row identifier
     * @param principal - Logged in user
     */
    public void deleteRow(Long tableId, String rowKey, Principal principal) throws IOException ;
    
}
