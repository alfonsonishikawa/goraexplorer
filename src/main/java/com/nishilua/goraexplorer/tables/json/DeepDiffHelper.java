/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables.json;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.avro.Schema;
import org.apache.avro.util.Utf8;
import org.apache.gora.persistency.Persistent;
import org.apache.gora.persistency.impl.PersistentBase;
import org.apache.gora.util.GoraException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nishilua.goraexplorer.exceptions.InternalServerErrorException;

/**
 * Given the ArratyNode with the deep-diff differences, it performs the changes to the persistent entity given.
 * 
 * Differences are reported as one or more change records. Change records have the following structure:
 * 
 *     kind - indicates the kind of change; will be one of the following:
 *         N - indicates a newly added property/element
 *         D - indicates a property/element was deleted
 *         E - indicates a property/element was edited
 *         A - indicates a change occurred within an array
 *     path - the property path (from the left-hand-side root)
 *     lhs - the value on the left-hand-side of the comparison (undefined if kind === 'N')
 *     rhs - the value on the right-hand-side of the comparison (undefined if kind === 'D')
 *     index - when kind === 'A', indicates the array index where the change occurred
 *
 */
public class DeepDiffHelper {

    /**
     * Entry point to apply a Diff to a persistent entity.
     * The diff must be in deep-diff format.
     * 
     * @param persistentEntity
     * @param diff
     * @throws GoraException 
     * @throws ClassNotFoundException
     */
    public static void merge(PersistentBase persistentEntity, ArrayNode diff) throws GoraException, ClassNotFoundException {
        // Root diff is an Array of diff entries
        
        // The array delete commands must be reordered, since the delete indexes must be in descending order because
        // each delete command will be applied one by one and the indexes get updated on each delete. One of the solutions
        // is apply the commands ordered in reverse order (so deleting an element does not affect the elements with lower index).
        reorderArrayDeletes(diff); // Modifies the ArrayNode!!
        for (JsonNode diffEntry: diff) {
            ObjectNode singleChangeObjectDiff = (ObjectNode) diffEntry ;
            String kind = singleChangeObjectDiff.get("kind").textValue() ;
            
            switch (kind) {
                case "N" :
                    doAddNewProperty(persistentEntity, singleChangeObjectDiff) ;
                    break ;
                case "E" :
                    doEdit(persistentEntity, singleChangeObjectDiff) ;
                    break ;
                case "A" :
                    doArray(persistentEntity, singleChangeObjectDiff) ;
                    break ;
                case "D" :
                    doDelete(persistentEntity, singleChangeObjectDiff) ;
                    break ;
                default:
                    throw new InternalServerErrorException("Unrecognized operation type [" + kind + "]") ;
            }
        }
        
    }
    
    /**
     * Performs the new diff operation on the given persistent
     * @param persistentEntity - The persistent entity to modify
     * @param diffEntry - deep-diff edit entry
     * @throws GoraException 
     * @throws ClassNotFoundException 
     */
    private static void doAddNewProperty(PersistentBase persistentEntity, ObjectNode diffEntry) throws GoraException, ClassNotFoundException {
        ArrayNode path = (ArrayNode) diffEntry.get("path") ;
        String pathLastToken = path.get(path.size() - 1 ).asText() ;
        Entry<Object,Schema> parentPair = navigateToPenultimatePath(persistentEntity, path) ;
        
        AvroSchemaHelper.setValue(parentPair.getKey(), parentPair.getValue(), pathLastToken, diffEntry.get("rhs"));
    }
    
    /**
     * Performs the edit diff operation on the given persistent
     * @param persistentEntity - The persistent entity to modify
     * @param diffEntry - deep-diff edit entry
     * @throws GoraException 
     * @throws ClassNotFoundException 
     */
    private static void doEdit(PersistentBase persistentEntity, ObjectNode diffEntry) throws GoraException, ClassNotFoundException {
        ArrayNode path = (ArrayNode) diffEntry.get("path") ;
        String pathLastToken = path.get(path.size() - 1 ).asText() ;
        Entry<Object,Schema> parentPair = navigateToPenultimatePath(persistentEntity, path) ;
        
        AvroSchemaHelper.setValue(parentPair.getKey(), parentPair.getValue(), pathLastToken, diffEntry.get("rhs"));
    }
    
    /**
     * Performs the delete a map value diff operation on the given persistent
     * @param persistentEntity - The persistent entity to modify
     * @param diffEntry - deep-diff edit entry
     * @throws ClassNotFoundException 
     */
    @SuppressWarnings("rawtypes")
    private static void doDelete(PersistentBase persistentEntity, ObjectNode diffEntry) {
        ArrayNode path = (ArrayNode) diffEntry.get("path") ;
        Utf8 pathLastToken = new Utf8(path.get(path.size() - 1 ).asText()) ;
        Entry<Object,Schema> parentPair = navigateToPenultimatePath(persistentEntity, path) ;
        
        ((Map)parentPair.getKey()).remove(pathLastToken) ;
    }
    
    /**
     * Performs an array operation: new(or update) / delete.
     * @param persistentEntity - The persistent entity to modify
     * @param diffEntry - deep-diff edit entry
     * @throws GoraException 
     * @throws ClassNotFoundException 
     */
    @SuppressWarnings("rawtypes")
    private static void doArray(PersistentBase persistentEntity, ObjectNode diffEntry) throws GoraException, ClassNotFoundException {
        ArrayNode path = (ArrayNode) diffEntry.get("path") ;
        Long index = diffEntry.get("index").asLong() ;
        String arrayEditType = diffEntry.get("item").get("kind").textValue() ;
        Entry<Object,Schema> arrayPair = navigateToEndPath(persistentEntity, path) ;
        
        switch(arrayEditType) {
            case "N":
                AvroSchemaHelper.setValue(arrayPair.getKey(), arrayPair.getValue(), String.valueOf(index), diffEntry.get("item").get("rhs"));
                break ;
            case "D":
                ((List) arrayPair.getKey()).remove(index.intValue()) ;
                break ;
            default:
                throw new InternalServerErrorException("Unrecognized operation type [" + arrayEditType + "]") ;
        }
    }
    
    /**
     * Returns a pointer Object ot the Array/Map/Object after following all the given path.
     * 
     * @param persistentEntity - The persistent entity no navigate upon
     * @param pathNode - ArrayNode with the path fields/indexes of the data
     * @return an Object that is pointing to that las element if it is an array/map/object
     */
    private static Entry<Object,Schema> navigateToEndPath(PersistentBase persistentEntity, ArrayNode pathNode) {
        return navigatePathElements(persistentEntity, pathNode, pathNode.size()) ;
    }
    
    private static Entry<Object, Schema> navigateToPenultimatePath(PersistentBase persistentEntity, ArrayNode pathNode) {
        return navigatePathElements(persistentEntity, pathNode, pathNode.size() - 1) ;
    }

    /**
     * Returns a pointer Object ot the Array/Map/Object after following the given path the numer of times in numElements.
     * 
     * @param persistentEntity - The persistent entity no navigate upon
     * @param pathNode - ArrayNode with the path fields/indexes of the data
     * @param numElements - Number of elements to traverse from the path
     * @return an Object that is pointing to that las element if it is an array/map/object
     */
    private static Entry<Object, Schema> navigatePathElements(PersistentBase persistentEntity, ArrayNode pathNode, int numElements) {
        // Pointer to navigate through the path of an entity
        Object persistentNavigationPointer = persistentEntity ;
        Schema schema = persistentEntity.getSchema() ;
        for (int pathIndex = 0; pathIndex < numElements; pathIndex ++) {
            if (persistentNavigationPointer instanceof Persistent) {
                String fieldName = pathNode.get(pathIndex).textValue() ;
                schema = AvroSchemaHelper.getNonNullSchema(schema.getField(fieldName).schema()) ;
                persistentNavigationPointer = ((PersistentBase)persistentNavigationPointer).get(fieldName) ;
            } else if (persistentNavigationPointer instanceof Map) {
                schema = AvroSchemaHelper.getNonNullSchema(schema.getValueType()) ;
                persistentNavigationPointer = ((Map<?,?>) persistentNavigationPointer).get(pathNode.get(pathIndex).textValue()) ;
            } else if (persistentNavigationPointer instanceof List) {
                schema = AvroSchemaHelper.getNonNullSchema(schema.getElementType()) ;
                Integer arrayIndex = Integer.valueOf(pathNode.get(pathIndex).textValue()) ;
                persistentNavigationPointer = ((List<?>) persistentNavigationPointer).get(arrayIndex);
            } else {
                throw new InternalServerErrorException("Unexpected value at pathIndex = " + pathIndex ) ;
            }
        }
        return new SimpleImmutableEntry<Object, Schema>(persistentNavigationPointer, schema) ;
    }

    /**
     * Reorders the Array's delete diffs, so the highest indexes are applied first in decreasing order.
     * 
     * @param diff ArrayNode with all diff edits
     * @return void . The edit commands are reordered at the ArrayNode
     */
    private static void reorderArrayDeletes(ArrayNode diff) {
        // Hashmap containing for a given path, the delete commands
        // The ArrayNode -> TreeMap reason is because given a path:ArrayNode, the TreeMap will hold the index+DeleteCommand
        // and the keys can be retrieven in descending order from the TreeMap
        Map<ArrayNode, TreeMap<Long, ObjectNode>> groupedDeleteOperations = new HashMap<>() ;
        
        Iterator<JsonNode> diffEntriesIterator = diff.elements() ;
        
        // Extract all Array/D operations
        while (diffEntriesIterator.hasNext()) {
            ObjectNode diffEntry = (ObjectNode) diffEntriesIterator.next() ;
            String kind = diffEntry.get("kind").textValue() ;
            // Array operation
            if (kind.equals("A")) {
                String arrayEditType = diffEntry.get("item").get("kind").textValue() ;
                if (arrayEditType.equals("D")) {
                    ArrayNode path = (ArrayNode) diffEntry.get("path") ;
                    Long index = diffEntry.get("index").asLong() ;
                    
                    TreeMap<Long, ObjectNode> treeMapWithSinglePathDeletes = groupedDeleteOperations.get(path) ;
                    if (treeMapWithSinglePathDeletes == null) {
                        treeMapWithSinglePathDeletes = new TreeMap<>() ;
                        groupedDeleteOperations.put(path, treeMapWithSinglePathDeletes) ;
                    }
                    
                    treeMapWithSinglePathDeletes.put(index, diffEntry) ;
                    diffEntriesIterator.remove(); // Remove the saved element
                }
            }
        }
        
        // Insert again all the Array/D operations, but in revese key order
        for (ArrayNode pathNode : groupedDeleteOperations.keySet()) {
            TreeMap<Long, ObjectNode> operationsGroup = groupedDeleteOperations.get(pathNode) ;
            for (Long descendingKey : operationsGroup.descendingKeySet()) {
                diff.add(operationsGroup.get(descendingKey)) ;
            }
        }
        
    }
    
}
