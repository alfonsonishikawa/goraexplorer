/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables.json;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.avro.Schema;
import org.apache.avro.Schema.Type;
import org.apache.avro.util.Utf8;
import org.apache.gora.persistency.BeanFactory;
import org.apache.gora.persistency.impl.BeanFactoryImpl;
import org.apache.gora.persistency.impl.PersistentBase;
import org.apache.gora.util.GoraException;

import com.fasterxml.jackson.databind.JsonNode;
import com.nishilua.goraexplorer.exceptions.DiffTooLargeArrayIndexException;
import com.nishilua.goraexplorer.exceptions.InternalServerErrorException;
import com.nishilua.goraexplorer.exceptions.NotImplementedException;

class AvroSchemaHelper {
    
    /**
     * Given a parent object, the value schema, it updates/insert the a value given the key/fieldname.
     * @param parent
     * @param valueSchema
     * @param key
     * @param value
     * @throws GoraException 
     * @throws ClassNotFoundException - When a Persistent entity value being create does not exist
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void setValue(Object parent, Schema parentSchema, String key, JsonNode value) throws GoraException, ClassNotFoundException {
        Schema valueSchema ;

        // Discover the value schema
        parentSchema = AvroSchemaHelper.getNonNullSchema(parentSchema) ;
        switch (parentSchema.getType()) {
            case MAP:
                valueSchema = parentSchema.getValueType() ;
                break ;
            case ARRAY:
                valueSchema = parentSchema.getElementType() ;
                break ;
            case RECORD:
                valueSchema = parentSchema.getField(key).schema() ;
                break ;
            default:
                throw new InternalServerErrorException("Unexpected type") ;
        }
        
        // Create the value object from the diff and the schema
        valueSchema = AvroSchemaHelper.getNonNullSchema(valueSchema) ;
        Object valueConverted = convertDiffJsonNodeToValue(value, valueSchema) ;
        
        // Write the value to the field/map/array
        switch (parentSchema.getType()) {
            case MAP:
                ((Map)parent).put(new Utf8(key), valueConverted);
                break ;
            case ARRAY:
                // Updates a value, or ONLY add a last element (index must be correct as last value)
                List parentList = (List)parent ;
                Integer index = Integer.valueOf(key) ;
                if (index > parentList.size()) {
                    throw new DiffTooLargeArrayIndexException("Array index " + index + " when the list has " + parentList.size() + " elements. Only allowed to append one element") ;
                }
                if (index == parentList.size()) {
                    parentList.add(null) ;
                }
                parentList.set(index, valueConverted) ;
                break ;
            case RECORD:
                ((PersistentBase)parent).put(key, valueConverted);
                ((PersistentBase)parent).setDirty(key) ;
                break ;
            default:
                throw new InternalServerErrorException("Unexpected type") ;
        }
    }
    
    /**
     * Given a JsonNode with a diff object value, returns the value with the proper class Type
     * @param diffValue
     * @return
     * @throws GoraException 
     * @throws ClassNotFoundException - When creating a Persistent record, maybe the class is not found
     */
    private static Object convertDiffJsonNodeToValue(JsonNode diffValue, Schema diffSchema) throws GoraException, ClassNotFoundException {
        if (diffValue.isNull()) {
            return null ;
        }
        Type diffSchemaType = diffSchema.getType() ;
        switch (diffSchemaType) {
        case RECORD:
            @SuppressWarnings("rawtypes")
            BeanFactory beanFactory = new BeanFactoryImpl<>(String.class, diffSchema.getFullName()) ;
            PersistentBase newPersistent = (PersistentBase)beanFactory.newPersistent() ;
            
            Iterator<Entry<String,JsonNode>> recordDiffIterator = diffValue.fields() ;
            while (recordDiffIterator.hasNext()) {
                Entry<String, JsonNode> fieldDiff = recordDiffIterator.next() ;
                setValue(newPersistent, diffSchema, fieldDiff.getKey(), fieldDiff.getValue()) ;
            }
            return newPersistent ;
        case ENUM:
            throw new NotImplementedException("ENUMS not supported") ;
        case ARRAY:
            @SuppressWarnings("rawtypes")
            List newList = new ArrayList<>() ;
            int arrayIndex = 0 ;
            Iterator<JsonNode> arrayIterator = diffValue.iterator() ;
            while (arrayIterator.hasNext()) {
                JsonNode itemDiff = arrayIterator.next() ;
                setValue(newList, diffSchema, String.valueOf(arrayIndex), itemDiff) ;
                arrayIndex++ ;                
            }
            return newList ;
        case MAP:
            @SuppressWarnings("rawtypes")
            Map newMap = new HashMap<>() ;
            Iterator<Entry<String,JsonNode>> mapIterator = diffValue.fields() ;
            while (mapIterator.hasNext()) {
                Entry<String, JsonNode> mapElementEntry = mapIterator.next() ;
                String key = mapElementEntry.getKey() ;
                JsonNode elementDiff = mapElementEntry.getValue() ;
                setValue(newMap, diffSchema, key, elementDiff) ;
            }
            return newMap ;
        case UNION:
            throw new InternalServerErrorException("Unexpected UNION Type at this level after #setValue()") ;
        case FIXED:
            throw new NotImplementedException("Fixed is not implemented") ;
        case STRING:
            return diffValue.textValue() ;
        case BYTES:
            return ByteBuffer.wrap(Base64.getDecoder().decode(diffValue.textValue())) ;
        case INT:
            return (Integer)diffValue.asInt() ;
        case LONG:
            return (Long)diffValue.asLong() ;
        case FLOAT:
            return (Float)((Double)diffValue.asDouble()).floatValue();
        case DOUBLE:
            return (Double)diffValue.asDouble() ;
        case BOOLEAN:
            return (Boolean)diffValue.asBoolean() ;
        case NULL:
            // actually not implemented
            // return null ;
            throw new NotImplementedException("NULL is not implemented") ;
        default:
            throw new InternalServerErrorException("Unexpected type '" + diffSchemaType.getName() + "'") ;
        }
    }    

    /**
     * Given an Avro Schema, if it is a union null, returns the non-null schema of the union
     * @param schema - The Schema, potentially an union null one.
     * @return A non-null Schema (the same of the imput, or the non-null one of a union)
     */
    public static Schema getNonNullSchema(Schema schema) {
        if (schema.getType() == Type.UNION) {
            int index = getResolvedUnionIndex(schema) ;
            return schema.getTypes().get(index) ;
        } else {
            return schema ;
        }
    }
    
    /**
     * Given an union schema, returns the index of the non-null element.
     * Copied from Gora's implementation.
     * 
     * @param unionScema
     * @return
     */
    public static int getResolvedUnionIndex(Schema unionScema) {
      if (unionScema.getTypes().size() == 2) {

        // schema [type0, type1]
        Type type0 = unionScema.getTypes().get(0).getType();
        Type type1 = unionScema.getTypes().get(1).getType();

        // Check if types are different and there's a "null", like ["null","type"]
        // or ["type","null"]
        if (!type0.equals(type1)
            && (type0.equals(Schema.Type.NULL) || type1.equals(Schema.Type.NULL))) {

          if (type0.equals(Schema.Type.NULL))
            return 1;
          else
            return 0;
        }
      }
      return 2;
    }
    
    /**
     * Given an union schema, tells if it is a nullable schema. 
     * Copied from Gora's implementation.
     * 
     * @param unionSchema
     * @return true if the union schema is nullable.
     */
    public static boolean isNullable(Schema unionSchema) {
        for (Schema innerSchema : unionSchema.getTypes()) {
            if (innerSchema.getType().equals(Schema.Type.NULL)) {
                return true;
            }
        }
        return false;
    }

}
