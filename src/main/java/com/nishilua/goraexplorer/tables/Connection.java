/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.nishilua.goraexplorer.users.User;

@Entity
@Table(name="connections")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id", scope=Connection.class)
@NamedEntityGraph(name = "Connection.eagerLevel1", attributeNodes = { @NamedAttributeNode("tables"), @NamedAttributeNode("user") } )
public class Connection implements Serializable {

    private static final long serialVersionUID = -539226392101852695L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne(optional=false, fetch=FetchType.LAZY)
    @JoinColumn(name="username")
    private User user;
    
    @NotNull
    @Size(min=5, max=50)
    private String name;
    
    @Size(max=500)
    private String description;
    
    @Enumerated(EnumType.STRING)
    @NotNull
    private ConnectionType type;
    
    //@Lob - Not supported by SQLite JDBC Driver
    @Column(name="gora_properties")
    private String goraProperties;
    
    //@Lob - Not supported by SQLite JDBC Driver
    @Column(name="hadoop_configuration")
    private String hadoopConfiguration;

    @OneToMany(cascade=CascadeType.ALL, mappedBy="connection", orphanRemoval=true, fetch=FetchType.LAZY)
    private Set<TableEntity> tables;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ConnectionType getType() {
        return type;
    }

    public void setType(ConnectionType type) {
        this.type = type;
    }

    public String getGoraProperties() {
        return goraProperties;
    }

    public void setGoraProperties(String goraProperties) {
        this.goraProperties = goraProperties;
    }

    public String getHadoopConfiguration() {
        return hadoopConfiguration;
    }

    public void setHadoopConfiguration(String hadoopConfiguration) {
        this.hadoopConfiguration = hadoopConfiguration;
    }

    public void setTables(Set<TableEntity> tables) {
        this.tables = tables;
    }

    public Collection<TableEntity> getTables() {
        return tables;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Connection other = (Connection) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (type != other.type)
            return false;
        return true;
    }
    
}
