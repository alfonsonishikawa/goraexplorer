/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.avro.specific.SpecificRecord;
import org.apache.commons.codec.binary.Base64;
import org.apache.gora.persistency.Persistent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

/**
 * Class to build the DTO Persistent --> JSON, since Persistent is not a suitable class to serialize for services
 * 
 * Given a serie of (key,Persistent) pairs, builds a JSON array DTO with class JsonNode
 */
public class PersistentDTOBuilder {

    public static final String PRIMARY_KEY_NAME = "__id__" ;
    
    Map<Object, Persistent> entities = new LinkedHashMap<>() ;
    
    /**
     * Adds a (key,Persistent) pair to the builder.
     * @param keyValueEntities
     * @return
     */
    public PersistentDTOBuilder add(Object key, Persistent value) {
        entities.put(key, value) ;
        return this ;
    }
    
    /**
     * Adds a set of (key,Persistent) pairs to the builder.
     * @param keyValueEntities
     * @return
     */
    public PersistentDTOBuilder add(Map<Object, Persistent> keyValueEntities) {
        entities.putAll(keyValueEntities) ;
        return this ;
    }
    
    /**
     * Builds the JSON representation of the single element of the Collection configured, without wrapping it in an array
     * @return
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    public JsonNode buildSingle() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        Object key = this.entities.keySet().iterator().next() ;
        Persistent value = this.entities.get(key) ;
        ObjectNode entityNode = buildPersistent(value) ;
        this.setPrimaryKey(entityNode, key) ;
        return entityNode ;
    }
    
    /**
     * Builds the JSON representation of the Collection configured
     * @return ArrayNode with the collection of (Persistent + __id__) elements
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    public JsonNode build() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        ArrayNode rootArrayNode = JsonNodeFactory.instance.arrayNode() ;
        
        for (Entry<Object, Persistent> keyValueEntity: this.entities.entrySet()) {
            ObjectNode entityNode = buildPersistent(keyValueEntity.getValue()) ;
            this.setPrimaryKey(entityNode, keyValueEntity.getKey()) ;
            rootArrayNode.add(entityNode) ;
        }
        
        return rootArrayNode ;
    }
    
    /**
     * Given a Persistent instance, returns the ObjectNode with the JSON information
     * @param persistent
     * @return
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    private ObjectNode buildPersistent(Persistent persistent) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        ObjectNode entityNode = JsonNodeFactory.instance.objectNode() ;

        //We get the Enum "Field" from the Persistent instance
        Class<?>[] innerClasses = persistent.getClass().getDeclaredClasses() ;
        Class<?> enumField = null ;
        
        for (Class<?> candidateClass: innerClasses) {
            if ("Field".equals(candidateClass.getSimpleName())){
                enumField = candidateClass ;
                break ;
            }
        }
                
        // For each element of the enum (fields) we get the index and the name
        Method indexMethod = enumField.getEnumConstants()[0].getClass().getMethod("getIndex");
        Method nameMethod  = enumField.getEnumConstants()[0].getClass().getMethod("getName") ;
        
        for (Object fieldConstant: enumField.getEnumConstants()) {
            int fieldIndex = (int) indexMethod.invoke(fieldConstant) ;
            String fieldName = (String) nameMethod.invoke(fieldConstant) ;

            // Cast to org.apache.avro.specific.SpecificRecord (every actual Gora Entity extends it) to get the value using .get(index) 
            Object value = ((SpecificRecord)persistent).get(fieldIndex) ;

            // We assign the fieldname:value to the node depending on the type
            this.setMapNodeValue(entityNode, fieldName, value);
        }
        return entityNode ;
    }
    
    /**
     * Sets the primary key of a ObjectNode with the JSON information
     * @param entityNode
     * @param value
     */
    private void setPrimaryKey(ObjectNode entityNode, Object value) {
        if (value instanceof CharSequence) {
            entityNode.put(PRIMARY_KEY_NAME, (String)((CharSequence)value).toString()) ;
        } else if (value instanceof String) {
            entityNode.put(PRIMARY_KEY_NAME, (String)value) ;
        } else if (value instanceof Integer) {
            entityNode.put(PRIMARY_KEY_NAME, (Integer)value) ;
        } else if (value instanceof Long) {
            entityNode.put(PRIMARY_KEY_NAME, (Long)value) ;
        } else if (value instanceof Float) {
            entityNode.put(PRIMARY_KEY_NAME, (Float)value) ;
        } else if (value instanceof Double) {
            entityNode.put(PRIMARY_KEY_NAME, (Double)value) ;
        } else {
            throw new RuntimeException ("Unexpected key type " + value.getClass().getName() + " for value " + value.toString()) ;
        }
    }
    
    /**
     * Returns a TextNode with the value from the buffer.
     * @param value [!= null]
     * @return
     */
    private TextNode buildText(ByteBuffer value) {
        return JsonNodeFactory.instance.textNode(Base64.encodeBase64String(value.array())) ;
    }
    
    private ObjectNode buildMap(Map<Object, Object> values) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        ObjectNode mapNode = JsonNodeFactory.instance.objectNode() ;
        for (Entry<Object, Object> pairKV: values.entrySet()) {
            this.setMapNodeValue(mapNode, this.generateMapKey(pairKV.getKey()), pairKV.getValue());
        }
        return mapNode ;
    }

    private String generateMapKey(Object key) {
        if (key instanceof CharSequence || key instanceof Integer || key instanceof Long || key instanceof Float || key instanceof Double) {
            return key.toString();
        } else if (key instanceof String) {
            return (String) key;
        } else {
            throw new RuntimeException("Unexpected map key type " + key.getClass().getName() + " with value " + key.toString());
        }
    }
        
    private ArrayNode buildArray(List<?> values) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        ArrayNode arrayNode = JsonNodeFactory.instance.arrayNode() ;
        for (Object item: values) {
            this.addArrayNodeValue(arrayNode, item) ;            
        }
        return arrayNode ;
    }
    
    @SuppressWarnings("unchecked")
    private void setMapNodeValue(ObjectNode entityNode, String fieldName, Object value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        if (value == null) { 
            entityNode.putNull(fieldName) ;
        } else if (value instanceof CharSequence) {
            entityNode.put(fieldName, (String)((CharSequence)value).toString()) ;
        } else if (value instanceof Integer) {
            entityNode.put(fieldName, (Integer) value) ;
        } else if (value instanceof Long) {
            entityNode.put(fieldName, (Long) value) ;
        } else if (value instanceof Float) {
            entityNode.put(fieldName, (Float) value) ;
        } else if (value instanceof Double) {
            entityNode.put(fieldName, (Double) value) ;
        } else if (value instanceof Boolean) {
            entityNode.put(fieldName, (Boolean) value) ;
        } else if (value instanceof String) {
            entityNode.put(fieldName, (String)value) ;
        } else if (value instanceof ByteBuffer) {
            entityNode.set(fieldName, (TextNode) this.buildText((ByteBuffer)value)) ;
        } else if (value instanceof Map) {
            entityNode.set(fieldName, (ObjectNode) this.buildMap((Map<Object,Object>) value)) ;
        } else if (value instanceof List) {
            entityNode.set(fieldName, (ArrayNode) this.buildArray((List<?>)value)) ;
        } else if (value instanceof Persistent) {
            entityNode.set(fieldName, (ObjectNode) this.buildPersistent((Persistent)value)) ;
        } else {
            throw new RuntimeException ("Unexpected value type " + value.getClass().getName() + " for value " + value.toString()) ;
        }
    }

    @SuppressWarnings("unchecked")
    private void addArrayNodeValue(ArrayNode entityNode, Object value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        if (value == null) { 
            entityNode.addNull() ;  
        } else if (value instanceof CharSequence) {
            entityNode.add((String)((CharSequence)value).toString()) ;
        } else if (value instanceof Integer) {
            entityNode.add((Integer) value) ;
        } else if (value instanceof Long) {
            entityNode.add((Long) value) ;
        } else if (value instanceof Float) {
            entityNode.add((Float) value) ;
        } else if (value instanceof Double) {
            entityNode.add((Double) value) ;
        } else if (value instanceof Boolean) {
            entityNode.add((Boolean) value) ;
        } else if (value instanceof String) {
            entityNode.add((String)value) ;
        } else if (value instanceof ByteBuffer) {
            entityNode.add((TextNode) this.buildText((ByteBuffer)value)) ;
        } else if (value instanceof Map) {
            entityNode.add((ObjectNode) this.buildMap((Map<Object,Object>) value)) ;
        } else if (value instanceof List) {
            entityNode.add((ArrayNode) this.buildArray((List<?>)value)) ;
        } else if (value instanceof Persistent) {
            entityNode.add((ObjectNode) this.buildPersistent((Persistent)value)) ;
        } else {
            throw new RuntimeException ("Unexpected value type " + value.getClass().getName() + " for value " + value.toString()) ;
        }
    }    
    
}
