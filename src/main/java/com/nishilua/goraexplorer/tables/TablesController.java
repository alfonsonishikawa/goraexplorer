/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.Principal;
import java.util.Collection;
import java.util.Map;

import org.apache.gora.persistency.Persistent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nishilua.goraexplorer.exceptions.CannotAccessResourceException;
import com.nishilua.goraexplorer.exceptions.InternalServerErrorException;
import com.nishilua.goraexplorer.exceptions.ResourceAlreadyExistsException;
import com.nishilua.goraexplorer.exceptions.WrongFilterParameterException;
import com.nishilua.goraexplorer.exceptions.WrongParameterFormatException;
import com.nishilua.goraexplorer.request.Filters;
import com.nishilua.goraexplorer.tables.request.RowId;
import com.nishilua.goraexplorer.tables.request.TableRequest;
import com.nishilua.goraexplorer.tables.response.TableResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "tables")
@ResponseStatus(HttpStatus.OK)
@RequestMapping(path = "/tables")
public class TablesController {

    Logger logger = LoggerFactory.getLogger(TablesController.class);
        
    @Autowired
    private TableEntityService tableService;
    
    @RequestMapping(path = "/connection/{connectionId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Retrieves all tables belonging to the logged in user and connection", responseContainer="Set", response=TableEntity.class)
    @ApiResponses(value = { @ApiResponse(code = 403, message = "The user has not access to the connection") }) // Forbidden
    public @ResponseBody Collection<TableEntity> getConnections(
            @ApiParam(value="Connection id") @PathVariable Long connectionId,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {        
        return this.tableService.getTables(connectionId, principal);
    }
 
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Retrieves all tables belonging to the logged in user and given connection id", responseContainer="Set", response=TableEntity.class)
    @ApiResponses(value = { @ApiResponse(code = 403, message = "The user has not access to the connection"), // Forbidden
                            @ApiResponse(code = 400, message = "Wrong filter parameter") }) // Bad Parameter
    public @ResponseBody Collection<TableEntity> getConnections(
            @ApiParam(value="URL encoded JSON filter parameter: %5B%7B%22property%22%3A%22connection%22%2C%22value%22%3A1%2C%22exactMatch%22%3Atrue%7D%5D")
                @RequestParam(name="filter", required=true) Filters filters,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal) throws Exception
    {
        if (filters.size() != 1) {
            throw new WrongFilterParameterException("Expected only 1 filter") ;
        }
        if ( ! "connection".equals(filters.get(0).getProperty()) )  {
            throw new WrongFilterParameterException("Expected filter for connection id") ;
        }
        Long connectionId = ((Integer) filters.get(0).getValue()).longValue() ;
        return this.getConnections(connectionId, principal) ;
    }
    
    @RequestMapping(path = "/{tableId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Retrieves the given table belonging to the user", response=TableEntity.class)
    @ApiResponses(value = { @ApiResponse(code = 403, message = "The user has not access to the table") }) // Forbidden
    public @ResponseBody TableEntity getConnection(
            @ApiParam(value="Table id") @PathVariable Long tableId,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        if ( !this.tableService.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }
        
        return this.tableService.getTable(tableId, principal);
    }
    
    @RequestMapping(path="/scan/{tableId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Retrieves the data of a given table")
    @ApiResponses(value = { @ApiResponse(code = 403, message = "User has not access to the table") }) // Forbidden
    public @ResponseBody JsonNode scanTable(
            @ApiParam(value="Table id") @PathVariable Long tableId,
            @ApiParam(value="Start key") String startKey,
            @ApiParam(value="End Key") String endKey,
            @ApiParam(value="Num. elements") Long scanSize,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true) Principal principal)
    {
        if ( !this.tableService.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }

        try {
            PersistentDTOBuilder persistentDTOBuilder = new PersistentDTOBuilder() ;
            Map<Object, Persistent> rows = this.tableService.scan(tableId, startKey, endKey, scanSize) ;
            return persistentDTOBuilder.add(rows).build() ;
        } catch (Exception e) {
            logger.error("Error retrieving the table data.", e);
            throw new InternalServerErrorException(e.getMessage(), e) ;
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Adds a new table. The connection must belong to the logged user", response = Connection.class)
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Error creating the new table") }) // InternalServerError
    public @ResponseBody TableResponse addNewTable(
            @ApiParam(value="New table data") @RequestBody TableRequest newTable,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        TableEntity createdTable = this.tableService.addNewTable(newTable, principal);
        TableResponse newTableResponse = new TableResponse() ;
        BeanUtils.copyProperties(createdTable, newTableResponse);
        return newTableResponse ;
    }
    
    @RequestMapping(path="/{tableId}", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Updates a table. The table must belong to the logged user", response = Connection.class)
    @ApiResponses(value =
        { @ApiResponse(code = 403, message = "User has not access to the table"), // Forbidden
          @ApiResponse(code = 500, message = "Error updating the table") }) // InternalServerError
    public @ResponseBody TableResponse updateConnection(
            @ApiParam(value="ID of the table to update") @PathVariable("tableId") Long tableId,
            @ApiParam(value="Object with the update values to merge") @RequestBody JsonNode updateValues,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        if ( !this.tableService.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access the table", principal.getName(), tableId)) ;
        }
        // Sanitize:
        // Do not let the user change the table id nor the related connection
        ((ObjectNode)updateValues).remove("id") ;
        ((ObjectNode)updateValues).remove("connection") ;
        TableEntity updatedTable = this.tableService.updateTable(tableId, updateValues, principal);
        TableResponse tableResponse = new TableResponse() ;
        BeanUtils.copyProperties(updatedTable, tableResponse);
        
        return tableResponse ;
    }
    
    @RequestMapping(path="/{tableId}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Deletes the given table. The table must belong to the logged user")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Error deleting the table") }) // InternalServerError
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteConnection(
            @ApiParam(value="ID of the table to delete") @PathVariable("tableId") Long tableId,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        if ( !this.tableService.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access the table %d", principal.getName(), tableId)) ;
        }
        this.tableService.deleteTable(tableId, principal) ;
    }
    
    @RequestMapping(path = "/tableId/{tableId}/row", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Creates a new row in a table", response=TableEntity.class)
    @ApiResponses(value =
        { @ApiResponse(code = 403, message = "User has not access to the table"), // Forbidden
          @ApiResponse(code = 409, message = "The row already exists") // Conflig
        })
    public @ResponseBody JsonNode createRow(
            @ApiParam(value="Table id") @PathVariable Long tableId,
            @ApiParam(value="Object with __id__ key to create") @RequestBody RowId rowId,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        
        if ( !this.tableService.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }

        try {
            String key = rowId.get__id__() ;

            // Check if the row key exists
            if (this.tableService.existsRow(tableId, key, principal)) {
                throw new ResourceAlreadyExistsException("Cannot create the new row because the key already exists") ;
            }
            
            // Create the new row
            PersistentDTOBuilder persistentDTOBuilder = new PersistentDTOBuilder() ;
            Persistent newPersistent = this.tableService.createOrUpdateRow(tableId, key, null, principal) ;
            
            return persistentDTOBuilder.add(key, newPersistent).buildSingle() ;
            
        } catch (IOException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            this.logger.error("Error creating a new row", e) ;
            logger.error("Error retrieving the table data.", e);
            throw new InternalServerErrorException(e.getMessage(), e) ;
        }
        
    }
    
    @RequestMapping(path = "/tableId/{tableId}/row", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Updates a row from a table")
    @ApiResponses(value =
        { @ApiResponse(code = 400, message = "The diff parameter has wrong format"), // Bad Request
          @ApiResponse(code = 400, message = "The diff parameter has a wrong array index"), // Bad Request
          @ApiResponse(code = 403, message = "User has not access to the table"), // Forbidden
          @ApiResponse(code = 500, message = "Error updating the table"), // InternalServerError
          @ApiResponse(code = 501, message = "Feature not implemented") }) // Not implemented
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void updateRow(
            @ApiParam(value="Table id") @PathVariable Long tableId,
            @ApiParam(value="Update Row object with rowDiff and rowKey") @RequestBody JsonNode updateRowJsonObject,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        JsonNode rowDiff = updateRowJsonObject.get("rowDiff") ;
        String rowKey = updateRowJsonObject.get("rowKey").asText() ;
        
        if ( !this.tableService.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }
        if (rowDiff == null) {
            return ;
        }
        if ( ! (rowDiff instanceof ArrayNode) ) {
            this.logger.error(rowDiff.toString());
            throw new WrongParameterFormatException("The row diff parameter should be an array. Wrong format") ;
        }
        
        try {
            this.tableService.createOrUpdateRow(tableId,rowKey, (ArrayNode)rowDiff, principal);
        } catch (Exception e) {
            this.logger.error("Error updating a persistent entity", e) ;
            throw new InternalServerErrorException("Error updating the persistent entity", e) ;
        }
    }
    
    @RequestMapping(path = "/tableId/{tableId}/row", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Deletes a row from a table")
    @ApiResponses(value =
        { @ApiResponse(code = 403, message = "User has not access to the table") // Forbidden
        })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteRow(
            @ApiParam(value="Table id") @PathVariable Long tableId,
            @ApiParam(value="Object with __id__ key to delete") @RequestBody RowId rowId,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal) {

        if ( !this.tableService.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }
        
        String key = rowId.get__id__() ;

        try {
            this.tableService.deleteRow(tableId, key, principal) ;
        } catch (IOException e) {
            this.logger.error("Error deleting a persistent entity", e) ;
            throw new InternalServerErrorException("Error deleting the persistent entity", e) ;
        }
    }
    
}
