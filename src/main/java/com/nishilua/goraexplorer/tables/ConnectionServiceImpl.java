/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.gora.store.DataStoreMetadataFactory;
import org.apache.gora.store.impl.DataStoreMetadataAnalyzer;
import org.apache.hadoop.conf.Configuration;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.nishilua.goraexplorer.exceptions.CannotAccessResourceException;
import com.nishilua.goraexplorer.exceptions.InternalServerErrorException;
import com.nishilua.goraexplorer.tables.request.ConnectionRequest;
import com.nishilua.goraexplorer.tables.response.BackendTablesMetadata;
import com.nishilua.goraexplorer.users.User;
import com.nishilua.goraexplorer.users.UserRepository;

@Service("connectionService")
@Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
public class ConnectionServiceImpl implements ConnectionService {

    @PersistenceContext
    private EntityManager em ;
    
    @Autowired
    private ConnectionRepository connectionRepository ;
    
    @Autowired
    private UserRepository userRepository ;

    /**
     * Object mapper for updating operation from JsonNodes to Entity
     */
    private static ObjectMapper JSONObjectMapper = new ObjectMapper() ;
    
    @Override
    public Collection<Connection> getConnections(Principal principal) {
        return this.connectionRepository.findByUserUsername(principal.getName()) ;
    }
    
    @Override
    public Connection getConnection(Long connectionId, Principal principal) {
        if ( !this.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        return this.connectionRepository.findConnectionEagerLevel1ById(connectionId) ;
    }
    
    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly=false)
    public Connection addNewConnection(ConnectionRequest connectionRequest, Principal principal) {
        User user = this.userRepository.findById(principal.getName())
            .orElseThrow(
                () -> new InternalServerErrorException("The user '" + principal.getName() + "' was not found. Operation aborted.")) ;
        Connection newConnection = new Connection() ;
        BeanUtils.copyProperties(connectionRequest, newConnection);
        newConnection.setUser(user) ;
        newConnection.setTables(null);
        return this.connectionRepository.saveAndFlush(newConnection) ;
    }
    
    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly=false)
    public void deleteConnection(Long connectionId, Principal principal) {
        if ( !this.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        this.connectionRepository.deleteById(connectionId) ;
    }

    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly=false)
    public Connection updateConnection(Long connectionId, JsonNode updateValues, Principal principal) {
        
        if ( !this.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }

        Connection oldConnection = connectionRepository.findConnectionEagerLevel1ById(connectionId) ;
        ObjectReader updater = JSONObjectMapper.readerForUpdating(oldConnection);
        Connection mergedConnection = null ;
        try {
            mergedConnection = updater.readValue(updateValues);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.connectionRepository.saveAndFlush(mergedConnection) ;
    }

    @Override
    public BackendTablesMetadata getBackendTablesMetadata(Long connectionId, Principal principal) throws ClassNotFoundException, IOException {
        if ( !this.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        Connection connection = this.connectionRepository.getOne(connectionId) ;
        Configuration hadoopConfiguration = DataStoreHelper.createConfigurationFromConnection(connection) ;
        Properties goraProperties = DataStoreHelper.createPropertiesFromConnection(connection) ;
        DataStoreMetadataAnalyzer analyzer = DataStoreMetadataFactory.createAnalyzer(hadoopConfiguration, goraProperties) ;
        BackendTablesMetadata tablesMetadata = new BackendTablesMetadata() ;
        tablesMetadata.setType(analyzer.getType()) ;
        tablesMetadata.setTables(analyzer.getTablesNames()) ;
        analyzer.close() ;
        return tablesMetadata;
    }

    @Override
    public Object getBackendTableMetadata(Long connectionId, String tableName, Principal principal) throws ClassNotFoundException, IOException {
        if ( !this.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        Connection connection = this.connectionRepository.getOne(connectionId) ;
        Configuration hadoopConfiguration = DataStoreHelper.createConfigurationFromConnection(connection) ;
        Properties goraProperties = DataStoreHelper.createPropertiesFromConnection(connection) ;
        DataStoreMetadataAnalyzer analyzer = DataStoreMetadataFactory.createAnalyzer(hadoopConfiguration, goraProperties) ;
        Object backendTableMetadata = analyzer.getTableInfo(tableName) ;
        analyzer.close() ;
        return backendTableMetadata;
    }

    @Override
    public boolean checkConnectionBelongsToUser(Long connectionId, Principal principal) {
        boolean connectionBelongsToUser = (null != this.connectionRepository.findByIdAndUserUsername(connectionId, principal.getName())) ;
        this.em.clear(); // BUG from JPA not fetching again when asked for further levels of eagerness
        return connectionBelongsToUser ;

    }

}
