/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.IOException;
import java.security.Principal;
import java.util.Collection;

import org.apache.gora.util.GoraException;

import com.fasterxml.jackson.databind.JsonNode;
import com.nishilua.goraexplorer.tables.request.ConnectionRequest;
import com.nishilua.goraexplorer.tables.response.BackendTablesMetadata;

public interface ConnectionService {

    /**
     * Gets all connections of the given principal
     * @param principal - Principal to get the connections from
     * @return A collection with all the user's connections
     */
    public Collection<Connection> getConnections(Principal principal) ;
    
    /**
     * Gets the information of the given connection, belonging to the given principal
     * @param principal - Principal to get the connections from
     * @return A collection with all the user's connections
     */
    public Connection getConnection(Long connectionId, Principal principal) ;
    
    /**
     * Adds a new empty connection for the given principal
     * @param newConnection - Data for the connection. The username and tables will be overwritten.
     * @param principal - Principal to add the connection to
     * @return The new added connection with the id field filled.
     */
    public Connection addNewConnection(ConnectionRequest newConnection, Principal principal) ;
    
    /**
     * Deletes the given connection (by id)
     * @param connectionId - Connection id to delete
     * @param principal - Principal to check permissions
     */
    public void deleteConnection(Long connectionId, Principal principal) ;
    
    /**
     * Updates the given connection (by id) with the values passed in the JsonNode
     * @param connectionId - Connection id to update
     * @param updateValues - Values to write
     * @param principal - Principal to check permissions
     * @return The updated connection
     */
    public Connection updateConnection(Long connectionId, JsonNode updateValues, Principal principal) ;
    
    /**
     * Given a connection, returns the native information from the backend about which tables does it
     * contains. This will be useful to create new tables connectons.
     * 
     * @param connectionId - Connection id with the configuration to connect to a native backend
     * @param principal- Principal to check permissions
     * @return A BackendTablesMetadata instance
     * @throws GoraException 
     * @throws IOException 
     */
    public BackendTablesMetadata getBackendTablesMetadata(Long connectionId, Principal principal) throws GoraException, ClassNotFoundException, IOException ;
    
    /**
     * Returns detailed information of a native table, like what fields have, columns,...
     * Useful to create new tables connections.
     * @param connectionId - Connection id with the configuration to connect to a native backend. 
     *                       The parameter tablename must belong  to this connection
     * @param tableName - One table name from {{@link #getBackendTablesMetadata(Long, Principal)},
     * @param principal - Principal to check permissions 
     * @return An instance depending of the specific native backend. 
     * @throws GoraException 
     * @throws IOException 
     */
    public Object getBackendTableMetadata(Long connectionId, String tableName, Principal principal) throws GoraException, ClassNotFoundException, IOException ;
    
    /**
     * Checks if a given connection belongs to a given user.
     * WARNING: Clears the Entity Manager!
     * 
     * @param connectionId - Connection identifier
     * @param principal - Principal principal
     * @return true if the connection belongs to the user, false otherwise.
     */
    public boolean checkConnectionBelongsToUser(Long connectionId, Principal principal) ;
}
