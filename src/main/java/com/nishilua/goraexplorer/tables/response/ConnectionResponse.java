/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables.response;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.nishilua.goraexplorer.tables.ConnectionType;
import com.nishilua.goraexplorer.tables.TableEntity;
import com.nishilua.goraexplorer.users.User;

/**
 * Bean with the response object for a new connection, adapted for ExtJS:
 * <ul>
 * <li>The referenced entities must be as "id" not the POJO objects like when serialized in JSON</li>
 * </ul>
 * 
 * For this, to the referenced User the annotation @JsonIdentityReference(alwaysAsId = true) has been
 * used, what makes it to return only the username and not all the User bean in json.
 */
public class ConnectionResponse {

    private Long id;
    private String name;
    private String description;
    private ConnectionType type;
    private String goraProperties;
    private String hadoopConfiguration;
    @JsonIdentityReference(alwaysAsId = true)
    private User user;
    @JsonIdentityReference(alwaysAsId = true)
    private Set<TableEntity> tables;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public ConnectionType getType() {
        return type;
    }
    
    public void setType(ConnectionType type) {
        this.type = type;
    }
    
    public String getGoraProperties() {
        return goraProperties;
    }
    
    public void setGoraProperties(String goraProperties) {
        this.goraProperties = goraProperties;
    }
    
    public String getHadoopConfiguration() {
        return hadoopConfiguration;
    }
    
    public void setHadoopConfiguration(String hadoopConfiguration) {
        this.hadoopConfiguration = hadoopConfiguration;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<TableEntity> getTables() {
        return tables;
    }

    public void setTables(Set<TableEntity> tables) {
        this.tables = tables;
    }
    
}
