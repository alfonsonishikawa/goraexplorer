/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables.response;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.nishilua.goraexplorer.tables.Connection;

/**
 * Bean with the response object for a new table, adapted for ExtJS:
 * <ul>
 * <li>The referenced entities must be as "id" not the POJO objects like when serialized in JSON</li>
 * </ul>
 * 
 * For this, the referenced entities attributes are annotated with @JsonIdentityReference(alwaysAsId = true)
 */
public class TableResponse {
    
    private Long id ;
    private String name ;
    private String description ;
    private String avroSchema ;
    private String mapping ;
    private String keyClass ;
    @JsonIdentityReference(alwaysAsId = true)
    private Connection connection ;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getAvroSchema() {
        return avroSchema;
    }
    
    public void setAvroSchema(String avroSchema) {
        this.avroSchema = avroSchema;
    }
    
    public String getMapping() {
        return mapping;
    }
    
    public void setMapping(String mapping) {
        this.mapping = mapping;
    }
    
    public String getKeyClass() {
        return keyClass;
    }
    
    public void setKeyClass(String keyClass) {
        this.keyClass = keyClass;
    }
    
    public Connection getConnection() {
        return connection;
    }
    
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
}
