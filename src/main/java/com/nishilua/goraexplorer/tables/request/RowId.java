/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Bean to receive the REST RequestBody JSON to create a new table row. The client will send the new row's Key.
 * <ul>
 * <li>If there is any additional field in the request it will be ignored. The annotation
 * @JsonIgnoreProperties(ignoreUnknwn = true) was added for this.</li>
 * </ul>
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
public class RowId {
    
    private String __id__ ;

    public String get__id__() {
        return __id__;
    }

    public void set__id__(String __id__) {
        this.__id__ = __id__;
    }    
    
}
