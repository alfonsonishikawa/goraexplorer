/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Bean to receive the REST RequestBody JSON to create a new table.
 * <ul>
 * <li>If there is any additional field in the request it will be ignored. The annotation
 * @JsonIgnoreProperties(ignoreUnknwn = true) was added for this.</li>
 * <li>The User information is ignored always when using this bean</li>
 * </ul>
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
public class TableRequest {

        private Long connection ;
        private String name ;
        private String description ;
        private String avroSchema ;
        private String mapping ;
        private String keyClass ;
        
        public Long getConnection() {
            return connection;
        }
        
        public void setConnection(Long connection) {
            this.connection = connection;
        }
        
        public String getName() {
            return name;
        }
        
        public void setName(String name) {
            this.name = name;
        }
        
        public String getDescription() {
            return description;
        }
        
        public void setDescription(String description) {
            this.description = description;
        }
        
        public String getAvroSchema() {
            return avroSchema;
        }
        
        public void setAvroSchema(String avroSchema) {
            this.avroSchema = avroSchema;
        }
        
        public String getMapping() {
            return mapping;
        }
        
        public void setMapping(String mapping) {
            this.mapping = mapping;
        }
        
        public String getKeyClass() {
            return keyClass;
        }

        public void setKeyClass(String keyClass) {
            this.keyClass = keyClass;
        }

}
