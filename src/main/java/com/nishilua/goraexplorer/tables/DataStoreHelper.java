/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;

/**
 * Helper functions for DataStores, Connections, Tablees,..
 *
 */
public class DataStoreHelper {

    /**
     * Given a connection, creates a Configuration instance from the information in the connection
     * @param connection - The connection to use to create a Configuration instance
     * @return A Hadoop Configuration instance
     */
    public static Configuration createConfigurationFromConnection(Connection connection) {
        Configuration hadoopConfiguration = new Configuration() ;
        hadoopConfiguration.addResource(IOUtils.toInputStream(connection.getHadoopConfiguration(), Charset.forName("UTF-8"))) ;
        return hadoopConfiguration ;
    }
    
    /**
     * Given a connection, creates a Properties instance with Gora configuration from the information in the connection
     * @param connection - The connection to use to create a Properties instance
     * @return A Hadoop Configuration instance
     */
    public static Properties createPropertiesFromConnection(Connection connection) throws IOException {
        Properties goraProperties = new Properties();
        goraProperties.load(IOUtils.toInputStream(connection.getGoraProperties(), Charset.forName("UTF-8")));
        return goraProperties ;
    }
    
}
