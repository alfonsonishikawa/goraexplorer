/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.exceptions;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler{

    Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);
    
    @ExceptionHandler(CannotAccessResourceException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public final @ResponseBody ErrorDetail handleCannotAccessResourceException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(ConflictException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public final @ResponseBody ErrorDetail handleConflictException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(CompilationException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public final @ResponseBody ErrorDetail handleCompilationException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public final @ResponseBody ErrorDetail handleEntityNotFoundException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(InternalServerErrorException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final @ResponseBody ErrorDetail handleInternalServerErrorException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public final @ResponseBody ErrorDetail handleResourceNotFoundException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }

    @ExceptionHandler(UserExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public final @ResponseBody ErrorDetail handleUserExistsException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(TransactionSystemException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final @ResponseBody ErrorDetail handleTransactionSystemException(TransactionSystemException e, WebRequest request) {
        String errorMessage = e.getMessage() ;
        if (e.getRootCause() != null) {
            errorMessage = errorMessage + "\n" + e.getRootCause().getMessage() ;
        }
        ErrorDetail errorDetail = new ErrorDetail(errorMessage) ;
        this.logger.error("", e);
        return errorDetail ;
    }
    
    @ExceptionHandler(MissingIdException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final @ResponseBody ErrorDetail handleMissingIdException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(WrongFilterParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final @ResponseBody ErrorDetail handleWrongFilterParameterException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(NotImplementedException.class)
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    public final @ResponseBody ErrorDetail handleNotImplementedException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
 
    @ExceptionHandler(WrongParameterFormatException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final @ResponseBody ErrorDetail handleWrongParameterFormatException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(DiffTooLargeArrayIndexException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final @ResponseBody ErrorDetail handleDiffTooLargeArrayIndexException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    @ExceptionHandler(ResourceAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public final @ResponseBody ErrorDetail handleResourceAlreadyExistsException(Exception e, WebRequest request) {
        return genericHandleException(e)  ;
    }
    
    private final ErrorDetail genericHandleException(Exception e) {
        String errorMessage = e.getMessage() ;
        if (e.getCause() != null) {
            errorMessage = errorMessage + "\n" + e.getCause().getMessage() ;
        }
        ErrorDetail errorDetail = new ErrorDetail(errorMessage) ;
        this.logger.error("", e);
        return errorDetail ;
    }
    
}
