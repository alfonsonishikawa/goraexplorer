/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.users;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.nishilua.goraexplorer.auth.Group;
import com.nishilua.goraexplorer.tables.Connection;

/**
 * Entity implementation class for Entity: User
 */
@Entity
@Table(name="users")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="username", scope=User.class)
@NamedEntityGraph (
        name = "User.mainDataFromUser",
        attributeNodes = {
                @NamedAttributeNode(value = "connections", subgraph = "connections"),
                @NamedAttributeNode(value = "groups", subgraph = "groups")},
        subgraphs = {
                @NamedSubgraph( name = "connections", attributeNodes = @NamedAttributeNode("tables")),
                @NamedSubgraph( name = "groups",
                                attributeNodes = {
                                        @NamedAttributeNode("authorities")
                                        //,@NamedAttributeNode("users")
                                        } )
        }
    )
public class User implements Serializable {

    private static final long serialVersionUID = -6521122333173751908L;

    @Id
    @NotNull
    @Size(min=5, max=50)
    private String username;
    
    @JsonIgnore
    @NotNull
    private String password;
    
    @Email
    @Size(min=7, max=50)
    private String email;
    
    @NotNull
    private Boolean enabled;
    
    @Column(name="image_url", length=1024)
    private String imageUrl;
    
    @ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(name="GROUP_MEMBERS",
        joinColumns=@JoinColumn(name="USERNAME"),
        inverseJoinColumns=@JoinColumn(name="GROUP_ID", referencedColumnName="ID")
        )
    private Set<Group> groups;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="user", orphanRemoval=true, fetch=FetchType.LAZY)
    private Set<Connection> connections;

    public User() {
        super();
    }
    
    public User(String name, String password, String email, Boolean enabled) {
        this.username = name ;
        this.password = password ;
        this.email = email ;
        this.enabled = enabled ;
    }

   public User(String name, String password, String email, Boolean enabled, String imageUrl) {
        this.username = name ;
        this.password = password ;
        this.email = email ;
        this.enabled = enabled ;
        this.imageUrl = imageUrl ;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return String.format("User - Username: %s, Email: %s, Enabled: %b", this.username, this.email, this.enabled) ;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<Connection> getConnections() {
        return connections;
    }

    public void setConnections(Set<Connection> connections) {
        this.connections = connections;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
        result = prime * result + ((imageUrl == null) ? 0 : imageUrl.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (enabled == null) {
            if (other.enabled != null)
                return false;
        } else if (!enabled.equals(other.enabled))
            return false;
        if (imageUrl == null) {
            if (other.imageUrl != null)
                return false;
        } else if (!imageUrl.equals(other.imageUrl))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        return true;
    }


}
