/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.users;

import java.security.Principal;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nishilua.goraexplorer.exceptions.InternalServerErrorException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "users")
@ResponseStatus(HttpStatus.OK)
@RequestMapping(path = "/users")
public class UsersController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Retrieves all users", responseContainer="Set", response=User.class)
    public @ResponseBody Collection<User> getUsers() {
        return this.userService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Creates a new user", response = User.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "User exists") }) // UserExistsException
    public @ResponseBody User newUser(@RequestBody User user) {
        User newUser =  this.userService.saveNewUser(user) ;
        if (newUser == null) {
            throw new InternalServerErrorException("Error when creating user " + user);
        }
        return newUser;
    }

    @RequestMapping(path = "/{username}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Retrieves one user", response = User.class)
    @ApiResponses(value = { @ApiResponse(code = 404, message = "User not found") }) // ResourceNotFoundException
    public @ResponseBody User getUser(@PathVariable String username) {
        return this.userService.get(username) ;
    }

    @RequestMapping(path = "/profile", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Retrieves own user information, but only user related.",
        notes = "Once a user has logged in, should use this method to retrieve own information. The user must be logged in.",
        response = User.class)
    public @ResponseBody User getProfile(
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        User user = this.userService.get(principal.getName());
        return user ;
    }
    
    @RequestMapping(path = "/userMainData", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Retrieves own user main information after login",
        notes = "Once a user has logged in, should use this method to retrieve all own information. The user must be logged in.",
        response = User.class)
    public @ResponseBody User getUserMainData(
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        User user = this.userService.getMainData(principal.getName());
        return user ;
    }

}
